/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Files
 * (https://gitlab.com/DesQ/DesQApps/DesQFiles)
 * DesQ Files a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtAlgorithms>

// Local Headers
#include "AppsModel.hpp"

#include <DFXdg.hpp>
#include <DFInotify.hpp>


class Application {
    public:
        Application( DFL::XDG::DesktopFile *desktop ) {
            this->desktop = desktop;

            QVariant freqIndexVar = menuSett->value( "Frequent" );
            int      freqIndex    = freqIndexVar.toStringList().indexOf( desktop->desktopName() );

            if ( freqIndex != -1 ) {
                mFrequent = QString( "frequent%1" ).arg( freqIndex );
            }

            QVariant favIndexVar = menuSett->value( "Favorites" );
            int      favIndex    = favIndexVar.toStringList().indexOf( desktop->desktopName() );

            if ( favIndex != -1 ) {
                mFavorite = QString( "favorite%1" ).arg( favIndex );
            }
        }

        /** Name of the application, Like okular */
        QString name() {
            return desktop->name();
        }

        /** Generic name, like Document Viewer */
        QString genericName() {
            return desktop->genericName();
        }

        /** Description, like Universal document viewer */
        QString description() {
            return desktop->description();
        }

        /** Category of the app, like Office */
        QString category() {
            return desktop->category();
        }

        /** Category of the app, like Office */
        QString filterString() {
            return desktop->name() + desktop->genericName() + desktop->description() + desktop->executable();
        }

        /** Desktop name, like org.kde.okular.desktop */
        QString desktopName() {
            return desktop->desktopName();
        }

        /** If this is a frequent app */
        QString frequent() {
            return mFrequent;
        }

        /** Set the frequency level */
        void setFrequent( QString level ) {
            mFrequent = level;
        }

        /** If this is a favorite app */
        QString favorite() {
            return mFavorite;
        }

        /** Set the favorite level */
        void setFavorite( QString level ) {
            mFavorite = level;
        }

        /** Application icon */
        QIcon icon() {
            QString mIcon = desktop->icon();

            if ( mIcon.startsWith( "/" ) ) {
                if ( QFileInfo( mIcon ).exists() ) {
                    return QIcon( mIcon );
                }

                else {
                    return QIcon::fromTheme( "application-x-executable" );
                }
            }

            return QIcon::fromTheme( mIcon, QIcon::fromTheme( "application-x-executable" ) );
        }

    private:
        DFL::XDG::DesktopFile *desktop;
        QIcon mIcon;

        QString mFrequent;
        QString mFavorite;
};


DesQ::Menu::Model::Model( QWidget *parent ) : QAbstractItemModel( parent ) {
    DFL::Inotify *fsw = new DFL::Inotify();

    connect( fsw, &DFL::Inotify::nodeCreated, this, &DesQ::Menu::Model::handlePath );
    connect( fsw, &DFL::Inotify::nodeChanged, this, &DesQ::Menu::Model::handlePath );
    connect( fsw, &DFL::Inotify::nodeDeleted, this, &DesQ::Menu::Model::handlePath );

    for ( QString path: desktopPaths ) {
        if ( QFile::exists( path ) ) {
            fsw->addWatch( path, DFL::Inotify::PathOnly );
        }
    }

    fsw->startWatch();

    connect( menuSett, &DFL::Settings::settingChanged, this, &DesQ::Menu::Model::updateData );
}


DesQ::Menu::Model::~Model() {
    qDeleteAll( mAppsList );
}


int DesQ::Menu::Model::rowCount( const QModelIndex& parent ) const {
    if ( parent.isValid() ) {
        return 0;
    }

    return mAppsList.count();
}


int DesQ::Menu::Model::columnCount( const QModelIndex& parent ) const {
    if ( parent.isValid() ) {
        return 0;
    }

    return 1;
}


QModelIndex DesQ::Menu::Model::index( int row, int column, const QModelIndex& parent ) const {
    /** None of our applications have parents */
    if ( parent.isValid() ) {
        return QModelIndex();
    }

    /** We cannot have an index beyond rowCount()/columnCount() */
    if ( row >= mAppsList.length() or column != 0 ) {
        return QModelIndex();
    }

    return createIndex( row, column, mAppsList.at( row ) );
}


QModelIndex DesQ::Menu::Model::parent( const QModelIndex& ) const {
    return QModelIndex();
}


QVariant DesQ::Menu::Model::data( const QModelIndex& idx, int role ) const {
    if ( not idx.isValid() ) {
        return QVariant();
    }

    Application *internal = (Application *)idx.internalPointer();

    switch ( role ) {
        case Qt::DecorationRole: {
            return internal->icon();
        }

        case Qt::DisplayRole: {
            return internal->name();
        }

        case Qt::FontRole: {
            return QFont( qApp->font().family(), 13 );
        }

        case Qt::ForegroundRole: {
            return qApp->palette().color( QPalette::Text );
        }

        case Qt::ToolTipRole: {
            return QString( "<b>%1</b><br>%2" ).arg( internal->name() ).arg( internal->description() );
        }

        /* For application search */
        case Qt::UserRole + 1: {
            return internal->filterString();
        }

        /* Category */
        case Qt::UserRole + 2: {
            return internal->category();
        }

        /* Desktop file name */
        case Qt::UserRole + 3: {
            return internal->desktopName();
        }

        /* App usage frequency */
        case Qt::UserRole + 4: {
            return internal->frequent();
        }

        /* User marked as favorite */
        case Qt::UserRole + 5: {
            return internal->favorite();
        }

        default: {
            return QVariant();
        }
    }

    return QVariant();
}


void DesQ::Menu::Model::reload() {
    loadDesktops();
}


Qt::ItemFlags DesQ::Menu::Model::flags( const QModelIndex& idx ) const {
    if ( idx.isValid() ) {
        return ( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
    }

    return Qt::NoItemFlags;
}


void DesQ::Menu::Model::launchApplication( const QModelIndex& idx ) {
    if ( not idx.isValid() ) {
        return;
    }

    DesQ::Environment proxyEnviron = DesQ::Utils::getNetworkProxy();

    QProcessEnvironment pe( QProcessEnvironment::systemEnvironment() );

    for ( QString key: proxyEnviron.keys() ) {
        pe.insert( key, proxyEnviron.value( key ) );
    }

    QString               desktopFileName = idx.data( Qt::UserRole + 3 ).toString();
    DFL::XDG::DesktopFile app( desktopFileName );

    if ( not app.startApplication( {}, "", pe ) ) {
        qDebug() << "Failed to launch application" << idx.data( Qt::DisplayRole ).toString() << desktopFileName;

        /** We don't want to update Frequent when we fail to launch an app */
        return;
    }

    QStringList frequents = menuSett->value( "Frequent" );

    if ( frequents.contains( desktopFileName ) ) {
        /** Remove it from current position */
        frequents.removeAll( desktopFileName );
    }

    /** Add it to first place */
    frequents.prepend( desktopFileName );

    menuSett->setValue( "Frequent", frequents );
}


void DesQ::Menu::Model::loadDesktops() {
    emit loading();

    mDesktopsList.clear();

    QStringList desktops;

    for ( QString path: desktopPaths ) {
        QDir appDir( path );
        appDir.setFilter( QDir::Files );
        appDir.setNameFilters( { "*.desktop" } );

        for ( QFileInfo app: appDir.entryInfoList() ) {
            desktops << app.fileName();
        }
    }

    /** Clear all the duplicates */
    desktops.removeDuplicates();

    beginResetModel();

    qDeleteAll( mAppsList );
    mAppsList.clear();

    /** Add all the apps to the model */
    for ( QString app: desktops ) {
        DFL::XDG::DesktopFile *desktopFile = new DFL::XDG::DesktopFile( app );

        if ( not desktopFile->isValid() ) {
            continue;
        }

        if ( desktopFile->type() != DFL::XDG::DesktopFile::Type::Application ) {
            continue;
        }

        if ( desktopFile->visible() ) {
            Application *newApp = new Application( desktopFile );

            mAppsList << newApp;
        }
    }

    for ( Application *app: mAppsList ) {
        mDesktopsList << app->desktopName();
    }

    endResetModel();

    emit loaded();

    loadCategories();
}


void DesQ::Menu::Model::loadCategories() {
    mCategories.clear();
    for ( Application *app: mAppsList ) {
        if ( not mCategories.contains( app->category() ) ) {
            mCategories << app->category();
        }

        if ( app->category() == "Uncategorized" ) {
            qDebug() << "Uncategorized app" << app->name() << app->desktopName();
        }
    }

    emit updateCategories( mCategories );
}


void DesQ::Menu::Model::handlePath( QString path ) {
    QFileInfo info( path );

    /** We entertain only files whose name ends with ".desktop" */
    if ( ( not info.isFile() ) or ( not path.endsWith( ".desktop" ) ) ) {
        return;
    }

    /** We need only the desktop name */
    QString desktopName = info.fileName().replace( ".desktop", "" );

    /**
     * We're loading the model: at least updating it.
     * This requires that we invalidate the proxy
     */
    emit loading();

    int idx = mDesktopsList.indexOf( desktopName );

    /** Remove the existing entry */
    if ( idx != -1 ) {
        beginRemoveRows( QModelIndex(), idx, idx );

        Application *app = mAppsList.takeAt( idx );
        delete app;

        mDesktopsList.removeAt( idx );

        endRemoveRows();
    }

    /** We may have this app at lower ranks: Attempt to load it. */
    DFL::XDG::DesktopFile *desktop = new DFL::XDG::DesktopFile( desktopName );

    bool display = desktop->isValid();

    display &= ( desktop->type() == DFL::XDG::DesktopFile::Type::Application );
    display &= desktop->visible();

    if ( display ) {
        Application *newApp = new Application( desktop );

        /** Append @app  at the end */
        idx = mAppsList.length();

        beginInsertRows( QModelIndex(), idx, idx );

        mDesktopsList.insert( idx, desktopName );
        mAppsList.insert( idx, newApp );

        endInsertRows();

        QModelIndex midx = index( idx, 0, QModelIndex() );
        emit        dataChanged( midx, midx );
    }

    /**
     * We're done loading/updating the model
     * We invalidate the proxy, and filter and sort it
     */
    emit loaded();

    /** Update the categories */
    loadCategories();
}


void DesQ::Menu::Model::updateData( QString key, QVariant value ) {
    /** Update the favorites */
    if ( key == "Favorite" ) {
        QStringList favorites = value.toStringList();

        /** First, remove all the favorite tags that are not in this list */
        for ( Application *app: mAppsList ) {
            /** If this app is tagged as favorite, but not in the current favorites list, remove it */
            if ( app->favorite().length() and not favorites.contains( app->desktopName() ) ) {
                app->setFavorite( QString() );
            }

            /** If this app is in the favorites list, tag it as favorite */
            if ( favorites.contains( app->desktopName() ) ) {
                QString fav = QString( "favorite%1" ).arg( favorites.indexOf( app->desktopName() ) );
                app->setFavorite( fav );
            }
        }
    }

    /** Update the frequents */
    else if ( key == "Frequent" ) {
        QStringList frequents = value.toStringList();

        /** First, remove all the frequent tags that are not in this list */
        for ( Application *app: mAppsList ) {
            /** If this app is tagged as favorite, but not in the current favorites list, remove it */
            if ( app->favorite().length() and not frequents.contains( app->desktopName() ) ) {
                app->setFrequent( QString() );
            }

            /** If this app is in the frequents list, tag it as frequent */
            if ( frequents.contains( app->desktopName() ) ) {
                QString freq = QString( "frequent%1" ).arg( frequents.indexOf( app->desktopName() ) );
                app->setFrequent( freq );
            }
        }
    }
}
