/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Apps
 * (https://gitlab.com/DesQ/DesQApps/DesQApps)
 * DesQ Apps a fast and light file manager for DesQ DE.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <desq/Utils.hpp>

namespace DesQ {
    namespace Menu {
        class Model;
    }
}

class Application;

class DesQ::Menu::Model : public QAbstractItemModel {
    Q_OBJECT;

    public:
        Model( QWidget *parent );
        ~Model();

        int rowCount( const QModelIndex& parent    = QModelIndex() ) const override;
        int columnCount( const QModelIndex& parent = QModelIndex() ) const override;

        QModelIndex index( int row, int col, const QModelIndex& parent = QModelIndex() ) const;

        QModelIndex parent( const QModelIndex& child ) const;

        /** We want to ensure thumbnailing is possible for image Apps */
        QVariant data( const QModelIndex& idx, int role = Qt::DisplayRole ) const override;

        /** Reload the model, applying name filters, dir filters, etc */
        void reload();

        /** Drag and Drop */
        Qt::ItemFlags flags( const QModelIndex& index ) const override;

        /** To toggle hidden Apps */
        void launchApplication( const QModelIndex& idx );

    private:
        /** Parse and load all the desktops */
        void loadDesktops();

        /** Iterate through all the applications and update the category list */
        void loadCategories();

        /** Handle a added/changed/removed path: They all are handled identically */
        void handlePath( QString );

        /** Update Favorites and Frequent */
        void updateData( QString key, QVariant value );

        QList<Application *> mAppsList;
        QStringList mDesktopsList;
        QStringList mCategories;

        /** Location of the Desktop Apps */
        const QStringList desktopPaths = {
            QDir::home().filePath( ".local/share/flatpak/exports/share/applications/" ),

            QDir::home().filePath( ".local/share/applications/" ),
            QDir::home().filePath( ".local/share/applications/wine/" ),
            QDir::home().filePath( ".local/share/games/" ),

            "/var/lib/flatpak/exports/share/applications/",
            "/usr/local/share/applications/",
            "/usr/local/share/games/",

            "/usr/share/applications/",
            "/usr/share/games/"
        };

    Q_SIGNALS:
        void loading();
        void loaded();

        void updateCategories( QStringList );
};
