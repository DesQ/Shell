/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "CategoryWidget.hpp"

DesQ::Menu::CategoryWidget::CategoryWidget( QWidget *parent ) : QWidget( parent ) {
    setFixedHeight( 64 );

    categories = QStringList(
        {
            "Frequently Used",
            "Favorites",
            "sep",
            "Accessories", "Development",
            "Education", "Games",
            "Graphics", "Internet",
            "Multimedia", "Office",
            "Science & Math", "Settings",
            "System", "Wine",
            "sep",
            "All Applications",
            "Uncategorized"
        }
    );

    QStringList categoryIcons = {
        "applications-all",           "preferences-desktop-default-applications",
        "",
        "applications-utilities",     "applications-development",
        "applications-education",     "applications-games",
        "applications-graphics",      "applications-internet",
        "applications-multimedia",    "applications-office",
        "applications-science",       "preferences",
        "applications-system",        "wine",
        "",
        "preferences-system-windows",
        "applications-other"
    };

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins( 8, 8, 8, 8 ) );
    lyt->setSpacing( 8 );

    lyt->addStretch();

    buttons = new QButtonGroup();
    buttons->setExclusive( true );

    for ( int i = 0; i < categories.count(); i++ ) {
        if ( categories[ i ] == "sep" ) {
            QLabel *sep = new QLabel( QByteArray( "\xE2\xAC\x9B", 3 ) );
            sep->setFixedSize( 48, 48 );
            sep->setAlignment( Qt::AlignCenter );
            lyt->addWidget( sep );
        }

        else {
            QToolButton *btn = new QToolButton();
            btn->setIcon( QIcon::fromTheme( categoryIcons.value( i ) ) );
            btn->setAutoRaise( true );
            btn->setFixedSize( 48, 48 );
            btn->setIconSize( QSize( 40, 40 ) );
            btn->setCheckable( true );
            btn->setToolTip( categories[ i ] );

            if ( categories[ i ] == "All Applications" ) {
                btn->setChecked( true );
            }

            else {
                btn->setChecked( false );
            }

            buttons->addButton( btn, i );

            connect(
                btn, &QToolButton::clicked, [ = ] () {
                    emit loadCategory( categories.at( i ) );
                }
            );

            lyt->addWidget( btn );
        }
    }

    lyt->addStretch();

    setLayout( lyt );
}


void DesQ::Menu::CategoryWidget::setCategories( QStringList newCats ) {
    newCats << "All Applications";      // To be always shown
    newCats << "Frequently Used";       // To be always shown
    newCats << "Favorites";             // To be always shown

    for ( int i = 0; i < categories.count(); i++ ) {
        if ( categories[ i ] == "sep" ) {
            continue;
        }

        if ( newCats.contains( categories[ i ] ) ) {
            buttons->button( i )->show();
        }

        else {
            buttons->button( i )->hide();

            qDebug() << "Hiding" << categories[ i ];

            if ( buttons->button( i )->isChecked() ) {
                // emit loadCategory( "Frequently Used" );
                buttons->button( 0 )->click();
            }
        }
    }
}


void DesQ::Menu::CategoryWidget::selectCategory( QString category ) {
    if ( categories.contains( category ) ) {
        buttons->button( categories.indexOf( category ) )->setChecked( true );
    }
}


void DesQ::Menu::CategoryWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( childAt( mEvent->pos() ) == this ) {
        emit closeMenu();
    }

    QWidget::mouseReleaseEvent( mEvent );
}


void DesQ::Menu::CategoryWidget::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Slash ) {
        emit focusSearch();
        kEvent->accept();

        return;
    }

    QWidget::keyPressEvent( kEvent );
}
