/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ViewsImpl.hpp"
#include <DFColorUtils.hpp>


void AppsDelegate::paint( QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const {
    QRectF appRect = QRectF( QPointF( 0.0, 0.0 ), option.rect.size() );

    QImage   img( appRect.size().toSize(), QImage::Format_ARGB32 );
    QPainter p( &img );

    img.fill( Qt::transparent );
    p.setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );
    p.setFont( index.data( Qt::FontRole ).value<QFont>() );

    if ( option.state & QStyle::State_MouseOver ) {
        p.save();
        p.setPen( option.palette.color( QPalette::Highlight ) );
        QColor bg = DFL::ColorUtils::tint( QColor( Qt::transparent ), option.palette.color( QPalette::Highlight ), 0.2 );
        p.setBrush( bg );
        p.drawRoundedRect( appRect.adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );
        p.restore();
    }

    else if ( option.state & QStyle::State_Selected ) {
        p.save();
        p.setPen( option.palette.color( QPalette::Highlight ) );
        QColor bg = DFL::ColorUtils::tint( QColor( Qt::transparent ), option.palette.color( QPalette::Highlight ), 0.4 );
        p.setBrush( bg );
        p.drawRoundedRect( appRect.adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );
        p.restore();
    }

    QIcon   icon = index.data( Qt::DecorationRole ).value<QIcon>();
    QPixmap pix  = icon.pixmap( 96 );

    if ( pix.width() < 96 and pix.height() < 96 ) {
        pix = pix.scaled( QSize( 96, 96 ), Qt::KeepAspectRatio, Qt::SmoothTransformation );
    }

    /** Center horizontally. 10 px margin on top */
    qreal imgX = appRect.x() + ( appRect.width() - pix.width() ) / 2;
    qreal imgY = appRect.y() + 10.0;

    p.drawPixmap( QPointF( imgX, imgY ), pix );

    /** Text is auto-centered; just get the text rect */
    qreal  txtX = appRect.x();
    qreal  txtY = appRect.y() + 10 + 96 + 10;
    QRectF txtRect( txtX, txtY, appRect.width(), appRect.height() - 116 );

    QString text = index.data( Qt::DisplayRole ).toString();
    QString txt  = p.fontMetrics().elidedText( text, option.textElideMode, appRect.width() - 10 );

    p.save();
    p.setPen( option.palette.color( QPalette::Text ) );
    p.drawText( txtRect, Qt::AlignCenter, txt );
    p.restore();

    p.end();

    painter->drawImage( option.rect, img );
}


QSize AppsDelegate::sizeHint( const QStyleOptionViewItem&, const QModelIndex& ) const {
    return QSize( 171, 156 );
}
