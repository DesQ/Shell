/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "AppsView.hpp"
#include "AppsModel.hpp"
#include "ViewsImpl.hpp"

#include <desq/Utils.hpp>

DesQ::Menu::View::View( QWidget *parent ) : QListView( parent ) {
    setViewMode( QListView::IconMode );
    setFlow( QListView::LeftToRight );

    setWrapping( true );
    setWordWrap( false );

    setTextElideMode( Qt::ElideRight );
    setResizeMode( QListView::Adjust );
    setMovement( QListView::Static );

    setIconSize( QSize( 96, 96 ) );
    setGridSize( QSize( 171, 156 ) );
    setUniformItemSizes( true );

    setFrameStyle( QFrame::NoFrame );
    setEditTriggers( QListView::NoEditTriggers );
    viewport()->setAutoFillBackground( false );

    setViewportMargins( 210, 5, 210, 5 );

    setItemDelegate( new AppsDelegate() );
}


DesQ::Menu::View::~View() {
    disconnect();

    delete appsModel;
    delete proxyModel;
}


void DesQ::Menu::View::loadCategory( QString category ) {
    if ( category == "Frequently Used" ) {
        proxyModel->setFilterRole( Qt::UserRole + 4 );
        proxyModel->setFilterWildcard( "frequent" );

        proxyModel->setSortRole( Qt::UserRole + 4 );
        proxyModel->sort( 0 );
    }

    else if ( category == "Favorites" ) {
        proxyModel->setFilterRole( Qt::UserRole + 5 );
        proxyModel->setFilterWildcard( "favorite" );

        proxyModel->setSortRole( Qt::UserRole + 5 );
        proxyModel->sort( 0 );
    }

    else {
        proxyModel->setFilterRole( Qt::UserRole + 2 );

        proxyModel->setSortRole( Qt::DisplayRole );
        proxyModel->sort( 0 );

        if ( category == "All Applications" ) {
            proxyModel->setFilterFixedString( "" );
            qDebug() << "Loading all applications";
        }

        else {
            proxyModel->setFilterFixedString( category );
        }
    }

    setFocus();
}


void DesQ::Menu::View::setFilter( QString filter ) {
    proxyModel->setFilterRole( Qt::UserRole + 1 );
    proxyModel->setFilterWildcard( filter );

    proxyModel->setSortRole( Qt::DisplayRole );
    proxyModel->sort( 0 );
}


void DesQ::Menu::View::setupModel( DesQ::Menu::Model *model ) {
    appsModel = model;

    proxyModel = new QSortFilterProxyModel( this );

    /** Filter the app by name + genericName + description + executable */
    proxyModel->setFilterCaseSensitivity( Qt::CaseInsensitive );
    proxyModel->setFilterRole( Qt::UserRole + 1 );

    /** Dynamically sort and filter */
    proxyModel->setDynamicSortFilter( true );

    proxyModel->setSourceModel( appsModel );
    setModel( proxyModel );

    connect( appsModel, &DesQ::Menu::Model::loading,          proxyModel, &QSortFilterProxyModel::invalidate );
    connect( appsModel, &DesQ::Menu::Model::loaded,           proxyModel, &QSortFilterProxyModel::invalidate );

    connect( appsModel, &DesQ::Menu::Model::updateCategories, this,       &DesQ::Menu::View::updateCategories );

    connect(
        this, &QListView::clicked, [ = ]( const QModelIndex& index ) {
            appsModel->launchApplication( index );
            emit closeMenu();
        }
    );

    connect(
        this, &QListView::activated, [ = ]( const QModelIndex& index ) {
            appsModel->launchApplication( index );
            emit closeMenu();
        }
    );
}


void DesQ::Menu::View::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( not indexAt( mEvent->pos() ).isValid() ) {
        emit closeMenu();
    }

    QListView::mouseReleaseEvent( mEvent );
}


void DesQ::Menu::View::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( viewport() );

    painter.setPen( Qt::gray );
    painter.drawLine( geometry().topRight(), geometry().bottomRight() );
    painter.end();

    QListView::paintEvent( pEvent );
}


void DesQ::Menu::View::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Slash ) {
        emit focusSearch();
        kEvent->accept();

        return;
    }

    QListView::keyPressEvent( kEvent );
}


void DesQ::Menu::View::focusInEvent( QFocusEvent *fEvent ) {
    if ( not selectionModel()->hasSelection() ) {
        setCurrentIndex( model()->index( 0, 0 ) );
    }

    QListView::focusInEvent( fEvent );
}
