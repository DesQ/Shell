/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <QtDBus>
#include "Global.hpp"
#include "PagerWidget.hpp"

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <desqui/ShellPlugin.hpp>

WorkspaceWidget::WorkspaceWidget( WorkSpace ws, int number, QWidget *parent ) : QWidget( parent ) {
    mCurWS  = ws;
    mNumber = number;
    setFixedSize( QSize( 32, 24 ) );

    setMouseTracking( true );
}


WorkSpace WorkspaceWidget::workSpace() {
    return mCurWS;
}


void WorkspaceWidget::highlight( bool yes ) {
    mHighlight = yes;
    repaint();
}


void WorkspaceWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void WorkspaceWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( ( mEvent->button() == Qt::LeftButton ) and mPressed ) {
        /**
         * If we're highlighted, then this is the current workspace.
         * We toggle "ShowDesktop"
         */
        if ( mHighlight ) {
            emit showDesktop( mCurWS );
        }

        else {
            emit switchWorkSpace( mCurWS );
        }
    }

    repaint();
    mEvent->accept();
}


// void WorkspaceWidget::enterEvent( QEvent *event ) {
//     // emit showWorkSpaceInfo( mCurWS );
//
//     QWidget::enterEvent( event );
// }


void WorkspaceWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();

    if ( mHighlight ) {
        QColor clr( palette().color( QPalette::Highlight ) );
        clr = clr.darker();
        painter.setPen( QPen( clr, 1.0 ) );
        clr = clr.lighter();
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    else {
        QColor clr( palette().color( QPalette::Window ) );
        painter.setPen( QPen( clr, 1.0 ) );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    painter.restore();

    painter.save();

    if ( mHighlight ) {
        QFont f( font() );
        f.setWeight( QFont::Bold );
        painter.setFont( f );
    }

    painter.drawText( rect(), Qt::AlignCenter, QString::number( mNumber ) );
    painter.restore();

    painter.end();

    pEvent->accept();
}


PagerPluginWidget::PagerPluginWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    /** Defaults, so that we don't have a crash */
    currentWset         = 1;
    currentGrid.rows    = 1;
    currentGrid.columns = 1;
    currentWS.row       = 0;
    currentWS.column    = 0;

    wsLyt = new QGridLayout();
    setLayout( wsLyt );

    connect(
        this, &DesQ::Shell::PluginWidget::widgetScreenChanged, [ = ] ( QScreen *scrn ) {
            /** First get the output information */
            QJsonArray opInfo = wfIpc->getOutputs();

            /** And retrieve the output-id of the current screen */
            for ( uint i = 0; i < opInfo.size(); i++ ) {
                /** Get the name */
                QString opName = opInfo[ i ][ "name" ];

                /** If this is the screen we are rendering */
                if ( opName.c_str() == scrn->name() ) {
                    /** Save the output id */
                    mScreenId = opInfo[ i ][ "id" ];

                    /** Now we can set the workspace-set */
                    currentWset = opInfo[ i ][ "wset-index" ];

                    /** Next, we set the workspace grid */
                    currentGrid.rows    = opInfo[ i ][ "workspace" ][ "grid_height" ];
                    currentGrid.columns = opInfo[ i ][ "workspace" ][ "grid_width" ];

                    /** Finally, we set the current workspace */
                    currentWS.row    = opInfo[ i ][ "workspace" ][ "y" ];
                    currentWS.column = opInfo[ i ][ "workspace" ][ "x" ];

                    populateLayout( currentGrid );
                    highlightWorkspace( currentWS );

                    break;
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceChanged, [ = ] ( nlohmann::json wsInfo ) {
            // std::cout << wsInfo.dump( 4 ) << std::endl;

            if ( wsInfo[ "output-data" ][ "id" ] == mScreenId ) {
                currentWS.row    = wsInfo[ "new-workspace" ][ "y" ];
                currentWS.column = wsInfo[ "new-workspace" ][ "x" ];
                highlightWorkspace( currentWS );
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceSetChanged, [ = ] ( nlohmann::json wssInfo ) {
            // std::cout << wssInfo.dump( 4 ) << std::endl;

            if ( wssInfo[ "output-data" ][ "id" ] == mScreenId ) {
                /** Now we can set the workspace-set */
                currentWset = wssInfo[ "new-wset-data" ][ "index" ];

                /** Next, we set the workspace grid */
                currentGrid.rows    = wssInfo[ "new-wset-data" ][ "workspace" ][ "grid_height" ];
                currentGrid.columns = wssInfo[ "new-wset-data" ][ "workspace" ][ "grid_width" ];

                /** Finally, we set the current workspace */
                currentWS.row    = wssInfo[ "new-wset-data" ][ "workspace" ][ "y" ];
                currentWS.column = wssInfo[ "new-wset-data" ][ "workspace" ][ "x" ];

                populateLayout( currentGrid );
                highlightWorkspace( currentWS );
            }
        }
    );

    /** So that we know exactly where the mouse is */
    setMouseTracking( true );

    popup = new QLabel();
    popup->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );
    popup->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
}


PagerPluginWidget::~PagerPluginWidget() {
    qDeleteAll( workspaceList );

    delete wsLyt;
    delete popup;
}


void PagerPluginWidget::populateLayout( WorkSpaceGrid grid ) {
    /* Delete the previously added WorkSpace widgets */
    if ( wsLyt->count() ) {
        qDeleteAll( wsLyt->findChildren<WorkspaceWidget *>() );
    }

    if ( workspaceList.count() ) {
        qDeleteAll( workspaceList );
    }

    workspaceList.clear();

    for ( int r = 0; r < grid.rows; r++ ) {
        for ( int c = 0; c < grid.columns; c++ ) {
            int             wsCount = r * grid.columns + c + 1;
            WorkspaceWidget *wsw    = new WorkspaceWidget( { r, c }, wsCount, this );
            wsLyt->addWidget( wsw, r, c );
            workspaceList << wsw;

            connect( wsw, &WorkspaceWidget::showWorkSpaceInfo, this, &PagerPluginWidget::showWorkSpaceInfo );

            connect(
                wsw, &WorkspaceWidget::switchWorkSpace, [ = ] ( WorkSpace ws ) {
                    wfIpc->switchToWorkspace( mScreenId, ws.row, ws.column );
                }
            );

            connect(
                wsw, &WorkspaceWidget::showDesktop, [ = ] ( WorkSpace ) {
                    nlohmann::json request;
                    request[ "method" ] = "wm-actions/toggle_showdesktop";
                    request[ "data" ][ "output-id" ] = mScreenId;
                    wfIpc->genericRequest( request );
                }
            );

            connect(
                wsw, &WorkspaceWidget::hideTooltip, [ = ]() {
                    if ( popup->isVisible() ) {
                        popup->close();
                    }
                }
            );
        }
    }
}


void PagerPluginWidget::highlightWorkspace( WorkSpace ws ) {
    for ( WorkspaceWidget *wsw: workspaceList ) {
        if ( wsw->workSpace() == ws ) {
            wsw->highlight( true );
        }

        else {
            wsw->highlight( false );
        }
    }
}


void PagerPluginWidget::updatePopup( uint ) {
    if ( popup and popup->isVisible() ) {
        QPoint          cursor = mapFromGlobal( QCursor::pos() );
        WorkspaceWidget *ww    = qobject_cast<WorkspaceWidget *>( childAt( cursor ) );

        if ( ww ) {
            WorkSpace ws = ww->workSpace();
            showWorkSpaceInfo( ws );
        }
    }
}


void PagerPluginWidget::showWorkSpaceInfo( WorkSpace ws ) {
}
