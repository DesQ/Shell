/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <unistd.h>

#include <QtWidgets>
#include <QSlider>
#include <QSvgWidget>
#include <QGraphicsOpacityEffect>
#include <QtDBus>

#include "BrightnessWidget.hpp"

#include <desq/Utils.hpp>
#include <DFLogin1.hpp>
#include <desqui/ShellPlugin.hpp>

BrightnessWidget::BrightnessWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    /** Login1 Impl */
    login1 = new DFL::Login1( this );

    /** Brightness Timer */
    changeTimer = new QBasicTimer();

    /** Get the SVG Data */
    QFile svgf( ":/monitor.svg" );

    svgf.open( QFile::ReadOnly );
    svg = svgf.readAll();
    svgf.close();

    QFileSystemWatcher *fsw = new QFileSystemWatcher( this );
    connect(
        fsw, &QFileSystemWatcher::fileChanged, [ = ] ( QString fileName ) {
            QStringList parts = fileName.split( "/", Qt::SkipEmptyParts );
            QString device    = parts[ 3 ];     // amdgpu_bl1 or asus::kbd_backlight

            QFile blFile( fileName );

            if ( blFile.open( QFile::ReadOnly ) ) {
                devices[ device ]->blockSignals( true );
                devices[ device ]->setValue( blFile.readAll().toInt() );
                devices[ device ]->blockSignals( false );
                blFile.close();
            }
        }
    );

    /** List all the devices */
    QDirIterator it( "/sys/class/backlight/", QDir::Dirs | QDir::NoDotAndDotDot );
    QStringList  backlightDevices;

    while ( it.hasNext() ) {
        backlightDevices << it.next();
    }

    for ( QString name: QDir( "/sys/class/leds/" ).entryList( QDir::NoDotAndDotDot | QDir::Dirs ) ) {
        if ( name.toLower().contains( "kbd_backlight" ) ) {
            backlightDevices << "/sys/class/leds/" + name;
        }
    }

    /** Layout in which the widgets will be placed */
    QGridLayout *lyt = new QGridLayout( this );
    int         i    = 0;

    for ( QString devicePath: backlightDevices ) {
        QSlider   *dev = new QSlider( Qt::Horizontal );
        QFileInfo info( devicePath );
        devices[ info.fileName() ] = dev;

        /** Get max_brightness and set the range */
        QFile blMaxFile( devicePath + "/max_brightness" );
        int   minVal = ( devicePath.contains( "kbd_backlight" ) ? 0 : 1 );
        int   maxVal = 100;

        if ( blMaxFile.open( QFile::ReadOnly ) ) {
            maxVal = blMaxFile.readAll().toInt();
            blMaxFile.close();
        }

        dev->setRange( minVal, maxVal );

        /**
         * Get the current brightness.
         * First attempt actual_brightness, then brightness.
         * For kbd_backlight, try brightness_hw_changed, and then brightness
         * In each case, add these files to watcher.
         */
        bool    valueSet   = false;
        QString blFileName = devicePath + "/" + ( devicePath.contains( "kbd_backlight" ) ? "brightness_hw_changed" : "actual_brightness" );

        if ( QFile::exists( blFileName ) ) {
            fsw->addPath( blFileName );

            QFile blFile( blFileName );

            if ( blFile.open( QFile::ReadOnly ) ) {
                dev->setValue( blFile.readAll().toInt() );
                blFile.close();
                valueSet = true;
            }
        }

        blFileName = devicePath + "/" + "brightness";
        fsw->addPath( blFileName );

        if ( valueSet == false ) {
            QFile blFile( blFileName );

            if ( blFile.open( QFile::ReadOnly ) ) {
                dev->setValue( blFile.readAll().toInt() );
                blFile.close();
            }

            else {
                dev->setValue( 100 );
            }
        }

        /** Change the brightness when the slider position is changed */
        connect(
            dev, &QSlider::valueChanged, [ = ] ( int value ) {
                changeBrightness( devicePath, value );
            }
        );

        QSvgWidget *display = new QSvgWidget( this );
        display->setFixedSize( 24, 24 );
        displayWidgets[ info.fileName() ] = display;

        QLabel *lbl = new QLabel( info.fileName() );

        for ( QString devPath: devices.keys() ) {
            displayWidgets.value( devPath )->load( QByteArray( svg ) );
            lyt->addWidget( display, 2 * i,     0 );
            lyt->addWidget( lbl,     2 * i,     1, Qt::AlignLeft | Qt::AlignVCenter );
            lyt->addWidget( dev,     2 * i + 1, 0, 1, 2 );

            i++;
        }
    }

    /** If we don't have any devices */
    if ( devices.count() == 0 ) {
        lyt->addWidget( new QLabel( "No backlight devices." ), 0, 0, Qt::AlignCenter );
    }

    setLayout( lyt );

    opacity = new QGraphicsOpacityEffect( this );
    opacity->setOpacity( 0.4 );
    setGraphicsEffect( opacity );

    connect(
        this, &DesQ::Shell::PluginWidget::entered, [ = ] () {
            opacity->setOpacity( 1.0 );
        }
    );

    connect(
        this, &DesQ::Shell::PluginWidget::exited, [ = ] () {
            opacity->setOpacity( 0.4 );
        }
    );
}


void BrightnessWidget::changeBrightness( QString dev, int value ) {
    toBeChanged[ dev ] = value;

    /** Start the timer if it's not running */
    if ( changeTimer->isActive() == false ) {
        changeTimer->start( 250, this );
    }
}


void BrightnessWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == changeTimer->timerId() ) {
        changeTimer->stop();

        for ( QString dev: toBeChanged.keys() ) {
            QStringList parts = dev.split( "/", Qt::SkipEmptyParts );
            login1->setBrightness( parts[ 2 ], parts[ 3 ], toBeChanged[ dev ] );

            QSlider *slider = devices[ dev ];

            if ( slider ) {
                qreal pc = 100.0 * ( devices[ dev ]->value() - devices[ dev ]->minimum() ) / ( devices[ dev ]->maximum() - devices[ dev ]->minimum() );

                displayWidgets.value( dev )->load( QByteArray( svg ).replace( QByteArray( "###" ), QByteArray::number( pc / 100.0, 'f', 2 ) ) );
            }
        }
    }

    QWidget::timerEvent( tEvent );
}
