/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/


/**
 * MoonRise Calculator: https://github.com/signetica/MoonRise
 **/

#include <curl/curl.h>

#include <QtWidgets>
#include <desqui/ShellPlugin.hpp>
#include <DFHjsonParser.hpp>

#include "WeatherWidget.hpp"
#include "WeatherData.hpp"

const QString GeoUrl     = "https://geocoding-api.open-meteo.com/v1/search?name=%1&count=100&language=en&format=json";
const QString WeatherUrl = "https://api.open-meteo.com/v1/forecast?latitude=%1&longitude=%2&daily=%3&timezone=%4&forecast_days=1&current_weather=true";

/** Parameters we're looking for. For the basic plugin, this is fixed. */
const QString WeatherParamsDaily = "temperature_2m_max,temperature_2m_min";

class WeatherTask : public QObject, public QRunnable {
    Q_OBJECT;

    public:
        WeatherTask() {
            mUrl = QString();
            setAutoDelete( false );
        }

        void setUrl( QString url ) {
            mUrl = url;
        }

        void run() {
            if ( mUrl.length() == 0 ) {
                emit weather( QString() );
                return;
            }

            CURL *handle = curl_easy_init();

            QString response;

            QSettings network( "DesQ", "Network" );

            if ( network.value( "ProxyType" ).toInt() == 4 ) {
                curl_easy_setopt( handle, CURLOPT_PROXY, network.value( "HTTPProxy" ).toString().toUtf8().data() );
            }

            /** Forcibly set Proxy to "", so that it does not read from env */
            else if ( network.value( "ProxyType" ).toInt() == 0 ) {
                curl_easy_setopt( handle, CURLOPT_PROXY, "" );
            }

            /** Curl does not support PAC Url */
            else if ( network.value( "ProxyType" ).toInt() == 3 ) {
                emit weather( QString() );
                return;
            }

            curl_easy_setopt( handle, CURLOPT_URL, mUrl.toUtf8().data() );
            curl_easy_setopt(
                handle, CURLOPT_WRITEFUNCTION, +[] ( char *buffer, size_t size, size_t nmemb, QString * str )->size_t {
                    size_t newLength = size * nmemb;
                    try {
                        str->append( QString::fromUtf8( buffer, newLength ) );
                    } catch ( std::bad_alloc& e ) {
                        return 0;
                    }

                    return newLength;
                }
            );

            curl_easy_setopt( handle, CURLOPT_WRITEDATA, &response );

            /** Should not take more than 10s to get the info */
            curl_easy_setopt( handle, CURLOPT_TIMEOUT,   10 );

            /** Perform the request */
            CURLcode ret = curl_easy_perform( handle );

            Q_UNUSED( ret );

            /** Cleanup the handle */
            curl_easy_cleanup( handle );

            emit weather( response );
        }

    private:
        QString mUrl;

    Q_SIGNALS:
        void weather( QString data );
};


WeatherWidget::WeatherWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    weatherSett = new DFL::Settings( QDir::home().filePath( ".config/DesQ/WeatherInfo.conf" ) );
    QStringList keys = weatherSett->allKeys();

    icon = new QLabel();
    icon->setFixedSize( QSize( 48, 48 ) );
    icon->setAlignment( Qt::AlignCenter );
    icon->setToolTip( "Click to refresh" );
    icon->setPixmap( QIcon( ":/missing-data.svg" ).pixmap( 48, 48 ) );

    place = new QLabel();
    place->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
    place->setText( "<big><b>Lost in space<b/></big><br><b>Aliens alert!!</b>" );

    temp = new QLabel();
    temp->setFont( QFont( font().family(), font().pointSize() * 2, QFont::Bold, false ) );
    temp->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
    temp->setText( "-- °C" );

    tempRange = new QLabel();
    tempRange->setAlignment( Qt::AlignCenter );
    tempRange->setText( "-- °C<br>-- °C" );

    weatherTask = new WeatherTask();
    connect( weatherTask, &WeatherTask::weather, this, &WeatherWidget::updateWeather );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( icon,      0, 0, Qt::AlignCenter );
    lyt->addWidget( place,     0, 1, Qt::AlignCenter );
    lyt->addWidget( tempRange, 1, 0, Qt::AlignCenter );
    lyt->addWidget( temp,      1, 1, Qt::AlignCenter );

    setLayout( lyt );

    /** Check if we have the place name */
    if ( keys.contains( "Latitude" ) and keys.contains( "Longitude" ) ) {
        placeInit = true;
    }

    QTimer::singleShot(
        100,
        this,
        &WeatherWidget::getCurrentWeatherInfo
    );

    hourlyTimer = new QBasicTimer();

    opacity = new QGraphicsOpacityEffect( this );
    opacity->setOpacity( 0.4 );
    setGraphicsEffect( opacity );

    connect(
        this, &DesQ::Shell::PluginWidget::entered, [ = ] () {
            opacity->setOpacity( 1.0 );
        }
    );

    connect(
        this, &DesQ::Shell::PluginWidget::exited, [ = ] () {
            opacity->setOpacity( 0.4 );
        }
    );

    setFixedWidth( 200 );
    setMinimumHeight( 100 );

    setMouseTracking( true );

    getCurrentWeatherInfo();
}


void WeatherWidget::getCurrentWeatherInfo() {
    hourlyTimer->stop();

    icon->setPixmap( QIcon( ":/missing-data.svg" ).pixmap( 48, 48 ) );
    place->setText( "<big><b>Lost in space<b/></big><br><b>Aliens alert!!</b>" );
    temp->setText( "-- °C" );
    tempRange->setText( "-- °C<br>-- °C" );

    QCoreApplication::processEvents();

    /** Place is ready */
    if ( placeInit == false ) {
        if ( getPlaceInfo() == false ) {
            return;
        }

        placeInit = true;
    }

    double  latitude  = weatherSett->value( "Latitude" );
    double  longitude = weatherSett->value( "Longitude" );
    QString timeZone  = weatherSett->value( "TimeZone" );

    weatherTask->setUrl( WeatherUrl.arg( latitude ).arg( longitude ).arg( WeatherParamsDaily ).arg( timeZone ) );
    QThreadPool::globalInstance()->start( weatherTask );
}


void WeatherWidget::updateWeather( QString response ) {
    if ( response.isEmpty() ) {
        return;
    }

    QString     placeStr       = weatherSett->value( "Place" );
    QVariantMap weatherInfoMap = DFL::Config::Hjson::readConfigFromString( response );

    QVariantMap dailyInfo   = weatherInfoMap[ "daily" ].toMap();
    QVariantMap currentInfo = weatherInfoMap[ "current_weather" ].toMap();

    qreal tempMin = dailyInfo[ "temperature_2m_min" ].toList()[ 0 ].toDouble();
    qreal tempMax = dailyInfo[ "temperature_2m_max" ].toList()[ 0 ].toDouble();

    qreal       curTemp  = currentInfo[ "temperature" ].toDouble();
    QStringList iconText = WeatherInfoMap[ currentInfo[ "weathercode" ].toInt() ];
    bool        isDay    = ( currentInfo[ "is_day" ].toInt() ? true : false );

    icon->setPixmap( QIcon( ":/" + ( isDay ? iconText[ 1 ] : iconText[ 2 ] ) + ".svg" ).pixmap( 48 ) );
    place->setText( QString( "<big><b>%1<b/></big><br><b>%2</b>" ).arg( placeStr ).arg( iconText[ 0 ] ) );
    temp->setText( QString( "%1 °C" ).arg( curTemp, 0, 'f', 1 ) );
    tempRange->setText( QString( "%1 °C<br>%2 °C" ).arg( tempMin, 0, 'f', 1 ).arg( tempMax, 0, 'f', 1 ) );

    /** Get the current time stamp */
    QDateTime dt = QDateTime::fromString( currentInfo[ "time" ].toString(), "yyyy-MM-ddTHH:mm" );

    /** Schedule the next update at 1h 1m from the current time stamp */
    int ms = QDateTime::currentDateTime().msecsTo( dt.addSecs( 3660 ) );

    /** Start the timer */
    hourlyTimer->start( ms, Qt::VeryCoarseTimer, this );
}


bool WeatherWidget::getPlaceInfo() {
    LocationDialog *locDlg = new LocationDialog();

    locDlg->exec();

    QVariantList location = locDlg->location();

    /** We have exactly 4 entries: name, latitude, longitude, timezone */
    if ( location.length() == 4 ) {
        weatherSett->setValue( "Place",     location[ 0 ] );
        weatherSett->setValue( "Latitude",  location[ 1 ] );
        weatherSett->setValue( "Longitude", location[ 2 ] );
        weatherSett->setValue( "TimeZone",  location[ 3 ] );

        return true;
    }

    return false;
}


void WeatherWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        if ( icon->underMouse() ) {
            getCurrentWeatherInfo();
        }

        else if ( place->underMouse() ) {
            if ( getPlaceInfo() ) {
                getCurrentWeatherInfo();
            }
        }
    }

    QWidget::mousePressEvent( mEvent );
}


void WeatherWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == hourlyTimer->timerId() ) {
        getCurrentWeatherInfo();
    }
}


LocationDialog::LocationDialog() : QDialog() {
    setWindowTitle( "Selection location" );
    setWindowIcon( QIcon::fromTheme( "weather-few-clouds" ) );

    locTask = new WeatherTask();
    connect( locTask, &WeatherTask::weather, this, &LocationDialog::updateLocations );

    text       = new QLabel( "Enter the location:" );
    locationLE = new QLineEdit( this );

    QToolButton *searchBtn = new QToolButton( this );
    searchBtn->setIcon( QIcon::fromTheme( "edit-find" ) );

    results = new QListWidget( this );

    selectBtn = new QToolButton( this );
    selectBtn->setIcon( QIcon::fromTheme( "dialog-ok-apply" ) );

    QToolButton *cancelBtn = new QToolButton( this );
    cancelBtn->setIcon( QIcon::fromTheme( "dialog-cancel" ) );
    connect( cancelBtn, &QToolButton::clicked, this, &QDialog::reject );

    QHBoxLayout *locLyt = new QHBoxLayout();
    locLyt->setContentsMargins( QMargins() );
    locLyt->addWidget( locationLE );
    locLyt->addWidget( searchBtn );

    QHBoxLayout *btnLyt = new QHBoxLayout();
    btnLyt->setContentsMargins( QMargins() );
    btnLyt->addStretch();
    btnLyt->addWidget( selectBtn );
    btnLyt->addWidget( cancelBtn );

    QVBoxLayout *lyt = new QVBoxLayout( this );
    lyt->addWidget( text );
    lyt->addLayout( locLyt );
    lyt->addWidget( results );
    lyt->addLayout( btnLyt );

    setLayout( lyt );

    connect( locationLE, &QLineEdit::returnPressed, this, &LocationDialog::searchLocation );
    connect( searchBtn,  &QToolButton::clicked,     this, &LocationDialog::searchLocation );

    connect(
        selectBtn, &QToolButton::clicked, [ = ] () {
            selectedLocation.clear();

            if ( results->count() ) {
                QListWidgetItem *selected = results->currentItem();

                if ( selected->text().length() ) {
                    selectedLocation << selected->data( Qt::UserRole + 0 );
                    selectedLocation << selected->data( Qt::UserRole + 1 );
                    selectedLocation << selected->data( Qt::UserRole + 2 );
                    selectedLocation << selected->data( Qt::UserRole + 3 );
                }
            }

            close();
        }
    );
}


QVariantList LocationDialog::location() {
    return selectedLocation;
}


void LocationDialog::searchLocation() {
    setDisabled( true );
    QCoreApplication::processEvents();

    /** Reset the results pane */
    results->clear();

    /** Get the list of places and the latitude and longitude */
    QString place = locationLE->text();
    locTask->setUrl( GeoUrl.arg( place ) );
    QThreadPool::globalInstance()->start( locTask );
}


void LocationDialog::updateLocations( QString response ) {
    QVariantMap  locReplyMap = DFL::Config::Hjson::readConfigFromString( response );
    QVariantList locations   = locReplyMap[ "results" ].toList();

    for ( QVariant locVar: locations ) {
        QVariantMap     location = locVar.toMap();
        QListWidgetItem *item    = new QListWidgetItem( results );

        item->setText(
            QString(
                "%1, %2, %3 (%4, %5)"
            ).arg(
                location[ "name" ].toString()
            ).arg(
                location[ "admin2" ].toString()
            ).arg(
                location[ "admin1" ].toString()
            ).arg(
                location[ "latitude" ].toDouble(), 0, 'f', 5
            ).arg(
                location[ "longitude" ].toDouble(), 0, 'f', 5
            )
        );

        item->setData( Qt::UserRole + 0, location[ "name" ] );
        item->setData( Qt::UserRole + 1, location[ "latitude" ] );
        item->setData( Qt::UserRole + 2, location[ "longitude" ] );
        item->setData( Qt::UserRole + 3, location[ "timezone" ] );

        results->addItem( item );
    }

    if ( locations.isEmpty() ) {
        QListWidgetItem *item = new QListWidgetItem( results );
        item->setText( "No results" );
        item->setFlags( Qt::NoItemFlags );

        results->addItem( item );
        selectBtn->setDisabled( true );
    }

    else {
        selectBtn->setEnabled( true );
    }

    /** Select the first one by default */
    results->setCurrentRow( 0 );

    setEnabled( true );
    QCoreApplication::processEvents();
}


#include "WeatherWidget.moc"
