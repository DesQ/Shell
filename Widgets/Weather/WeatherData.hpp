/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

const QMap<int, QStringList> WeatherInfoMap = {
    {
        0,{
            "Clear sky",
            "clear-day",
            "clear-night"
        }
    },
    {
        1,{
            "Mainly Clear Sky",
            "cloudy-1-day",
            "cloudy-1-night"
        }
    },
    {
        2,{
            "Partly Cloudy",
            "cloudy-2-day",
            "cloudy-2-night"
        }
    },
    {
        3,{
            "Overcast Sky",
            "cloudy-3-day",
            "cloudy-3-night"
        }
    },
    {
        45,{
            "Fog",
            "fog-day",
            "fog-night"
        }
    },
    {
        48,{
            "Rime",
            "frost-day",
            "frost-night"
        }
    },
    {
        51,{
            "Light Drizzle",
            "rainy-1-day",
            "rainy-1-night"
        }
    },
    {
        53,{
            "Medium Drizzle",
            "rainy-1-day",
            "rainy-1-night"
        }
    },
    {
        55,{
            "Heavy Drizzle",
            "rainy-2-day",
            "rainy-2-night"
        }
    },
    {
        56,{
            "Freezing Drizzle",
            "rain-and-sleet-mix",
            "rain-and-sleet-mix"
        }
    },
    {
        57,{
            "Freezing Drizzle",
            "rain-and-sleet-mix",
            "rain-and-sleet-mix"
        }
    },
    {
        61,{
            "Light rain",
            "rainy-2-day",
            "rainy-2-night"
        }
    },
    {
        63,{
            "Rain",
            "rainy-3-day",
            "rainy-3-night"
        }
    },
    {
        65,{
            "Heavy rain",
            "rainy-3-day",
            "rainy-3-night"
        }
    },
    {
        66,{
            "Freezing rain",
            "rain-and-sleet-mix",
            "rain-and-sleet-mix"
        }
    },
    {
        67,{
            "Heavy Freezing Rain",
            "rain-and-sleet-mix",
            "rain-and-sleet-mix"
        }
    },
    {
        71,{
            "Light Snow Fall",
            "snowy-1-day",
            "snowy-1-night"
        }
    },
    {
        73,{
            "Snow Fall",
            "snowy-2-day",
            "snowy-2-night"
        }
    },
    {
        75,{
            "Heavy Snow Fall",
            "snowy-3-day",
            "snowy-3-night"
        }
    },
    {
        77,{
            "Grainy Snow Fall",
            "snow-and-sleet-mix",
            "snow-and-sleet-mix"
        }
    },
    {
        80,{
            "Light Rain Showers",
            "rainy-2-day",
            "rainy-2-night"
        }
    },
    {
        81,{
            "Rain Showers",
            "rainy-3-day",
            "rainy-3-night"
        }
    },
    {
        82,{
            "Heavy Rain Showers",
            "rainy-3-day",
            "rainy-3-night"
        }
    },
    {
        85,{
            "Light Snow Showers",
            "snowy-2-day",
            "snowy-2-night"
        }
    },
    {
        86,{
            "Heavy Snow Showers",
            "snowy-3-day",
            "snowy-3-night"
        }
    },
    {
        95,{
            "Thunderstorm",
            "scattered-thunderstorms-day",
            "scattered-thunderstorms-night",
        }
    },
    {
        96,{
            "Thunderstorm",
            "scattered-thunderstorms-day",
            "scattered-thunderstorms-night",
        }
    },
    {
        99,{
            "Heavy Thunderstorm",
            "scattered-thunderstorms-day",
            "scattered-thunderstorms-night",
        }
    },
};
