/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <desqui/ShellPlugin.hpp>

#include <DFSettings.hpp>

#include "ClockWidget.hpp"

#include <thread>

DigitalClockBase::DigitalClockBase( QTimeZone tz, QString country, QWidget *parent ) : QWidget( parent ) {
    mTZ = tz;

    QDateTime dt   = QDateTime::currentDateTime();
    int       size = font().pointSize() * 16.0 / 12.0 * 2.0;

    time = new QLCDNumber( this );
    time->setDigitCount( 5 );
    time->setSegmentStyle( QLCDNumber::Flat );
    time->setFrameStyle( QLCDNumber::NoFrame );
    time->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding ) );
    time->setMinimumHeight( size * 2 );
    time->display( dt.toString( "hh:mm" ) );

    secs = new QLCDNumber( this );
    secs->setDigitCount( 2 );
    secs->setSegmentStyle( QLCDNumber::Flat );
    secs->setFrameStyle( QLCDNumber::NoFrame );
    secs->setFixedSize( QSize( size, size ) );
    secs->display( dt.toString( "ss" ) );

    ampm = new QLabel();
    ampm->setAlignment( Qt::AlignCenter );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );

    date = new QLabel();
    date->setAlignment( Qt::AlignCenter );
    date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );

    flag = new QLabel();
    flag->setAlignment( Qt::AlignCenter );
    flag->setFont( QFont( "Noto Color Emoji", 11 ) );
    flag->setText( country );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( time, 0, 0, 2, 1 );
    lyt->addWidget( secs, 0, 1, Qt::AlignCenter );
    lyt->addWidget( ampm, 1, 1, Qt::AlignCenter );
    lyt->addWidget( date, 2, 0, Qt::AlignCenter );
    lyt->addWidget( flag, 2, 1, Qt::AlignCenter );
    setLayout( lyt );

    setFixedHeight( size * 3.5 );

    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();

    shadow->setOffset( 1, 1 );
    shadow->setColor( palette().color( QPalette::Window ) );
    setGraphicsEffect( shadow );
}


DigitalClockBase::~DigitalClockBase() {
    delete time;
    delete secs;
    delete ampm;
    delete date;
    delete flag;
}


void DigitalClockBase::updateTime() {
    QDateTime dt = QDateTime::currentDateTime().toTimeZone( mTZ );

    time->display( dt.toString( "hh:mm" ) );
    secs->display( dt.toString( "ss" ) );
    ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );
    date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );
}


DigitalClock::DigitalClock( QWidget *parent ): DesQ::Shell::PluginWidget( parent ) {
    DFL::Settings *clockSett = new DFL::Settings( QDir::home().filePath( ".config/DesQ/Plugins/Clock.conf" ) );

    QStringList clockList = clockSett->value( "Clocks" );

    for ( QString clock: clockList ) {
        QString          tzStr( clockSett->value( QString( "%1::TimeZone" ).arg( clock ) ) );
        QTimeZone        tz    = ( tzStr == "System" ? QTimeZone::systemTimeZone() : QTimeZone( tzStr.toUtf8() ) );
        DigitalClockBase *base = new DigitalClockBase(
            tz,
            clockSett->value( QString( "%1::Flag" ).arg( clock ) ),
            this
        );

        clocks << base;
    }

    QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );

    for ( int i = 0; i < clocks.count(); i++ ) {
        lyt->addWidget( clocks[ i ] );

        if ( i < clocks.count() - 1 ) {
            /** TODO: Replace with separator */
            QFrame *sep = new QFrame();
            sep->setFrameStyle( QFrame::HLine | QFrame::Plain );
            lyt->addWidget( sep );
        }
    }

    setLayout( lyt );

    running = true;
    future  = std::async( std::launch::async, &DigitalClock::runTimer, this );
}


DigitalClock::~DigitalClock() {
    running = false;

    if ( future.valid() ) {
        // Wait for thread to finish (if still running)
        future.wait();
    }
}


bool DigitalClock::canReload() {
    return true;
}


void DigitalClock::runTimer() {
    while ( running ) {
        for ( DigitalClockBase *clock: clocks ) {
            clock->updateTime();
        }

        std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );
    }
}
