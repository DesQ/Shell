/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QLCDNumber>
#include <QLabel>
#include <QBasicTimer>
#include <desq/Battery.hpp>
#include <desqui/ShellPlugin.hpp>

#include <future>
#include <atomic>
#include <thread>

class DigitalClockBase : public QWidget {
    Q_OBJECT;

    public:
        DigitalClockBase( QTimeZone tz, QString country, QWidget *parent = nullptr );
        ~DigitalClockBase();

        void updateTime();

    private:
        QLCDNumber *time;
        QLCDNumber *secs;
        QLabel *ampm;
        QLabel *date;
        QLabel *flag;

        QTimeZone mTZ;
};


class DigitalClock : public DesQ::Shell::PluginWidget {
    Q_OBJECT;

    public:
        DigitalClock( QWidget *parent = nullptr );
        ~DigitalClock();

        bool canReload() override;

    private:
        QList<DigitalClockBase *> clocks;

        std::atomic<bool> running{ false };
        std::future<void> future;

        void runTimer();
};
