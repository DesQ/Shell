/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Scene.hpp"
#include "Manager.hpp"
#include "Global.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFGuiApplication.hpp>

DesQ::Scene::Manager::Manager( QString imgLoc ) {
    bgImg = imgLoc;

    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn, imgLoc );
    }

    connect( shellSett, &DFL::Settings::settingChanged, this, &DesQ::Scene::Manager::reloadSettings );

    /** Delete the instance and the layer surface when the screen is disconnected */
    connect(
        qApp, &DFL::GuiApplication::screenRemoved, [ = ] ( QScreen *screen ) {
            if ( mInstances.contains( screen->name() ) ) {
                WQt::LayerSurface *cls = mSurfaces.take( screen->name() );
                delete cls;

                DesQ::Scene::UI *ui = mInstances.take( screen->name() );
                ui->close();
                delete ui;
            }
        }
    );
}


DesQ::Scene::Manager::~Manager() {
    for ( WQt::LayerSurface *surf: mSurfaces.values() ) {
        delete surf;
    }
    mSurfaces.clear();

    for ( DesQ::Scene::UI *ui: mInstances.values() ) {
        ui->close();
        delete ui;
    }
    mInstances.clear();
}


void DesQ::Scene::Manager::handleMessages( QString msg, int fd ) {
    if ( msg.startsWith( "background\n" ) ) {
        QStringList parts = msg.split( "\n" );

        /**
         * Output name is specified.
         */
        if ( parts.count() == 3 ) {
            if ( mInstances.contains( parts[ 1 ] ) ) {
                mInstances[ parts[ 1 ] ]->update( parts[ 2 ] );
                qApp->messageClient( "done", fd );

                return;
            }

            else {
                qWarning() << parts[ 1 ] << "is not a valid output";
            }

            qApp->messageClient( "failed", fd );
        }

        else {
            for ( DesQ::Scene::UI *ui: mInstances.values() ) {
                ui->update( parts[ 1 ] );
            }

            qApp->messageClient( "done", fd );
        }
    }

    else if ( msg.startsWith( "set-background" ) ) {
        QStringList parts = msg.split( "\n" );

        if ( parts.length() == 3 ) {
            shellSett->setValue( "Background:" + parts[ 1 ], parts[ 2 ] );

            if ( mInstances.contains( parts[ 1 ] ) ) {
                mInstances[ parts[ 1 ] ]->update( parts[ 2 ] );
                qApp->messageClient( "done", fd );

                return;
            }

            qApp->messageClient( "failed", fd );
        }

        else {
            shellSett->setValue( "Background", parts[ 1 ] );
            for ( DesQ::Scene::UI *ui: mInstances.values() ) {
                ui->update( parts[ 1 ] );
            }

            qApp->messageClient( "done", fd );
        }
    }

    else {
        qWarning() << "Unhandled request:" << msg;
        qApp->messageClient( "Unhandled request", fd );
    }
}


void DesQ::Scene::Manager::createInstance( QScreen *scrn, QString bgImg ) {
    DesQ::Scene::UI *scene = new DesQ::Scene::UI( scrn, bgImg );

    scene->show();

    if ( WQt::Utils::isWayland() ) {
        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen( scrn );

        WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
            scene,                         // Window Handle
            output,                        // wl_output object - for multi-monitor support
            WQt::LayerShell::Background,   // Background layer
            "background"                   // Dummy namespace
        );

        /** Anchor everywhere */
        cls->setAnchors(
            WQt::LayerSurface::Top |
            WQt::LayerSurface::Bottom |
            WQt::LayerSurface::Left |
            WQt::LayerSurface::Right
        );

        /** Size - Same as the screen size */
        cls->setSurfaceSize( scrn->size() );

        /** Nothing should move this */
        cls->setExclusiveZone( -1 );

        /** No keyboard interaction */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Apply the changes */
        cls->apply();

        /** Resize the instance and the layer surface when screen size changes */
        connect(
            scrn, &QScreen::geometryChanged, [ = ]( const QRect& newGeometry ) {
                cls->setSurfaceSize( newGeometry.size() );
                cls->apply();
            }
        );

        mInstances[ scrn->name() ] = scene;
        mSurfaces[ scrn->name() ]  = cls;
    }

    else {
        scene->close();
        delete scene;
    }
}


void DesQ::Scene::Manager::reloadSettings( QString key, QVariant ) {
    /** General */
    if ( ( key == "Background" ) or ( key == "BackgroundPosition" ) ) {
        for ( DesQ::Scene::UI *ui: mInstances.values() ) {
            ui->update( key );
            ui->update( "opacity" );
        }
    }

    /** Output specific */
    else if ( key.startsWith( "Background:" ) or key.startsWith( "BackgroundPosition:" ) ) {
        QStringList parts  = key.split( ":" );
        QString     output = parts[ 1 ];

        if ( mInstances.contains( output ) ) {
            mInstances[ output ]->update( key );
            mInstances[ output ]->update( "opacity" );
        }
    }
}
