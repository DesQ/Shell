/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Scene.hpp"

#include <desq/Utils.hpp>
#include <DFXdg.hpp>
#include <DFSettings.hpp>
#include <desq/desq-config.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>

DesQ::Scene::UI::UI( QScreen *scr, QString imageLoc ) : QWindow() {
    mScreen = scr;

    /* We need our desktop to fill the whole physical screen, so update it any time screen changes */
    connect( mScreen, &QScreen::geometryChanged, this, &DesQ::Scene::UI::resizeDesktop );

    /** Check if we are going to use OpenGL */
    mUseGL = shellSett->value( "UseGL" );

    /** Init our variables */
    if ( mUseGL ) {
        /** We need an OpenGL surface */
        setSurfaceType( QWindow::OpenGLSurface );

        QSurfaceFormat format;
        format.setSamples( 16 );
        setFormat( format );
    }

    else {
        /** Create our BackingStore */
        mBackingStore = new QBackingStore( this );
    }

    /* Resize to fill the whole physical screen */
    resize( mScreen->size() );

    /* Title */
    setTitle( "DesQ Scene" );

    /* Set the windowFlags */
    setFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint );

    /* Background image */
    bgImagePath = imageLoc;

    /* Background position */
    if ( shellSett->hasKey( "BackgroundPosition:" + mScreen->name() ) ) {
        wallpaperPos = ( int )shellSett->value( "BackgroundPosition:" + mScreen->name() );
    }

    else {
        wallpaperPos = ( int )shellSett->value( "BackgroundPosition" );
    }

    /* Prepare the background */
    prepareBackgroundImage();
}


DesQ::Scene::UI::~UI() {
    mScreen->disconnect();
    delete mScreen;

    if ( mUseGL ) {
        delete mContext;
        delete mDevice;
    }

    else {
        delete mBackingStore;
    }
}


void DesQ::Scene::UI::update( QString what ) {
    if ( what == "opacity" ) {
        QVariantAnimation *anim = createAnimator();
        anim->start();

        return;
    }

    /** Update the bg image from settings */
    if ( what == "Background" ) {
        if ( shellSett->hasKey( "Output:" + mScreen->name() + "::Background" ) ) {
            bgImagePath = ( QString )shellSett->value( "Output:" + mScreen->name() + "::Background" );
        }

        else {
            bgImagePath = ( QString )shellSett->value( "Background" );
        }
    }

    /** Update the bg image from settings */
    else if ( what == "BackgroundPosition" ) {
        if ( shellSett->hasKey( "Output:" + mScreen->name() + "::BackgroundPosition" ) ) {
            wallpaperPos = ( int )shellSett->value( "Output:" + mScreen->name() + "::BackgroundPosition" );
        }

        else {
            wallpaperPos = ( int )shellSett->value( "BackgroundPosition" );
        }
    }

    else {
        bgImagePath = what;
    }

    prepareBackgroundImage();
}


void DesQ::Scene::UI::render() {
    /** If the window is not shown, don't perform a repaint.s */
    if ( !isExposed() ) {
        return;
    }

    /** Do nothing if the image is null */
    if ( desktopImage.isNull() ) {
        qWarning() << "Null Image";
        return;
    }

    QRect    rect( 0, 0, width(), height() );
    QPainter *painter;

    if ( mUseGL ) {
        bool initGLFunc = false;

        if ( mContext == nullptr ) {
            /** Create our OpenGL Context */
            mContext = new QOpenGLContext( this );
            mContext->setFormat( requestedFormat() );
            mContext->create();

            initGLFunc = true;
        }

        mContext->makeCurrent( this );

        if ( initGLFunc ) {
            /** Init various OpengGL functions */
            initializeOpenGLFunctions();
        }

        if ( mDevice == nullptr ) {
            mDevice = new QOpenGLPaintDevice();
        }

        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

        mDevice->setSize( rect.size() * devicePixelRatio() );
        mDevice->setDevicePixelRatio( devicePixelRatio() );

        painter = new QPainter( mDevice );
    }

    else {
        mBackingStore->beginPaint( rect );

        QPaintDevice *device = mBackingStore->paintDevice();
        painter = new QPainter( device );
    }

    /** Perform the painting */
    painter->setCompositionMode( QPainter::CompositionMode_SourceOver );
    painter->setOpacity( mOpacity );

    painter->setPen( Qt::NoPen );
    painter->setBrush( Qt::NoBrush );

    painter->setRenderHints( QPainter::Antialiasing );

    painter->drawImage( rect, desktopImage, QRectF( mPicOffset, rect.size() ) );

    painter->end();

    if ( mUseGL ) {
        mContext->swapBuffers( this );
    }

    else {
        mBackingStore->endPaint();
        mBackingStore->flush( rect );
    }
}


void DesQ::Scene::UI::cacheImage() {
    /**
     * We'll be saving the image as ~/.cache/DesQ/background.xxx
     * Additionally, we'll write an info file that contains the image path,
     * wallpaper position, hash, etc. This info file will be used
     * by isCacheFresh().
     */

    QDir cache( DFL::XDG::xdgCacheHome() + "/DesQ/" );

    QFile imgFile( bgImagePath );

    imgFile.open( QFile::ReadOnly );

    QCryptographicHash hash( QCryptographicHash::Sha512 );

    hash.addData( imgFile.readAll() );
    imgFile.close();

    QSettings sett( cache.filePath( "background.ini" ), QSettings::IniFormat );

    sett.beginGroup( "Output:" + mScreen->name() );
    sett.setValue( "File",     bgImagePath );
    sett.setValue( "Position", wallpaperPos );
    sett.setValue( "Hash",     hash.result() );
    sett.endGroup();

    sett.sync();

    desktopImage.save( cache.filePath( "background-" + mScreen->name() + ".jpg" ) );
}


bool DesQ::Scene::UI::isCacheFresh() {
    QDir      cache( DFL::XDG::xdgCacheHome() + "/DesQ/" );
    QSettings sett( cache.filePath( "background.ini" ), QSettings::IniFormat );

    if ( not QFile::exists( cache.filePath( "background-" + mScreen->name() + ".jpg" ) ) ) {
        return false;
    }

    sett.beginGroup( "Output:" + mScreen->name() );

    if ( sett.value( "File" ).toString() != bgImagePath ) {
        sett.endGroup();
        return false;
    }

    if ( sett.value( "Position" ).toInt() != wallpaperPos ) {
        sett.endGroup();
        return false;
    }

    QFile              imgFile( bgImagePath );
    QCryptographicHash hash( QCryptographicHash::Sha512 );

    imgFile.open( QFile::ReadOnly );
    hash.addData( imgFile.readAll() );
    imgFile.close();

    if ( sett.value( "Hash" ).toByteArray() != hash.result() ) {
        sett.endGroup();
        return false;
    }

    sett.endGroup();
    cachedImagePath = cache.filePath( "background-" + mScreen->name() + ".jpg" );

    return true;
}


void DesQ::Scene::UI::prepareBackgroundImage() {
    /* If the BG path was not specified by the user, use saved path */
    if ( bgImagePath.isEmpty() ) {
        if ( shellSett->hasKey( "Background:" + mScreen->name() ) ) {
            bgImagePath = ( QString )shellSett->value( "Background:" + mScreen->name() );
        }

        else {
            bgImagePath = ( QString )shellSett->value( "Background" );
        }
    }

    /** If the user does not want a background image */
    if ( bgImagePath == "None" ) {
        return;
    }

    /** User has specified a solid color? */
    if ( bgImagePath.startsWith( "#" ) ) {
        QColor clr( bgImagePath );
        desktopImage.fill( clr );

        requestUpdate();

        return;
    }

    /* If the specified path/saved path does not exist, use the default value */
    if ( not QFile::exists( bgImagePath ) ) {
        bgImagePath = SharePath "resources/default.jpg";
    }

    if ( isCacheFresh() ) {
        desktopImage = QImage( cachedImagePath );

        if ( desktopImage.size() == mScreen->size() ) {
            update( "opacity" );
            return;
        }

        else {
            qDebug() << "Cache appears fresh, but image sized do not match";
            qDebug() << cachedImagePath << desktopImage.size() << mScreen->size();
        }
    }

    QSize drawSize = mScreen->geometry().size();

    QImageReader ir( bgImagePath );
    QSize        pixSize = ir.size();

    mPicOffset = QPoint();

    switch ( wallpaperPos ) {
        // Centered: Show the full wallpaper at the center
        case 0: {
            pixSize    = pixSize.scaled( drawSize, Qt::KeepAspectRatio );
            mPicOffset = { abs( pixSize.width() - drawSize.width() ) / 2, abs( pixSize.height() - drawSize.height() ) / 2 };

            break;
        }

        // Stretched: Ignore aspect ratio
        case 1: {
            pixSize = pixSize.scaled( drawSize, Qt::IgnoreAspectRatio );
            break;
        }

        // Scaled: Qt::KeepAspectRatioByExpanding
        case 2: {
            pixSize = pixSize.scaled( drawSize, Qt::KeepAspectRatioByExpanding );
            break;
        }
    }

    ir.setScaledSize( pixSize );

    /* We failed to read the background image: do nothing */
    if ( ir.read( &desktopImage ) == false ) {
        qCritical() << "Unable to read the image. No background will be drawn";
        return;
    }

    /* Cache the scaled image so that it can be used later on */
    cacheImage();

    /** Set the opacity to zero */
    update( "opacity" );
}


void DesQ::Scene::UI::resizeDesktop() {
    /** Resize the window */
    resize( mScreen->size() );

    /* Reset the background */
    prepareBackgroundImage();

    update( "opacity" );
}


QVariantAnimation * DesQ::Scene::UI::createAnimator() {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    anim->setStartValue( 0.0 );
    anim->setEndValue( 1.0 );
    anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuad ) );

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) mutable {
            mOpacity = val.toDouble();
            render();
        }
    );

    return anim;
}


bool DesQ::Scene::UI::event( QEvent *event ) {
    if ( event->type() == QEvent::UpdateRequest ) {
        render();
        return true;
    }

    return QWindow::event( event );
}


void DesQ::Scene::UI::resizeEvent( QResizeEvent *resizeEvent ) {
    if ( mUseGL ) {
    }

    else {
        mBackingStore->resize( resizeEvent->size() );
    }
}


void DesQ::Scene::UI::exposeEvent( QExposeEvent * ) {
    if ( isExposed() ) {
        render();
    }
}
