/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <ctime>

#include "Global.hpp"
#include "ShellManager.hpp"
#include "AutoStart.hpp"
#include "TrackedProcess.hpp"

#include <desq/Utils.hpp>
#include <DFSettings.hpp>
#include <desq/desq-config.h>

#include <DFXdg.hpp>
#include <DFCoreApplication.hpp>

int MaxAutoRestartLimit;

static inline QString getSocketName( QString appId ) {
    QString sockName;

    /* $__DESQ_WORK_DIR is created and set by the session */
    if ( qgetenv( "__DESQ_WORK_DIR" ).length() ) {
        sockName = qEnvironmentVariable( "__DESQ_WORK_DIR" ) + "/" + appId;
    }

    /* It does not exist: Let's create it */
    else {
        /* We're going to assume that XDG_RUNTIME_DIR exists and is writable at this point */
        QString XDG_RUNTIME_DIR = DFL::XDG::xdgRuntimeDir();

        /* We're also going to assume that XDG_SESSION_ID exists */
        QString XDG_SESSION_ID( qgetenv( "XDG_SESSION_ID" ) );

        /* Create this path blindly. This MUST work: No fallbacks */
        QDir( XDG_RUNTIME_DIR ).mkpath( "DesQSession-" + XDG_SESSION_ID + "/" );

        /* Our socket lives here */
        sockName = QDir( XDG_RUNTIME_DIR ).filePath( "DesQSession-" + XDG_SESSION_ID + "/" + appId );
    }

    return sockName;
}


DesQ::ShellManager::ShellManager() : QObject() {
    MaxAutoRestartLimit = shellSett->value( "Manager::MaxAutoRestartLimit" );
}


DesQ::ShellManager::~ShellManager() {
    shutdown();
}


void DesQ::ShellManager::startManagement() {
    /** Tracked processes specified in the User Config */
    QStringList utils = shellSett->value( "Utilities" );

    for ( QString util: utils ) {
        qDebug() << "Starting" << util;

        QString  name( shellSett->value( util + "::Exec" ) );
        QVariant args = shellSett->value( util + "::Args" );

        startProcess( name, args.toStringList() );
    }

    // Synthetic 5s delay.
    int t = 0;

    while ( t < 10 ) {
        // Sleep of 500 ms at one time
        // 500 (ms) * 1000 (us per ms) * 1000 (ns per us)
        struct timespec ts = { 0, 500 * 1000 * 1000 };
        nanosleep( &ts, NULL );
        qApp->processEvents();
        t++;
    }

    qDebug() << "Starting auto-start apps";
    DesQAutoStart *autoStart = new DesQAutoStart();

    autoStart->start();
}


void DesQ::ShellManager::listProcesses( int fd ) {
    qApp->messageClient( activatedUtils.keys().join( "\n" ), fd );
}


void DesQ::ShellManager::startProcess( QString util, QStringList args ) {
    /** Do nothing if it's already running */
    if ( activatedUtils.contains( util ) ) {
        return;
    }

    QString utilExec = DesQ::Utils::getUtilityPath( util );

    if ( DesQ::Utils::isExecutable( utilExec ) ) {
        TrackedProcess *tp = new TrackedProcess( utilExec, args );
        tp->start();

        connect(
            tp, &TrackedProcess::crashed, [ = ] () mutable {
                if ( activatedUtils.contains( util ) ) {
                    activatedUtils.take( util )->deleteLater();
                }
            }
        );

        activatedUtils[ util ] = tp;
    }
}


void DesQ::ShellManager::killProcess( QString util, bool kill ) {
    /** Do nothing if it's not running */
    if ( activatedUtils.contains( util ) == false ) {
        return;
    }

    if ( kill ) {
        activatedUtils.take( util )->kill();
    }

    else {
        activatedUtils.take( util )->terminate();
    }
}


void DesQ::ShellManager::restartProcess( QString util ) {
    /** Do nothing if it's not running */
    if ( activatedUtils.contains( util ) == false ) {
        return;
    }

    /** Restart otherwise */
    else {
        activatedUtils.value( util )->restart();
    }
}


void DesQ::ShellManager::shutdown() {
    delete shellSett;

    for ( QString util: activatedUtils.keys() ) {
        TrackedProcess *tp = activatedUtils.take( util );

        if ( tp->isRunning() ) {
            tp->terminate();
            delete tp;
        }
    }

    /* Close this daemon */
    qApp->quit();
}


void DesQ::ShellManager::handleMessages( QString message, int fd ) {
    if ( message == "list-processes" ) {
        listProcesses( fd );
    }

    else if ( message.startsWith( "start-process" ) ) {
        QStringList parts = message.split( "\n\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        startProcess( parts[ 0 ], ( parts.count() == 2 ? parts.at( 1 ).split( "\n" ) : QStringList() ) );
    }

    else if ( message.startsWith( "restart-process" ) ) {
        QStringList parts = message.split( "\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        restartProcess( parts[ 0 ] );
    }

    else if ( message.startsWith( "stop-process" ) ) {
        QStringList parts = message.split( "\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        killProcess( parts[ 0 ], false );
    }

    else if ( message.startsWith( "kill-process" ) ) {
        QStringList parts = message.split( "\n", Qt::SkipEmptyParts );
        parts.removeAt( 0 );

        killProcess( parts[ 0 ], true );
    }

    else if ( message.startsWith( "terminate" ) ) {
        shutdown();
        qApp->messageClient( activatedUtils.keys().join( "\n" ), fd );
    }
}
