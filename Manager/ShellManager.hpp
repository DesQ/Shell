/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>
#include <QtGui>
#include <QtCore>

class TrackedProcess;

namespace DesQ {
    class ShellManager;
}

class DesQ::ShellManager : public QObject {
    Q_OBJECT;

    public:
        ShellManager();
        ~ShellManager();

        void startManagement();

        /** List the tracked processes */
        void listProcesses( int );

        /** Start a tracked process if not already running */
        void startProcess( QString, QStringList );

        /**
         * Stop/Kill a running tracked process
         * @kill == true,  send kill -SIGKILL <pid>
         * @kill == false, send kill -SIGTERM <pid>
         */
        void killProcess( QString, bool kill );

        /** Restart a running tracked process */
        void restartProcess( QString );

    public Q_SLOTS:
        void shutdown();

        void handleMessages( QString, int );

    private:
        QHash<QString, TrackedProcess *> activatedUtils;
};
