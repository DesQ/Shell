/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <csignal>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include <QtCore>

#include <desq/Utils.hpp>

#include <DFCoreApplication.hpp>
#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "Global.hpp"
#include "ShellManager.hpp"

DFL::Settings *shellSett = nullptr;
DFL::Settings *panelSett = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Shell.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Shell started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    DFL::CoreApplication app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Shell" );
    app.setApplicationVersion( PROJECT_VERSION );

    QObject::connect(
        &app, &DFL::CoreApplication::unixSignalReceived, [ &app ] ( int signum ) {
            if ( ( signum == SIGTERM ) || ( signum == SIGQUIT ) || ( signum == SIGINT ) ) {
                app.quit();
            }
        }
    );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "l", "list-processes" }, "List the tracked processes" } );
    parser.addOption( { { "s", "start-process" }, "Run an executable/desq utility as a tracked process", "name" } );
    parser.addOption( { { "r", "restart-process" }, "Restart an alerady running tracked process", "process" } );
    parser.addOption( { { "p", "stop-process" }, "Stop a running tracked process", "process" } );
    parser.addOption( { { "k", "kill-process" }, "Kill a running tracked process", "process" } );
    parser.addOption( { { "t", "terminate" }, "Shutdown desq-shell" } );

    parser.process( app );

    if ( app.lockApplication() ) {
        /** Initialize the settings */
        shellSett = DesQ::Utils::initializeDesQSettings( "Shell", "Shell" );
        panelSett = DesQ::Utils::initializeDesQSettings( "Panel", "Panel" );

        /** Initialize the Shell Manager */
        DesQ::ShellManager manager;

        /** Reroute the messages to the server to DesQ Shell Manager */
        QObject::connect( &app, &DFL::CoreApplication::messageFromClient, &manager, &DesQ::ShellManager::handleMessages );

        /** Start the managerment */
        manager.startManagement();

        return app.exec();
    }

    else {
        if ( parser.isSet( "list-processes" ) ) {
            QObject::connect(
                &app, &DFL::CoreApplication::messageFromServer, [ = ]( QString msg ) {
                    QStringList procs( msg.split( "\n" ) );
                    printf( "DesQ Shell " PROJECT_VERSION "\n\n" );
                    printf( "Currently, the following processes are running:\n" );
                    for ( QString proc: procs ) {
                        printf( "  => %s\n", proc.toUtf8().constData() );
                    }
                    printf( "\n" );

                    /**
                     * We cannot use return 0 => that will take us to app.exec();
                     * std::exit( 0 ) will exit the app with exit code = 0.
                     */
                    std::exit( 0 );
                }
            );

            app.messageServer( "list-processes" );

            /** Wait till we recieve a reply, and display it. */
            app.exec();
        }

        else if ( parser.isSet( "start-process" ) ) {
            QString proc = parser.value( "start-process" );
            app.messageServer( "start-process\n\n" + proc + "\n\n" + parser.positionalArguments().join( "\n" ) );
        }

        else if ( parser.isSet( "restart-process" ) ) {
            QString proc = parser.value( "restart-process" );
            app.messageServer( "restart-process\n" + proc );
        }

        else if ( parser.isSet( "stop-process" ) ) {
            QString proc = parser.value( "kill-process" );
            app.messageServer( "stop-process\n" + proc );
        }

        else if ( parser.isSet( "kill-process" ) ) {
            QString proc = parser.value( "kill-process" );
            app.messageServer( "kill-process\n" + proc );
        }

        else if ( parser.isSet( "terminate" ) ) {
            app.messageServer( "terminate" );

            QObject::connect(
                &app, &DFL::CoreApplication::messageFromServer, [ = ]( QString msg ) {
                    if ( msg == "success" ) {
                        /** Everything went smoothly */
                        std::exit( 0 );
                    }

                    else {
                        /** Something went wrong */
                        std::exit( 1 );
                    }
                }
            );

            /** We wait for the server to terminate */
            return app.exec();
        }

        else {
            parser.showHelp();
        }

        return 0;
    }

    return 0;
}
