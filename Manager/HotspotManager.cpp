/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QDebug>
#include <ctime>

#include "Global.hpp"
#include "HotspotManager.hpp"

#include <desq/Utils.hpp>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFSettings.hpp>
#include <DFGuiApplication.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayfireShell.hpp>

WQt::Registry *wlRegistry = nullptr;

DesQ::HotspotManager::HotspotManager() : QObject() {
    /** WQt::Registry should already setup. Let's just wait for the interface */
    if ( wlRegistry->waitForInterface( WQt::Registry::WayfireShellInterface ) ) {
        shell = wlRegistry->wayfireShell();

        /** We can now setup the hotspots */
        setupHotCorners();
    }

    /** Maybe the plugin is not loaded, yet!? When it's loaded, we'll setup everything. */
    else {
        connect(
            wlRegistry, &WQt::Registry::interfaceRegistered, [ = ]( WQt::Registry::Interface iface ) mutable {
                if ( iface == WQt::Registry::WayfireShellInterface ) {
                    shell = wlRegistry->wayfireShell();

                    /** We can now setup the hotspots */
                    setupHotCorners();
                }
            }
        );
    }
}


DesQ::HotspotManager::~HotspotManager() {
    delete shell;
    shell = nullptr;
}


void DesQ::HotspotManager::setupHotCorners() {
    connect( qApp, &QApplication::screenAdded, this, &DesQ::HotspotManager::setupHotSpotForScreen );

    for ( QScreen *scrn: qApp->screens() ) {
        setupHotSpotForScreen( scrn );
    }
}


void DesQ::HotspotManager::setupHotSpotForScreen( QScreen *scrn ) {
    wl_output *wlout = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );

    WQt::WfOutput *wfOut = shell->getOutput( wlout );

    wfOut->setup();

    WQt::HotSpot *topLeftHS = wfOut->createHotSpot( WQt::WfOutput::TopLeft, 1, 250 );

    connect(
        topLeftHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::TopLeft, scrn->name() );
        }
    );
    connect(
        topLeftHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::TopLeft, scrn->name() );
        }
    );
    topLeftHS->setup();

    WQt::HotSpot *topRightHS = wfOut->createHotSpot( WQt::WfOutput::TopRight, 1, 250 );

    connect(
        topRightHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::TopRight, scrn->name() );
        }
    );
    connect(
        topRightHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::TopRight, scrn->name() );
        }
    );
    topRightHS->setup();

    WQt::HotSpot *bottomLeftHS = wfOut->createHotSpot( WQt::WfOutput::BottomLeft, 1, 250 );

    connect(
        bottomLeftHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::BottomLeft, scrn->name() );
        }
    );
    connect(
        bottomLeftHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::BottomLeft, scrn->name() );
        }
    );
    bottomLeftHS->setup();

    WQt::HotSpot *bottomRightHS = wfOut->createHotSpot( WQt::WfOutput::BottomRight, 1, 250 );

    connect(
        bottomRightHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
            handleHotCorner( WQt::WfOutput::BottomRight, scrn->name() );
        }
    );
    connect(
        bottomRightHS, &WQt::HotSpot::leftHotSpot, [ = ] () {
            cancelHotCorner( WQt::WfOutput::BottomRight, scrn->name() );
        }
    );
    bottomRightHS->setup();
}


void DesQ::HotspotManager::handleHotCorner( int corner, QString scrn ) {
    /* Based on the corner take action */
    switch ( corner ) {
        case WQt::WfOutput::TopLeft: {
            qDebug() << "Entering Top Left hot Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::TopRight: {
            qDebug() << "Entering Top Right Corner of" << scrn;
            /* Use TasksPlugin to show the list widget */
            break;
        }

        case WQt::WfOutput::BottomLeft: {
            qDebug() << "Entering Bottom Left Corner";
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "menu" ), {} );

            break;
        }

        case WQt::WfOutput::BottomRight: {
            qDebug() << "Entering Bottom Right Corner";
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "cask" ), { "--raise", "--output", scrn } );
            break;
        }

        /* In case hot corner was canceled */
        default: {
            break;
        }
    }
}


void DesQ::HotspotManager::cancelHotCorner( int corner, QString scrn ) {
    switch ( corner ) {
        case WQt::WfOutput::TopLeft: {
            qDebug() << "Leaving Top Left Corner of" << scrn;
            break;
        }

        /* We are exiting from the TopRight corner, so show restore the window state */
        case WQt::WfOutput::TopRight: {
            qDebug() << "Leaving Top Right Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::BottomLeft: {
            qDebug() << "Leaving Bottom Left Corner of" << scrn;
            break;
        }

        case WQt::WfOutput::BottomRight: {
            qDebug() << "Leaving Bottom Right Corner";
            break;
        }

        /* In case hot corner was canceled */
        default: {
            break;
        }
    }
}


int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/HotspotManager.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qInfo() << "------------------------------------------------------------------------";
    qInfo() << "DesQ Hotspot Manager started at" << date.constData();
    qInfo() << "------------------------------------------------------------------------\n";

    DFL::GuiApplication app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "HotspotManager" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-hotspotmanager" );

    if ( app.lockApplication() ) {
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );

        wlRegistry->setup();

        DesQ::HotspotManager manager;

        return app.exec();
    }

    qWarning() << "Another instance of DesQ Hotspot Manager is already running...!";

    return 0;
}
