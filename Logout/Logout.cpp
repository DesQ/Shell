/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QLabel>
#include <QScreen>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QPainter>

#include <desq/Utils.hpp>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <DFUtils.hpp>
#include <DFIpcClient.hpp>
#include <DFApplication.hpp>

#include "Global.hpp"
#include "Logout.hpp"
#include "Widgets.hpp"

static inline QToolButton * getButton( QString icon, QString name ) {
    QToolButton *btn = new QToolButton();

    btn->setIconSize( QSize( 96, 96 ) );
    btn->setToolButtonStyle( Qt::ToolButtonTextUnderIcon );
    btn->setIcon( QIcon::fromTheme( icon ) );
    btn->setText( name );
    btn->setAutoRaise( true );

    return btn;
}


PowerDialog::PowerDialog( QScreen *scrn ) : QWidget() {
    mScreen = scrn;

    connect(
        mScreen, &QScreen::geometryChanged, [ = ] ( const QRect& newRect ) {
            setFixedSize( newRect.size() );
        }
    );

    setFixedSize( mScreen->size() );

    setWindowFlags( Qt::BypassWindowManagerHint | Qt::FramelessWindowHint );
    setAttribute( Qt::WA_TranslucentBackground );

    prepareIpcClient();

    QPalette pltt = qApp->palette();

    pltt.setColor( QPalette::Window,     Qt::black );
    pltt.setColor( QPalette::WindowText, Qt::white );

    setPalette( pltt );

    mSessHndlr = new SessionHandler();

    createUI();
}


PowerDialog::~PowerDialog() {
    delete mSessionClient;

    delete lockBtn;
    delete logOutBtn;
    delete suspendBtn;
    delete hibernateBtn;
    delete powerOffBtn;
    delete rebootBtn;
    delete cancelBtn;

    delete mScreen;
}


void PowerDialog::show() {
    showMaximized();

    /** wl_output corresponding to @screen */
    wl_output *output = WQt::Utils::wlOutputFromQScreen( mScreen );

    cls = wlRegistry->layerShell()->getLayerSurface(
        windowHandle(),            // Window Handle
        output,                    // wl_output object - for multi-monitor support
        WQt::LayerShell::Overlay,  // Background layer
        "de-widget"                // Dummy namespace
    );

    /** Anchor everywhere */
    cls->setAnchors(
        WQt::LayerSurface::Top |
        WQt::LayerSurface::Bottom |
        WQt::LayerSurface::Left |
        WQt::LayerSurface::Right
    );

    /** Size - when */
    cls->setSurfaceSize( size() );

    /** Nothing should move this */
    cls->setExclusiveZone( -1 );

    /** No keyboard interaction */
    cls->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );

    /** Apply the changes */
    cls->apply();
}


void PowerDialog::close() {
    if ( cls != nullptr ) {
        delete cls;
        cls = nullptr;

        QWidget::close();
    }
}


void PowerDialog::prepareIpcClient() {
    QString orgName = "DesQ";
    QString appName = "Session Manager";

    QString sockDir( "%1/%2/%3" );

    sockDir = sockDir.arg( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) )
                 .arg( orgName.replace( " ", "" ) )
                 .arg( QString( qgetenv( "XDG_SESSION_ID" ) ) );

    QString sockPath = QString( "%1/%2.socket" ).arg( sockDir ).arg( appName.replace( " ", "" ) );

    mSessionClient = new DFL::IPC::Client( sockPath, this );

    mSessionClient->connectToServer();

    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 10 * 1000 ) == false ) {
        qCritical() << "Unable to connect to" << sockPath;
    }
}


void PowerDialog::createUI() {
    std::function<bool( QString )> canDo =
        [ this ] ( QString action ) -> bool {
            mSessionClient->sendMessage( "Can" + action );
            mSessionClient->waitForReply( 10 * 1000 );
            return ( mSessionClient->reply() == "true" ? true : false );
        };

    lockBtn = getButton( "system-lock-screen", "Lock Session" );
    connect( lockBtn,    &QToolButton::pressed, this, &PowerDialog::lock );

    logOutBtn = getButton( "system-log-out", "Logout" );
    connect( logOutBtn,  &QToolButton::pressed, this, &PowerDialog::logout );

    suspendBtn = getButton( "system-suspend", "Suspend" );
    connect( suspendBtn, &QToolButton::pressed, this, &PowerDialog::suspend );
    suspendBtn->setDisabled( canDo( "Suspend" ) == false );

    hibernateBtn = getButton( "system-suspend-hibernate", "Hibernate" );
    connect( hibernateBtn, &QToolButton::pressed, this, &PowerDialog::hibernate );
    hibernateBtn->setDisabled( canDo( "Hibernate" ) == false );

    powerOffBtn = getButton( "system-shutdown", "Shutdown" );
    connect( powerOffBtn, &QToolButton::pressed, this, &PowerDialog::poweroff );
    powerOffBtn->setDisabled( canDo( "PowerOff" ) == false );

    rebootBtn = getButton( "system-reboot", "Reboot" );
    connect( rebootBtn, &QToolButton::pressed, this, &PowerDialog::reboot );
    rebootBtn->setDisabled( canDo( "Reboot" ) == false );

    cancelBtn = getButton( "dialog-close", "Cancel" );
    connect( cancelBtn, &QToolButton::pressed, qApp, &QApplication::quit, Qt::QueuedConnection );

    QHBoxLayout *powerLyt = new QHBoxLayout();

    powerLyt->addStretch();
    powerLyt->addWidget( lockBtn );
    powerLyt->addWidget( Separator::vertical() );
    powerLyt->addWidget( logOutBtn );
    powerLyt->addWidget( suspendBtn );
    powerLyt->addWidget( hibernateBtn );
    powerLyt->addWidget( Separator::vertical() );
    powerLyt->addWidget( powerOffBtn );
    powerLyt->addWidget( rebootBtn );
    powerLyt->addWidget( Separator::vertical() );
    powerLyt->addWidget( cancelBtn );
    powerLyt->addStretch();

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->addStretch();
    baseLyt->addLayout( powerLyt );
    baseLyt->addStretch();

    setLayout( baseLyt );
}


void PowerDialog::lock() {
    QProcess::startDetached( DesQ::Utils::getUtilityPath( "lock" ), {} );
    qApp->quit();
}


void PowerDialog::logout() {
    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 100 * 1000 ) == false ) {
        return;
    }

    /** Hide this window */
    close();

    /** Wait for running apps to close */
    mSessHndlr->closeRunningApps();

    /** Session handling */
    mSessionClient->sendMessage( "logout" );

    /* We manually close this app */
    qApp->quit();
}


void PowerDialog::suspend() {
    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 100 * 1000 ) == false ) {
        return;
    }

    mSessionClient->sendMessage( "suspend" );

    /* We manually close this app */
    qApp->quit();
}


void PowerDialog::hibernate() {
    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 100 * 1000 ) == false ) {
        return;
    }

    mSessionClient->sendMessage( "hibernate" );

    /* We manually close this app */
    qApp->quit();
}


void PowerDialog::poweroff() {
    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 100 * 1000 ) == false ) {
        return;
    }

    /** Hide this window */
    close();

    /** Wait for running apps to close */
    mSessHndlr->closeRunningApps();

    mSessionClient->sendMessage( "shutdown" );

    /* We manually close this app */
    qApp->quit();
}


void PowerDialog::reboot() {
    /** Wait 100 ms for connection */
    if ( mSessionClient->waitForRegistered( 100 * 1000 ) == false ) {
        return;
    }

    /** Hide this window */
    close();

    /** Wait for running apps to close */
    mSessHndlr->closeRunningApps();

    mSessionClient->sendMessage( "reboot" );

    /* We manually close this app */
    qApp->quit();
}


void PowerDialog::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    QPalette pltt( palette() );
    QColor   bg( pltt.color( QPalette::Window ) );

    bg.setAlphaF( 0.8 );

    painter.setPen( Qt::NoPen );
    painter.setBrush( bg );

    painter.drawRect( rect() );
    painter.end();

    QWidget::paintEvent( pEvent );
}


void PowerDialog::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        close();
        qApp->quit();
    }

    QWidget::keyReleaseEvent( kEvent );
}
