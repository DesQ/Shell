/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <csignal>

// Local Headers
#include "Global.hpp"
#include "Logout.hpp"

#include <QScreen>
#include <signal.h>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFApplication.hpp>

WQt::Registry     *wlRegistry = nullptr;
DFL::IPC::Wayfire *wfIpc      = nullptr;


int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Logout.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Logout started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Logout" );
    app->setApplicationVersion( PROJECT_VERSION );

    app->setQuitOnLastWindowClosed( false );

    QObject::connect(
        app, &DFL::Application::unixSignalReceived, [ app ] ( int signum ) {
            if ( ( signum == SIGTERM ) || ( signum == SIGQUIT ) || ( signum == SIGINT ) ) {
                app->quit();
            }
        }
    );

    if ( app->lockApplication() ) {
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );

        wlRegistry->setup();

        wfIpc = new DFL::IPC::Wayfire( qgetenv( "WAYFIRE_SOCKET" ) );
        wfIpc->connectToServer();

        qDebug() << "Starting PowerDialog";

        for ( QScreen *screen: qApp->screens() ) {
            PowerDialog *dlg = new PowerDialog( screen );

            QObject::connect(
                app, &DFL::Application::screenRemoved, [ = ] ( QScreen *scrn ) {
                    if ( scrn == screen ) {
                        dlg->close();
                        delete dlg;
                    }
                }
            );

            dlg->show();
        }

        QObject::connect(
            app, &QApplication::screenAdded, [ app ] ( QScreen *screen ) {
                PowerDialog *pwrDlg = new PowerDialog( screen );

                QObject::connect(
                    app, &DFL::Application::screenRemoved, [ = ] ( QScreen *scrn ) {
                        if ( scrn == screen ) {
                            pwrDlg->close();
                            delete pwrDlg;
                        }
                    }
                );

                pwrDlg->show();
            }
        );

        return app->exec();
    }

    return 0;
}
