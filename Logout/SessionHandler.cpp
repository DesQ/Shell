/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <signal.h>

#include <QObject>
#include <QString>

#include <desq/Utils.hpp>

#include <DFUtils.hpp>
#include <DFIpcClient.hpp>
#include <DFGuiApplication.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

#include <wayqt/WayfireInformation.hpp>

#include "Global.hpp"
#include "SessionHandler.hpp"

SessionHandler::SessionHandler() {
    connect(
        wfIpc, &DFL::IPC::Wayfire::viewUnmapped, [ = ]( QJsonDocument event ) {
            QJsonObject view = event.object()[ "view" ].toObject();
            int64_t id       = view[ "id" ].toInt();

            if ( openViews.contains( id ) ) {
                openViews.removeAll( id );
            }
        }
    );
}


SessionHandler::~SessionHandler() {
    if ( wfIpc != nullptr ) {
        delete wfIpc;
    }
}


void SessionHandler::closeRunningApps() {
    if ( wfIpc == nullptr ) {
        return;
    }

    QJsonArray toplevels = wfIpc->listViews();
    qCritical() << QJsonDocument( toplevels ).toJson().constData();

    for ( uint i = 0; i < toplevels.size(); i++ ) {
        QJsonObject view = toplevels[ i ].toObject();

        if ( view[ "role" ].toString() == "toplevel" ) {
            int64_t id = view[ "id" ].toInt( 0 );

            if ( view[ "id" ].toInt() == getpid() ) {
                continue;
            }

            openViews << id;

            qCritical() << "Closing view" << view[ "id" ].toInt();

            wfIpc->closeView( id );
        }
    }

    /** Blocking call */
    monitorToplevels();
}


void SessionHandler::killRunningApps() {
    QJsonArray toplevels = wfIpc->listViews();

    for ( uint i = 0; i < toplevels.size(); i++ ) {
        QJsonObject view = toplevels[ i ].toObject();

        if ( view[ "role" ].toString() == "toplevel" ) {
            int pid = view[ "pid" ].toInt();
            kill( pid, SIGKILL );
        }
    }
}


void SessionHandler::monitorToplevels( uint32_t timeout ) {
    QElapsedTimer timer;

    /** We need the timer only if we have a non-zero timeout */
    if ( timeout > 0 ) {
        timer.start();
    }

    /** Wait for all toplevels to close */
    while ( openViews.length() ) {
        /** Sleep for a microsecond.+ */
        QThread::usleep( 1 );

        /** Process the pending events */
        QCoreApplication::processEvents();

        /** Kill all the applications after timeout */
        if ( timer.isValid() && ( timer.elapsed() >= timeout ) ) {
            killRunningApps();
            break;
        }
    }

    emit safeToQuit();
}
