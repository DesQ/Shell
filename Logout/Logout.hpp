/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QDialog>
#include <QWidget>
#include <QToolButton>

#include "SessionHandler.hpp"

namespace DFL {
    namespace IPC {
        class Client;
    }
}

namespace WQt {
    class LayerSurface;
}

class PowerDialog : public QWidget {
    Q_OBJECT

    public:
        PowerDialog( QScreen * );
        ~PowerDialog();

        /** Override show() and close() to use layer-shell */
        Q_SLOT void show();
        Q_SLOT void close();

    private:
        QToolButton *lockBtn, *logOutBtn, *suspendBtn, *hibernateBtn, *powerOffBtn, *rebootBtn, *cancelBtn;

        void lock();
        void logout();
        void suspend();
        void hibernate();
        void poweroff();
        void reboot();

        void createUI();
        void prepareIpcClient();

        QScreen *mScreen       = nullptr;
        WQt::LayerSurface *cls = nullptr;

        int uiMode;

        QString mSessionSockPath;

        SessionHandler *mSessHndlr       = nullptr;
        DFL::IPC::Client *mSessionClient = nullptr;

    protected:
        void paintEvent( QPaintEvent *pEvent );
        void keyReleaseEvent( QKeyEvent *kEvent );
};
