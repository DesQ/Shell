/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>
#include <DFWayfireIPC.hpp>

class SessionHandler : public QObject {
    Q_OBJECT

    public:
        SessionHandler();
        ~SessionHandler();

        /**
         * Use xdg-shell to intimate the applications to close.
         * Once all toplevels have been closed, safeToClose will be emitted.
         * This app will wait for
         */
        void closeRunningApps();

        /**
         * Kill the running apps by sending SIGKILL.
         * Once all toplevels have been closed, safeToClose will be emitted.
         */
        void killRunningApps();

        /**
         * Signal for the main process to close session
         */
        Q_SIGNAL void safeToQuit();

    private:
        /** Wait for the toplevels to close */
        void monitorToplevels( uint32_t timeout = 0 );

        QList<int64_t> openViews;
};
