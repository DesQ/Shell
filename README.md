# DesQ Shell

The user interface for DesQ. DesQ Shell is a group of components that form the main user interface.

Currently DesQ Shell consists of the following components:
- Manager - The shell manager to manage various components.
- Scene   - The background.
- Cask    - The widgets container.
- Menu    - The applications menu.
- Panel   - The panel to show running applications, pager and system tray.

The user experience can be enhanced by installing various DesQ Utilities like Disks, Power Manager, Volume, Runner, etc...

<!-- Compilation Instructions -->
<details markdown="1">
<summary>Notes for compiling on Linux</summary>

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/Shell.git Shell`
- Enter the `Shell` folder
  * `cd Shell`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`
</details>


<!-- Dependency details -->
<details markdown="1">
<summary>Dependencies</summary>

#### Compile-time dependencies
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* QtWayland (qt5wayland5-dev, qtwayland5-private-dev)
* Qt SVG (libqt5svg5-dev)
* libsensors (libsensors-dev)
* libdbusmenu-qt5 (libdbusmenu-qt5-dev)
* wayland (libdbusmenu-qt5-dev)
* [libdesq](https://gitlab.com/DesQ/libdesq)
* [libdesqui](https://gitlab.com/DesQ/libdesqui)
* [DFL::Utils](https://gitlab.com/desktop-frameworks/utils)
* [DFL::Xdg](https://gitlab.com/desktop-frameworks/xdg)
* [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc)
* [DFL::Applications](https://gitlab.com/desktop-frameworks/applications)
* [DFL::Settings](https://gitlab.com/desktop-frameworks/settings)
* [DFL::Layouts](https://gitlab.com/desktop-frameworks/layouts)
* [DFL::WayQt](https://gitlab.com/desktop-frameworks/wayqt)
* [DFL::SNI](https://gitlab.com/desktop-frameworks/status-notifier)
* [DFL::Login1](https://gitlab.com/desktop-frameworks/login1)
* [DFL::Inotify](https://gitlab.com/desktop-frameworks/inotify)
* [DFL::ColorUtils](https://gitlab.com/desktop-frameworks/color-utils)


#### Runtime Dependencies
In addition to the compile time dependencies, you'll need the following for a complete DesQ Experience.
* [Wayfire](https://github.com/WayfireWM/wayfire/)
* [Wayfire Plugins Extra](https://github.com/WayfireWM/wayfire/)
* [Wayfire Plugin hjson](https://gitlab.com/wayfireplugins/wf-config-hjson) - Hjson Config Backend
* [Wayfire Plugin windecor](https://gitlab.com/wayfireplugins/windecor) - Window Decorations
* [Wayfire Plugin focus-request](https://gitlab.com/wayfireplugins/focus-request) - To enable client-side activate requests
</details>


### Known Bugs
* Pinning a window on all desktops does not work.
* DesQ Menu sometimes shows duplicate entries when new desktops are added/removed.
* DesQ Menu and DesQ Panel can hang on some systems.


### Upcoming
* Any other feature you request for... :)
