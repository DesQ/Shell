/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

class AppButton : public QWidget {
    Q_OBJECT

    public:
        AppButton( QWidget *parent = 0 );

        void setFixedHeight( int );

    private:
        void updateApps();

        QPushButton *appBtn;
        QToolButton *appListBtn;

        QMenu *appList;
        QMenu *menuBar;
};

class TrayButton : public QToolButton {
    Q_OBJECT

    public:
        TrayButton( QWidget *parent = 0 );
};

class InfoButton : public QToolButton {
    Q_OBJECT;

    public:
        InfoButton( QWidget *parent = 0 );
};

class PowerButton : public QToolButton {
    Q_OBJECT;

    public:
        PowerButton( QWidget *parent = 0 );
};

class Spacer {
    public:
        static QWidget * horizontal( int );
        static QWidget * vertical( int );
};

class Separator : public QWidget {
    Q_OBJECT;

    public:
        static QWidget * vertical( QWidget *parent   = 0 );
        static QWidget * horizontal( QWidget *parent = 0 );

    private:
        enum Mode {
            Horizontal,
            Vertical
        };

        Separator( Separator::Mode mode, QWidget *parent = 0 );

        QLinearGradient vGrad, hGrad;
        Separator::Mode mMode;

    protected:
        void paintEvent( QPaintEvent * );
        void resizeEvent( QResizeEvent * );
};
