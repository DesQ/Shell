/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <csignal>

// LibDesQ Headers
#include <QtWidgets>
#include <desq/Utils.hpp>
#include <desqui/Application.hpp>

// Wayland Headers
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/Application.hpp>

class Notification : public QWidget {
    Q_OBJECT;

    public:
        inline Notification() {
            setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint );
            setAttribute( Qt::WA_TranslucentBackground );

            setFixedWidth( 270 );

            QLabel *icon = new QLabel();

            icon->setFixedWidth( 75 );
            icon->setMinimumHeight( 75 );
            icon->setPixmap( QIcon::fromTheme( "firefox" ).pixmap( 64, 64 ) );
            icon->setAlignment( Qt::AlignHCenter | Qt::AlignTop );
            icon->setSizePolicy( QSizePolicy( QSizePolicy::Fixed, QSizePolicy::MinimumExpanding ) );

            QLabel *text = new QLabel();

            text->setText(
                "<p><b><large>Notification title</large></b><br>"
                "This is a sample notification. We can have <i>rich text</i> in it.</p>"
                "<hr>"
                "<p><b><large>Notification title 2</large></b><br>"
                "This is a another notification. We can have <u><i>fancy text</i></u> in it.</p>"
            );
            text->setWordWrap( true );

            QWidget *base = new QWidget();

            base->setObjectName( "base" );

            QHBoxLayout *baseLyt = new QHBoxLayout();

            baseLyt->addWidget( icon, Qt::AlignHCenter | Qt::AlignTop );
            baseLyt->addWidget( text );
            base->setLayout( baseLyt );

            QHBoxLayout *lyt = new QHBoxLayout();

            lyt->setContentsMargins( QMargins() );

            lyt->addWidget( base );
            setLayout( lyt );
        }

    protected:
        void paintEvent( QPaintEvent *pEvent ) {
            QPainter painter( this );

            painter.setRenderHint( QPainter::Antialiasing );

            painter.setPen( QPen( Qt::gray, 1 ) );
            painter.setBrush( QColor( 70, 70, 70, 70 ) );
            painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 4, 4 );

            painter.end();
        }
};

int main( int argc, char **argv ) {
    qInstallMessageHandler( DesQ::Logger );
    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    WQt::Application app( "DesQExec", argc, argv );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    Notification *nott = new Notification();

    nott->show();

    if ( WQt::Utils::wlOutputFromQScreen() ) {
        WQt::Registry *registry = qApp->waylandRegistry();

        WQt::LayerSurface *cls = registry->layerShell()->getLayerSurface(
            nott->windowHandle(),
            nullptr,
            WQt::LayerShell::Overlay,
            "de-widget"
        );

        cls->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Right );
        cls->setMargins( QMargins( 10, 10, 10, 10 ) );
        cls->setSurfaceSize( nott->size() );
        cls->setExclusiveZone( 0 );
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );
        cls->apply();
    }

    return app.exec();
}


#include "Main.moc"
