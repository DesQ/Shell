/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "PanelGlobal.hpp"

class ClockLabel;
class InfoButton;
class TrayButton;
class PagerWidget;
class PowerButton;
class ShowDesktop;
class StatusNotifierHost;

namespace DesQ {
    namespace Panel {
        class TaskButton;
    }

    namespace Plugin {
        class PanelInterface;
    }
}

namespace WQt {
    class LayerSurface;
}

class DesQ::Panel::ModernUI : public QWidget {
    Q_OBJECT;

    public:
        enum Position {
            Left = 0x748C63,
            Top,
            Right,
            Bottom
        };

        enum AutoHide {
            Never = 0xD7042F,
            Always,
            WindowsCanCover
        };

        ModernUI( QScreen * );
        ~ModernUI();

    protected:
        void paintEvent( QPaintEvent *event ) override;

    private:
        /** Load all the plugins */
        void buildPanel();

        /** Resize the Panel if and when the screen resizes */
        void autoResize();

        /** Show the popup returned by DesQ::Panel::PluginWidget */
        void showPopup( QWidget *, QWidget * );

        /** Show the popup returned by DesQ::Panel::PluginWidget */
        void showTooltip( QWidget *, QWidget * );

        /** Load the plugin given it's name */
        void loadPlugin( QString pluginName );

        /** The base layout in which the plugin widgets are added */
        QHBoxLayout *widgetLyt = nullptr;

        /** List of active plugin */
        QList<DesQ::Plugin::PanelInterface *> mActivePlugins;

        WQt::LayerSurface *popupSurf   = nullptr;
        WQt::LayerSurface *tooltipSurf = nullptr;

        /** Height of the panel */
        int panelHeight = 36;

        /** The screen on which the panel is visible */
        QScreen *mScreen = nullptr;

        /** Pointer to the popup/tooltip that's already being displayed */
        QWidget *mPopup   = nullptr;
        QWidget *mTooltip = nullptr;
};
