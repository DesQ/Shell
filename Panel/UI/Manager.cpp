/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Manager.hpp"
#include "ModernUI.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFApplication.hpp>
#include <wayqt/WayfireShell.hpp>

DesQ::Panel::Manager::Manager() {
    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn );

        QVariantAnimation *showAnim = createAnimator( mSurfaces[ scrn->name() ], panelInstances[ scrn->name() ], true );
        showAnim->start();
        mInstanceStates[ scrn->name() ] = true;
    }

    connect(
        qApp, &QApplication::screenAdded, [ = ] ( QScreen *screen ) {
            createInstance( screen );

            QVariantAnimation *showAnim = createAnimator( mSurfaces[ screen->name() ], panelInstances[ screen->name() ], true );
            showAnim->start();
            mInstanceStates[ screen->name() ] = true;
        }
    );

    /** Delete the instance and the layer surface when the screen is disconnected */
    connect(
        qApp, &DFL::Application::screenRemoved, [ = ] ( QScreen *screen ) {
            if ( panelInstances.contains( screen->name() ) ) {
                delete mSurfaces.take( screen->name() );

                DesQ::Panel::ModernUI *ui = panelInstances.take( screen->name() );
                ui->close();
                delete ui;
            }
        }
    );
}


DesQ::Panel::Manager::~Manager() {
}


void DesQ::Panel::Manager::handleMessages( QString mesg, int fd ) {
    if ( mesg.startsWith( "reload" ) ) {
    }

    else if ( mesg.startsWith( "raise" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) ) {
            raiseInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "lower" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) ) {
            lowerInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "toggle" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( mInstanceStates[ parts[ 1 ] ] ) {
            hideLayerSurface( parts[ 1 ] );
            qApp->messageClient( "hidden", fd );
        }

        else {
            showLayerSurface( parts[ 1 ] );
            qApp->messageClient( "shown", fd );
        }
    }
}


void DesQ::Panel::Manager::createInstance( QScreen *scrn ) {
    DesQ::Panel::ModernUI *ui = new DesQ::Panel::ModernUI( scrn );

    ui->show();

    /** Function to show/hide the panel if AutoHide != Never */
    std::function<void( QScreen *, WQt::WfOutput::HotSpotPoint, DesQ::Panel::ModernUI *, bool )> setupHotSpot =
        [ = ] ( QScreen *scrn, WQt::WfOutput::HotSpotPoint location, DesQ::Panel::ModernUI *panel, bool hide ) {
            /**
             * scrn     - The screen on which this panel is visible.
             * location - Location of the panel on this screen.
             * panel    - The DesQ::Panel::ModernUI instance.
             * hide     - Flag to idicate hide/lower the panel.
             **/

            /** Get the wl_output for the given screen */
            wl_output *wlout = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );

            /** Get the WQt::WfOutput for the given screen */
            WQt::WfOutput *wfOut = wlRegistry->wayfireShell()->getOutput( wlout );

            /** Get the hotspot instance */
            WQt::HotSpot *panelHS = wfOut->createHotSpot( location, 5, 250 );

            /** A singleshot timer, that keeps activating until the panel is hidden */
            QTimer *hideTimer = new QTimer();
            hideTimer->setSingleShot( true );
            hideTimer->setInterval( 2500 );
            connect(
                hideTimer, &QTimer::timeout, [ = ] () {
                    /** Panel in still under the mouse */
                    if ( panel->underMouse() ) {
                        qDebug() << "Under mouse";
                        hideTimer->start();
                    }

                    /** Mouse is away, hide/lower the panel */
                    else {
                        /** Panel is to be hidden */
                        if ( hide ) {
                            qDebug() << "Panel hiding";
                            hideLayerSurface( scrn->name() );
                        }

                        /** Panel is to be lowered */
                        else {
                            qDebug() << "Panel lowering";
                            lowerInstance( scrn->name() );
                        }

                        hideTimer->stop();
                    }
                }
            );

            /** Show the panel when we enter the hotspot */
            connect(
                panelHS, &WQt::HotSpot::enteredHotSpot, [ = ] () {
                    if ( hide ) {
                        showLayerSurface( scrn->name() );
                        hideTimer->start();
                    }

                    else {
                        raiseInstance( scrn->name() );
                        hideTimer->start();
                    }
                }
            );

            /** Start the hide timer */
            hideTimer->start();

            panelHS->setup();
        };

    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType  lyr   = WQt::LayerShell::Top;
        WQt::WfOutput::HotSpotPoint hsLoc = WQt::WfOutput::Top;

        /** wl_output corresponding to @screen */
        wl_output         *output = WQt::Utils::wlOutputFromQScreen( scrn );
        WQt::LayerSurface *cls    = wlRegistry->layerShell()->getLayerSurface( ui->windowHandle(), output, lyr, "desq-panel" );

        /** Stuck to the Top/Bottom (for now). We need to implement settings */
        switch ( (int)panelSett->value( "Position" ) ) {
            case DesQ::Panel::ModernUI::Left: {
                cls->setAnchors( WQt::LayerSurface::Top );
                break;
            }

            case DesQ::Panel::ModernUI::Top: {
                cls->setAnchors( WQt::LayerSurface::Top );
                break;
            }

            case DesQ::Panel::ModernUI::Right: {
                cls->setAnchors( WQt::LayerSurface::Top );
                break;
            }

            case DesQ::Panel::ModernUI::Bottom: {
                cls->setAnchors( WQt::LayerSurface::Top );
                hsLoc = WQt::WfOutput::Top;
                break;
            }
        }

        /** Size of our surface */
        cls->setSurfaceSize( ui->size() );

        /** No margins (for now). We can implement DesQ Dock like settings for floating panel that hides */
        cls->setMargins( QMargins( 0, -ui->height(), 0, 0 ) );

        if ( (int)panelSett->value( "AutoHide" ) == DesQ::Panel::ModernUI::Never ) {
            /** Do not move this surface at any cost. */
            cls->setExclusiveZone( ui->height() );
        }

        /** We may need keyboard interaction (in future). Nothing needed now */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Commit to our choices */
        cls->apply();

        panelInstances[ scrn->name() ]  = ui;
        mSurfaces[ scrn->name() ]       = cls;
        mSurfaceStates[ scrn->name() ]  = lyr;
        mInstanceStates[ scrn->name() ] = true;

        qApp->processEvents();

        /** Apply the AutoHide features */
        switch ( (int)panelSett->value( "AutoHide" ) ) {
            case DesQ::Panel::ModernUI::Never: {
                /** Nothing to do!! */
                break;
            }

            case DesQ::Panel::ModernUI::Always: {
                setupHotSpot( scrn, hsLoc, ui, true );
                break;
            }

            case DesQ::Panel::ModernUI::WindowsCanCover: {
                setupHotSpot( scrn, hsLoc, ui, false );
                break;
            }
        }
    }

    else {
        ui->close();
        delete ui;
    }
}


void DesQ::Panel::Manager::destroyInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) ) {
        WQt::LayerSurface *surf = mSurfaces.take( opName );
        delete surf;
    }

    if ( mSurfaceStates.contains( opName ) ) {
        mSurfaceStates.remove( opName );
    }

    if ( panelInstances.contains( opName ) ) {
        DesQ::Panel::ModernUI *w = panelInstances.take( opName );
        w->close();
        delete w;
    }

    if ( mInstanceStates.contains( opName ) ) {
        mInstanceStates.remove( opName );
    }
}


void DesQ::Panel::Manager::raiseInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) == false ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Top ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Top;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::lowerInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) == false ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Bottom ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Bottom;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Bottom );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::showLayerSurface( QString opName ) {
    if ( panelInstances.contains( opName ) == false ) {
        return;
    }

    mSurfaceStates[ opName ] = WQt::LayerShell::Top;

    mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
    mSurfaces[ opName ]->apply();

    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], true );

    showAnim->start();

    mInstanceStates[ opName ] = true;
}


void DesQ::Panel::Manager::hideLayerSurface( QString opName ) {
    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], panelInstances[ opName ], false );

    hideAnim->start();

    mInstanceStates[ opName ] = false;
}


QVariantAnimation * DesQ::Panel::Manager::createAnimator( WQt::LayerSurface *surf, DesQ::Panel::ModernUI *ui, bool show ) {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    if ( show ) {
        anim->setStartValue( -ui->height() );
        anim->setEndValue( 0 );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::OutCubic ) );
    }

    else {
        anim->setStartValue( 0 );
        anim->setEndValue( -ui->height() );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuart ) );
    }

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
            surf->setMargins( QMargins( 0, val.toInt(), 0, 0 ) );
            surf->apply();

            qApp->processEvents();
        }
    );

    return anim;
}
