/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "PanelGlobal.hpp"
#include "ModernUI.hpp"

namespace DesQ {
    namespace Panel {
        class Manager;
    }
}

class QVariantAnimation;

/**
 * @class DesQ::Panel::Manager
 * This class will handle the creation, showing, hiding and destruction
 * of DesQ::Panel::ModernUI UI on one or all screens.
 * Additionally, it will also handle the CLI requests
 */
class DesQ::Panel::Manager : public QObject {
    Q_OBJECT;

    public:
        Manager();
        ~Manager();

        /**
         * Load the user settings.
         */
        void loadSettings();

        /**
         * Handle the requests from other instance. To reply to queries,
         * write suitable data to the file descriptor @fd.
         */
        Q_SLOT void handleMessages( QString msg, int fd );

        /**
         * Start an instance of DesQ::Panel::ModernUI on screen @scrn
         */
        void createInstance( QScreen *scrn );

        /**
         * Stop an instance of DesQ::Panel::ModernUI on screen named @opName
         */
        void destroyInstance( QString opName );

        /**
         * Raise an existing instance of DesQ::Panel::ModernUI
         * which is open on screen named @opName.
         * NOTE: To be used when DesQ::Panel::ModernUI is shown on desktop.
         */
        void raiseInstance( QString opName );

        /**
         * Lower an existing instance of DesQ::Panel::ModernUI
         * which is open on screen named @opName.
         * NOTE: To be used when DesQ::Panel::ModernUI is shown on desktop.
         */
        void lowerInstance( QString opName );

        /**
         * Show a layer surface on the output named @opName
         * If no instance of DesQ::Panel::ModernUI exists on this output
         * nothing will be done.
         * NOTE: This will automatically show the widget as top layer.
         */
        void showLayerSurface( QString opName );

        /**
         * Hide a layer surface on the output named @opName
         */
        void hideLayerSurface( QString opName );

    private:
        QHash<QString, bool> mInstanceStates;

        QHash<QString, WQt::LayerSurface *> mSurfaces;
        QHash<QString, WQt::LayerShell::LayerType> mSurfaceStates;

        bool mBackgroundMode = false;

        /**
         * Return an animator which will animate a layer surface when started.
         * @surf - Layer surface to be moved
         * @ui   - Instance of DesQ::Panel::ModernUI (used to calculate the margin positions)
         * @show - If true, the widget is being shown, otherwise hidden
         */
        QVariantAnimation *createAnimator( WQt::LayerSurface *surf, DesQ::Panel::ModernUI *ui, bool show );
};
