/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include "Global.hpp"
#include "ModernUI.hpp"
#include "StyleSheet.hpp"

#include "Widgets.hpp"

#include <desq/Utils.hpp>

#include <desq/desq-config.h>
#include <desqui/PanelPlugin.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>


static inline DesQ::Panel::PluginWidget * failedPlugin( QString pluginName, QWidget *parent ) {
    QLabel *lbl = new QLabel();

    lbl->setAlignment( Qt::AlignCenter );

    int panelHeight = ( int )panelSett->value( "Height" );

    lbl->setFixedSize( panelHeight, panelHeight );

    lbl->setPixmap( QIcon::fromTheme( "computer-fail-symbolic" ).pixmap( 32, 32 ) );

    DesQ::Panel::PluginWidget *base    = new DesQ::Panel::PluginWidget( parent );
    QHBoxLayout               *baseLyt = new QHBoxLayout();

    baseLyt->setContentsMargins( QMargins() );
    baseLyt->addWidget( lbl );
    base->setLayout( baseLyt );

    QLabel *popup = new QLabel( "Failed to load the plugin " + pluginName );

    popup->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    popup->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );

    QObject::connect(
        base, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            emit base->showTooltip( popup );
        }
    );

    return base;
}


DesQ::Panel::ModernUI::ModernUI( QScreen *scrn ) : QWidget() {
    mScreen = scrn;

    qDebug() << "Creating panel view for" << scrn->name();

    /* No frame, and stay at bottom */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

    panelHeight = ( int )panelSett->value( "Height" );

    setAttribute( Qt::WA_TranslucentBackground );
    setContentsMargins( QMargins() );

    /* Set the windowFlags */
    setWindowFlags( wFlags );

    setWindowTitle( tr( "DesQ Panel" ) );

    buildPanel();
    autoResize();

    connect( qApp->primaryScreen(), &QScreen::geometryChanged, this, &DesQ::Panel::ModernUI::autoResize );
}


DesQ::Panel::ModernUI::~ModernUI() {
    delete widgetLyt;
    delete mScreen;
}


void DesQ::Panel::ModernUI::autoResize() {
    // Span the entire width of the screen
    QSize scrSize = mScreen->size();

    setFixedSize( scrSize.width(), panelHeight );
}


void DesQ::Panel::ModernUI::buildPanel() {
    widgetLyt = new QHBoxLayout();
    widgetLyt->setSpacing( 0 );
    widgetLyt->setContentsMargins( QMargins() );

    setLayout( widgetLyt );

    QStringList plugins = panelSett->value( "Plugins" );

    for ( QString plugin: plugins ) {
        if ( plugin == "separator" ) {
            widgetLyt->addWidget( Separator::vertical() );
        }

        else if ( plugin == "spacer" ) {
            widgetLyt->addStretch();
        }

        else {
            loadPlugin( plugin );
        }
    }
}


void DesQ::Panel::ModernUI::showPopup( QWidget *view, QWidget *parent ) {
    /** Simply close the popup if it's visible */
    if ( mPopup && mPopup->isVisible() ) {
        delete popupSurf;

        mPopup->close();
    }

    mPopup = view;
    view->show();

    WQt::LayerShell::LayerType lyr = WQt::LayerShell::Top;

    wl_output *output = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );
    popupSurf = wlRegistry->layerShell()->getLayerSurface( view->windowHandle(), output, lyr, "panel" );

    /** Stuck to the Top/Bottom (for now). We need to implement settings */
    popupSurf->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Left );

    /** Size of our surface */
    popupSurf->setSurfaceSize( view->size() );

    /** Claculate space on the left */
    QRect parGeom    = parent->geometry();
    int   parCenter  = parGeom.x() + parGeom.width() / 2;
    int   leftMargin = parCenter - view->width() / 2;

    if ( leftMargin < 0 ) {
        leftMargin = 0;
    }

    else if ( ( leftMargin + view->width() ) > mScreen->size().width() ) {
        leftMargin -= ( leftMargin + view->width() - mScreen->size().width() );
    }

    /** Leave a space of 10 px on the left and 10 px from the panel. */
    popupSurf->setMargins( QMargins( leftMargin, panelHeight + 10, 0, 0 ) );

    /** We may need keyboard interaction (in future). Nothing needed now */
    popupSurf->setKeyboardInteractivity( WQt::LayerSurface::OnDemand );

    /** Commit to our choices */
    popupSurf->apply();

    view->setFocus( Qt::MouseFocusReason );
}


void DesQ::Panel::ModernUI::showTooltip( QWidget *tip, QWidget *parent ) {
    /** Simply close the popup if it's visible */
    if ( mTooltip and mTooltip->isVisible() ) {
        /** Delete the layer surface first */
        delete tooltipSurf;
        tooltipSurf = nullptr;

        mTooltip->close();
    }

    if ( tip == nullptr ) {
        return;
    }

    mTooltip = tip;
    tip->show();

    WQt::LayerShell::LayerType lyr = WQt::LayerShell::Top;

    wl_output *output = WQt::Utils::wlOutputFromQScreen( qApp->primaryScreen() );
    tooltipSurf = wlRegistry->layerShell()->getLayerSurface( tip->windowHandle(), output, lyr, "panel" );

    /** Stuck to the Top/Bottom (for now). We need to implement settings */
    tooltipSurf->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Left );

    /** Size of our surface */
    tooltipSurf->setSurfaceSize( tip->size() );

    /** Calculate space on the left */
    QRect parGeom    = parent->geometry();
    int   parCenter  = parGeom.x() + parGeom.width() / 2;
    int   leftMargin = parCenter - tip->width() / 2;

    if ( leftMargin < 0 ) {
        leftMargin = 0;
    }

    else if ( ( leftMargin + tip->width() ) > mScreen->size().width() ) {
        leftMargin -= ( leftMargin + tip->width() - mScreen->size().width() );
    }

    /** Leave a space of 10 px on the left and 10 px from the panel. */
    tooltipSurf->setMargins( QMargins( leftMargin, panelHeight + 10, 0, 0 ) );

    /** We may need keyboard interaction (in future). Nothing needed now */
    tooltipSurf->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

    /** Commit to our choices */
    tooltipSurf->apply();
}


void DesQ::Panel::ModernUI::loadPlugin( QString pluginName ) {
    QPluginLoader loader( PluginPath "panel/libdesq-plugin-" + pluginName + ".so" );

    QObject *pObject = loader.instance();

    DesQ::Panel::PluginWidget *pluginWidget = nullptr;

    if ( pObject ) {
        DesQ::Plugin::PanelInterface *plugin = qobject_cast<DesQ::Plugin::PanelInterface *>( pObject );

        if ( plugin ) {
            pluginWidget = plugin->widget( this );
            pluginWidget->setPanelHeight( panelHeight );
            pluginWidget->setPanelScreen( mScreen );
            widgetLyt->addWidget( pluginWidget );

            connect(
                pluginWidget, &DesQ::Panel::PluginWidget::showPopup, [ = ] ( QWidget *popup ) {
                    showPopup( popup, pluginWidget );
                }
            );

            connect(
                pluginWidget, &DesQ::Panel::PluginWidget::hidePopup, [ = ] () {
                    delete popupSurf;
                    popupSurf = nullptr;

                    mPopup->close();
                }
            );

            connect(
                pluginWidget, &DesQ::Panel::PluginWidget::showTooltip, [ = ] ( QWidget *tooltip ) {
                    showTooltip( tooltip, pluginWidget );
                }
            );

            connect(
                pluginWidget, &DesQ::Panel::PluginWidget::hideTooltip, [ = ] () {
                    if ( mTooltip and mTooltip->isVisible() ) {
                        delete tooltipSurf;
                        tooltipSurf = nullptr;

                        mTooltip->close();
                    }
                }
            );

            connect(
                pluginWidget, &DesQ::Panel::PluginWidget::exited, [ = ] () {
                    if ( mTooltip and mTooltip->isVisible() ) {
                        delete tooltipSurf;
                        tooltipSurf = nullptr;

                        mTooltip->close();
                    }
                }
            );

            return;
        }
    }

    qWarning() << "Plugin Error:" << pluginName << loader.errorString();
    DesQ::Panel::PluginWidget *failed = failedPlugin( pluginName, this );

    /**
     * Connect the showTooltip signal to the corresponding slot.
     * We know for sure that when the mouse exits the widget, the toolip is closed.
     */
    connect(
        failed, &DesQ::Panel::PluginWidget::showTooltip, [ = ] ( QWidget *tooltip ) {
            showTooltip( tooltip, failed );
        }
    );

    connect(
        failed, &DesQ::Panel::PluginWidget::exited, [ = ] () {
            if ( mTooltip and mTooltip->isVisible() ) {
                delete tooltipSurf;
                tooltipSurf = nullptr;

                mTooltip->close();
            }
        }
    );


    widgetLyt->addWidget( failed );
}


void DesQ::Panel::ModernUI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();
    painter.setBrush( QColor( 0, 0, 0, 180 ) );
    painter.setPen( Qt::NoPen );
    painter.drawRect( 0, 0, width(), height() );
    painter.restore();

    painter.end();

    QWidget::paintEvent( pEvent );
}
