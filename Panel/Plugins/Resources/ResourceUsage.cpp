/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include <sensors/sensors.h>
#include <sensors/error.h>

#include "ResourceUsage.hpp"

Resource::Resource( QString icoTxt, QString suffixTxt ) {
    QFont f( "Font Awesome 6 Free", 10 );

    f.setStyleName( "Solid" );

    icon = new QLabel( icoTxt, this );
    icon->setAlignment( Qt::AlignCenter );
    icon->setToolTip( "Click to refresh" );
    icon->setFont( f );

    value = new QLabel( "--", this );
    value->setAlignment( Qt::AlignVCenter | Qt::AlignRight );

    suffix = new QLabel( suffixTxt, this );
    suffix->setAlignment( Qt::AlignVCenter | Qt::AlignLeft );

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 2 );

    lyt->addWidget( icon );
    lyt->addWidget( value );
    lyt->addWidget( suffix );

    setLayout( lyt );
}


Resource::~Resource() {
    delete icon;
    delete value;
    delete suffix;
}


void Resource::setValue( QString val ) {
    value->setText( val );
}


void Resource::setSuffix( QString sfx ) {
    suffix->setText( sfx );
}


ResourcesWidget::ResourcesWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    pluginSett    = new DFL::Settings( QDir::home().filePath( ".config/DesQ/ResourcesInfo.conf" ) );
    resourcesSett = new DFL::Settings( QDir::home().filePath( ".config/DesQ/ContainerPluginResources.conf" ) );

    cpu = new Resource( "\uF2DB", "%" );
    cpu->setFixedSize( (int)( 1.5 * panelHeight ), panelHeight );

    ram = new Resource( "\uF538", "%" );
    ram->setFixedSize( (int)( 1.5 * panelHeight ), panelHeight );

    temp = new Resource( "\uF2CB", "°C" );
    temp->setFixedSize( (int)( 1.5 * panelHeight ), panelHeight );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );

    bool canShow = false;

    if ( (bool)pluginSett->value( "ShowCPU" ) == true ) {
        lyt->addWidget( cpu );
        canShow  = true;
        mShowCpu = true;
    }

    if ( (bool)pluginSett->value( "ShowRAM" ) == true ) {
        lyt->addWidget( ram );
        canShow  = true;
        mShowRam = true;
    }

    if ( (bool)pluginSett->value( "ShowTemp" ) == true ) {
        lyt->addWidget( temp );
        canShow   = true;
        mShowTemp = true;
    }

    /** Show temperature by default, if nothing is shown */
    if ( canShow == false ) {
        lyt->addWidget( temp );
        mShowTemp = true;
    }

    setLayout( lyt );

    mTooltip = new QLabel();

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    setMouseTracking( true );

    // connect(
    //     this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
    //         if ( mTooltip and mTooltip->isVisible() ) {
    //             return;
    //         }
    //
    //         emit showTooltip( mTooltip );
    //     }
    // );

    mSysInfo = DesQ::SystemInfo::sysInfoObject();
    mTimer   = new QBasicTimer();

    mTimer->start( 1000, this );

    updateResourceUsage();
}


ResourcesWidget::~ResourcesWidget() {
    mTimer->stop();
    delete mTimer;

    delete cpu;
    delete ram;
    delete temp;

    delete mSysInfo;
}


void ResourcesWidget::updateResourceUsage() {
    QList<qint64> percents;

    if ( mShowCpu ) {
        cpu->setValue( QString::number( (int)mSysInfo->getCpuUsage().at( 0 ) ) );
    }

    if ( mShowRam ) {
        QList<qint64> ramVals = mSysInfo->getRamUsage();
        ram->setValue( QString::number( (int)( ramVals[ 1 ] > 1 ? 100.0 * ramVals[ 0 ] / ramVals[ 1 ] : 0.0 ) ) );
    }

    if ( mShowTemp ) {
        temp->setValue( QString::number( (int)readThermalInfo() ) );
    }
}


qint64 ResourcesWidget::readThermalInfo() {
    sensors_cleanup();

    int err = sensors_init( nullptr );

    if ( err != 0 ) {
        qCritical() << "Error:" << sensors_strerror( err );
        return 0;
    }

    int  chipNumber = 0;
    char buf[ 256 ] { 0 };
    const sensors_chip_name  *chip;
    const sensors_subfeature *sub;

    while ( ( chip = sensors_get_detected_chips( nullptr, &chipNumber ) ) ) {
        if ( sensors_snprintf_chip_name( buf, 256, chip ) < 0 ) {
            snprintf( buf, 256, "%i", chipNumber );
        }

        if ( buf != (QString)resourcesSett->value( "Chip" ) ) {
            continue;
        }

        const char *adap = sensors_get_adapter_name( &chip->bus );

        if ( adap != (QString)resourcesSett->value( "Adaptor" ) ) {
            continue;
        }

        int num = 0;
        const sensors_feature *feature;

        while ( ( feature = sensors_get_features( chip, &num ) ) ) {
            char   *str = sensors_get_label( chip, feature );
            double temp = 0.0;

            if ( str != (QString)resourcesSett->value( "Feature" ) ) {
                continue;
            }

            sub = sensors_get_subfeature( chip, feature, (sensors_subfeature_type)( ( (int)feature->type ) << 8 ) );

            sensors_get_value( chip, sub->number, &temp );
            free( str );

            return temp;
        }
    }

    return 50.0;
}


void ResourcesWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == mTimer->timerId() ) {
        updateResourceUsage();
    }

    QWidget::timerEvent( tEvent );
}
