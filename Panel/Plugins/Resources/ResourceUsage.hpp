/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QBasicTimer>
#include <QToolButton>
#include <QListWidget>

#include <desq/SysInfo.hpp>
#include <desqui/PanelPlugin.hpp>

#include <DFSettings.hpp>

class Resource : public QWidget {
    Q_OBJECT;

    public:
        Resource( QString icon, QString suffix );
        ~Resource();

        void setValue( QString );
        void setSuffix( QString );

    private:
        QLabel *icon   = nullptr;
        QLabel *value  = nullptr;
        QLabel *suffix = nullptr;
};

class ResourcesWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT

    public:
        ResourcesWidget( QWidget *parent = 0 );
        ~ResourcesWidget();

    private:
        Resource *cpu  = nullptr;
        Resource *ram  = nullptr;
        Resource *temp = nullptr;

        QLabel *mTooltip = nullptr;

        QString cpuText  = "\uf2db %1 %";
        QString ramText  = "\uf538 %1 %";
        QString tempText = "\uf2cb %1 °C";

        bool mShowCpu  = false;
        bool mShowRam  = false;
        bool mShowTemp = false;

        DesQ::SystemInfo *mSysInfo = nullptr;
        QBasicTimer *mTimer        = nullptr;

        DFL::Settings *pluginSett    = nullptr;
        DFL::Settings *resourcesSett = nullptr;

        void updateResourceUsage();
        qint64 readThermalInfo();

    protected:
        void timerEvent( QTimerEvent *tEvent );
};
