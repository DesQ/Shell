/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <desq/Utils.hpp>

#include "Bluetooth.hpp"

BluetoothPluginWidget::BluetoothPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    bluetooth = new QLabel();

    bluetooth->setPixmap( QIcon::fromTheme( "desq-bluetooth" ).pixmap( panelHeight - 4 ) );
    bluetooth->setAlignment( Qt::AlignCenter );
    bluetooth->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( bluetooth );
    setLayout( lyt );

    setMouseTracking( true );

    mPopup = new BluezPopup();
    mPopup->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    mPopup->setAttribute( Qt::WA_TranslucentBackground );
    mPopup->setStyleSheet( "background-color: rgba(0, 0, 0, 0); border-radius: 3px;" );

    /** Hide this popup when escape is pressed */
    connect( mPopup, &BluezPopup::hidePopup, this, &DesQ::Panel::PluginWidget::hidePopup );

    mTooltip = new QLabel( "Bluetooth" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            if ( mPopup && mPopup->isVisible() ) {
                emit hidePopup();
            }

            else {
                if ( mTooltip && mTooltip->isVisible() ) {
                    emit hideTooltip();
                }

                emit showPopup( mPopup );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip && mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );
}


BluetoothPluginWidget::~BluetoothPluginWidget() {
    if ( mTooltip ) {
        emit hidePopup();
    }

    delete bluetooth;
    delete mTooltip;
}
