/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>
#include <desqui/Switch.hpp>
#include <desqui/PanelPlugin.hpp>

#include <BluezQt/InitManagerJob>
#include <BluezQt/Device>
#include <BluezQt/Adapter>
#include <BluezQt/Manager>

class BluezPopup : public DesQ::Panel::PluginPopupWidget {
    Q_OBJECT;

    public:
        BluezPopup();

    private slots:
        /** Bluetooth Manager Initialized. Do some house-keeping */
        void onManagerInitialized( BluezQt::InitManagerJob *job );

        /** Start device discovery */
        void scanForDevices();

        /** A device was discovered */
        void deviceFound( BluezQt::DevicePtr device );

        /** Update the list of known and available devices */
        void updateDeviceList();

        /** Select a different adapter */
        void adapterChanged( int index );

        /** Power off/on a given Bluetooth adapter */
        void toggleAdapter( bool enabled );

        /** Update the buttons */
        void deviceSelected( const QModelIndex& index );

        /** Trust/untrust a given device */
        void toggleTrust();

        /** Pair a new device or Connect to a previously paired device */
        void performAction();

        /** Disconnect the selected device */
        void disconnectDevice();

        /** Update the list of known devices */
        void updateKnownDevices();

        /** Update the list of connections to a given device */
        void prepareDeviceConnections( BluezQt::DevicePtr );

    private:
        QComboBox *adapterComboBox      = nullptr;
        DesQUI::Switch *adapterCheckBox = nullptr;
        QToolButton *scanButton         = nullptr;

        QProgressBar *progress    = nullptr;
        QVariantAnimation *anim   = nullptr;
        QListView *deviceListView = nullptr;

        QToolButton *actionButton     = nullptr;
        QToolButton *trustButton      = nullptr;
        QToolButton *disconnectButton = nullptr;

        BluezQt::Manager *manager          = nullptr;
        BluezQt::AdapterPtr currentAdapter = nullptr;

        QList<BluezQt::DevicePtr> knownDevices;
        QList<BluezQt::DevicePtr> availableDevices;
        BluezQt::DevicePtr selectedDevice = nullptr;

        QString mID = "136";
};
