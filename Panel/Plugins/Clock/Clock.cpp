/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include "Clock.hpp"

ClockPluginWidget::ClockPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    clock = new QLabel();

    clock->setFont( QFont( font().family(), 10, QFont::Bold ) );
    clock->setText( QDateTime::currentDateTime().toString( "  ddd dd, hh:mm:ss AP  " ) );
    clock->setAlignment( Qt::AlignCenter );
    clock->setFixedWidth( clock->fontMetrics().averageCharWidth() * 20 );
    clock->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( clock );
    setLayout( lyt );

    cTimer = new QBasicTimer();
    cTimer->start( 1000, this );

    setMouseTracking( true );

    mPopup = new QWidget();
    mPopup->setObjectName( "Popup" );

    QHBoxLayout *popupLyt = new QHBoxLayout();

    popupLyt->setContentsMargins( QMargins() );
    popupLyt->addWidget( new QCalendarWidget( mPopup ) );
    mPopup->setLayout( popupLyt );
    mPopup->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    mPopup->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );

    mTooltip = new QLabel( "Click to show the Calendar widget." );
    mTooltip->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            if ( mPopup and mPopup->isVisible() ) {
                emit hidePopup();
            }

            else {
                emit showPopup( mPopup );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip and mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );
}


ClockPluginWidget::~ClockPluginWidget() {
    cTimer->stop();

    delete cTimer;
    delete clock;
    delete mPopup;
}


void ClockPluginWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == cTimer->timerId() ) {
        QString dt = QDateTime::currentDateTime().toString( "  ddd dd, hh:mm:ss AP  " );
        clock->setText( dt );
    }
}
