/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QDebug>
#include <QWidget>
#include <QHBoxLayout>
#include <QToolButton>

#include <nlohmann/json.hpp>

#include <desqui/PanelPlugin.hpp>

#include "dbustypes.hpp"

#ifndef QUIntList
    #define QUIntList    QList<uint>
#endif

namespace WQt {
    class ToplevelManager;
    class Toplevel;
    class DesQShell;
    class DesQOutput;
}

class TaskButton;

class TasksPluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT

    public:
        TasksPluginWidget( QWidget *parent = nullptr );
        ~TasksPluginWidget();

        /**
         * Add a button for a given task, or add the task to the existing button
         * We need to watch out for the following three settings
         * 1. ShowCurrentScreenOnly
         * 2. ShowCurrentWSetOnly
         * 3. ShowCurrentWorkspaceOnly
         */
        void addTask( QJsonObject );

        /**
         * Remove a button for the given task, or remove the task from the existing button
         * We need to watch out for the following three settings
         * 1. ShowCurrentScreenOnly
         * 2. ShowCurrentWSetOnly
         * 3. ShowCurrentWorkspaceOnly
         */
        void removeTask( int64_t, QString, bool noCheck = false );

    private:
        void populateLayout();

        QHBoxLayout *tasksLayout;

        int64_t mScreenId = 0;
        int64_t mWSetId   = 0;

        WorkSpace mWS;
        QSize mWSBounds;

        /** A map of app-id and task button */
        QMap<QString, TaskButton *> idTaskMap;

        /** Remove all the existing tasks */
        void clearLayout();
};


class TasksPlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "System Tray";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "window" );
        }

        /* The plugin version */
        QString version() override {
            return PROJECT_VERSION;
        }

        /* The clock Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new TasksPluginWidget( parent );
        }
};
