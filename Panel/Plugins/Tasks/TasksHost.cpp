/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>

#include <QDebug>
#include <QtDBus>
#include <QLabel>

#include "Global.hpp"
#include "TasksHost.hpp"
#include "Task.hpp"

#include <DFSettings.hpp>
#include <desq/Utils.hpp>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <DFXdg.hpp>

DFL::Settings *tasksSett = nullptr;

/**
 * All view geometries are provided relative to the current worksapce.
 * When the current workspace changes, the view geometries also change.
 * Thus if the center of this view is within the current ws bounds, then
 * we can consider the view to be in the current workspace.
 */
bool isSameWorkSpace( QJsonObject view, QSize bounds ) {
    QJsonObject viewInfo = wfIpc->getViewInfo( view[ "id" ].toInt() );
    QJsonObject geometry = view[ "geometry" ].toObject();

    int w = geometry[ "width" ].toInt();
    int h = geometry[ "height" ].toInt();
    int x = geometry[ "x" ].toInt();
    int y = geometry[ "y" ].toInt();

    QPoint viewCenter(
        (int)std::floor( x + w / 2.0 ),
        (int)std::floor( y + h / 2.0 )
    );

    return QRect( QPoint( 0, 0 ), bounds ).contains( viewCenter );
}


QIcon iconForAppId( QString appId ) {
    /** No appId set */
    if ( appId.trimmed().isEmpty() ) {
        return QIcon::fromTheme( "application-x-executable" );
    }

    /** appId == name of the desktop file */
    DFL::XDG::DesktopFile desktop( appId );

    /**
     * In case the appId does not amtch the desktop name,
     * let's check the lower case version of it
     */
    if ( desktop.isValid() == false ) {
        desktop = DFL::XDG::DesktopFile( appId.toLower() );
    }

    QString icon = desktop.icon();

    /** Themed icon name may be given in the desktop file */
    if ( icon.length() && QIcon::hasThemeIcon( icon ) ) {
        return QIcon::fromTheme( icon );
    }

    /** Full path may be given in the desktop file */
    if ( icon.length() && QFile::exists( icon ) ) {
        return QIcon( icon );
    }

    /** Check the app id directly! */
    if ( QIcon::hasThemeIcon( appId ) ) {
        return QIcon::fromTheme( appId );
    }

    /** Check the lower case version of app id directly */
    if ( QIcon::hasThemeIcon( appId.toLower() ) ) {
        return QIcon::fromTheme( appId.toLower() );
    }

    /** Not worknig hard: Just return application-x-executable */
    return QIcon::fromTheme( "application-x-executable" );
}


TasksPluginWidget::TasksPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    mWS.row    = 0;
    mWS.column = 0;

    tasksSett = DesQ::Utils::initializeDesQPluginSettings( "Panel", "Tasks" );

    tasksLayout = new QHBoxLayout();
    tasksLayout->setContentsMargins( QMargins() );
    tasksLayout->setSpacing( 5 );

    setLayout( tasksLayout );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceSetChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();

            QJsonObject wsInfo = response[ "new-wset-data" ].toObject();
            QJsonObject output = response[ "output-data" ].toObject();

            if ( output[ "id" ].toInt() == mScreenId ) {
                mWSetId = wsInfo[ "index" ].toInt();

                QJsonObject ws = wsInfo[ "workspace" ].toObject();
                mWS.row        = ws[ "y" ].toInt();
                mWS.column     = ws[ "x" ].toInt();

                if ( (bool)tasksSett->value( "ShowCurrentWSetOnly" ) == true ) {
                    clearLayout();
                    populateLayout();
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();

            QJsonObject wsInfo = response[ "new-workspace" ].toObject();
            QJsonObject output = response[ "output-data" ].toObject();

            /** Same output and wset */
            QJsonObject wset = response[ "wset-data" ].toObject();

            if ( ( output[ "id" ].toInt() == mScreenId ) && ( wset[ "index" ].toInt() == mWSetId ) ) {
                mWS.row    = wsInfo[ "y" ].toInt();
                mWS.column = wsInfo[ "x" ].toInt();

                if ( (bool)tasksSett->value( "ShowCurrentWorkspaceOnly" ) == true ) {
                    clearLayout();
                    populateLayout();
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewMapped, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();

            QJsonObject view = response[ "view" ].toObject();

            /** Ghost view: these are unmapped xwayland views */
            if ( view[ "pid" ].toInt() == -1 ) {
                return;
            }

            /** We want only the "toplevel" views */
            QString role = view[ "role" ].toString();

            if ( role != "toplevel" ) {
                return;
            }

            addTask( view );
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewFocused, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();

            QJsonObject view = response[ "view" ].toObject();

            if ( view.empty() ) {
                for ( QString key: idTaskMap.keys() ) {
                    idTaskMap[ key ]->setActive( false, "" );
                }
            }

            else if ( view[ "role" ].toString() != "toplevel" ) {
                return;
            }

            else {
                QString appId = view[ "app-id" ].toString();
                QString title = view[ "title" ].toString();

                /** Set all tasks as unfocused, except this one */
                for ( QString key: idTaskMap.keys() ) {
                    if ( key == appId ) {
                        idTaskMap[ key ]->setActive( true, title );
                    }

                    else {
                        idTaskMap[ key ]->setActive( false, "" );
                    }
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewTitleChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();
            QJsonObject view     = response[ "view" ].toObject();

            if ( view.empty() ) {
                return;
            }

            int64_t viewId = view[ "id" ].toInt();
            QString title  = view[ "title" ].toString();

            for ( TaskButton *btn: idTaskMap ) {
                if ( btn->tasks().contains( viewId ) ) {
                    btn->setTitle( viewId, title );
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewAppIdChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();
            QJsonObject view     = response[ "view" ].toObject();

            int64_t id       = view[ "id" ].toInt();
            QString newAppId = view[ "app-id" ].toString();

            /** We already have a button for this app-id */
            if ( idTaskMap.contains( newAppId ) ) {
                for ( TaskButton *btn: idTaskMap ) {
                    if ( btn->tasks().contains( id ) ) {
                        btn->removeTask( id );
                    }
                }
            }

            QString oldAppId;
            TaskButton *btn = nullptr;
            for ( TaskButton *b: idTaskMap ) {
                if ( b->tasks().contains( id ) ) {
                    btn      = b;
                    oldAppId = btn->appId();
                    break;
                }
            }

            /** Nothing to be done: Probably this view has not been mapped yet */
            if ( btn == nullptr ) {
                return;
            }

            /** Remove the existing task and add a new one */
            removeTask( id, oldAppId, true );
            addTask( view );
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewOutputChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();

            QJsonObject output = response[ "output" ].toObject();
            QJsonObject view   = response[ "view" ].toObject();

            QString appId = view[ "app-id" ].toString();

            /** This is added to this taskbar: do the needful */
            if ( idTaskMap.contains( appId ) ) {
                removeTask( view[ "id" ].toInt(), appId );
            }

            /** We may have to add it to this widget */
            else {
                addTask( view );
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewWorkspaceChanged, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();
            QJsonObject view     = response[ "view" ].toObject();

            QString appId = view[ "app-id" ].toString();

            /** This is added to this taskbar: do the needful */
            if ( idTaskMap.contains( appId ) ) {
                removeTask( view[ "id" ].toInt(), appId );
            }

            /** We may have to add it to this widget */
            else {
                addTask( view );
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::viewUnmapped, [ = ] ( QJsonDocument respJson ) {
            QJsonObject response = respJson.object();
            QJsonObject view     = response[ "view" ].toObject();

            QString appId = view[ "app-id" ].toString();

            /** This is added to this taskbar: do the needful */
            if ( ( appId == "nil" ) || ( appId == "" ) ) {
                appId = "unknown";
            }

            removeTask( view[ "id" ].toInt(), appId, true );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::panelScreenChanged, [ = ] ( QScreen *scrn ) {
            /** Clear the tasks */
            clearLayout();

            /** Set the screen bounds */
            mWSBounds = scrn->size();

            /** First get the output information */
            QJsonArray opInfoList = wfIpc->listOutputs();

            /** 'result' will be there only when something goes wrong */
            if ( opInfoList.count() == 0 ) {
                setDisabled( true );
                return;
            }

            /** And retrieve the output-id of the current screen */
            for ( uint i = 0; i < opInfoList.count(); i++ ) {
                QJsonObject opInfo = opInfoList[ i ].toObject();

                /** Get the name */
                QString opName = opInfo[ "name" ].toString();

                /** If this is the screen we are rendering */
                if ( opName == scrn->name() ) {
                    /** Save the output id */
                    mScreenId = opInfo[ "id" ].toInt();

                    /** Now we can set the workspace-set */
                    mWSetId = opInfo[ "wset-index" ].toInt();

                    populateLayout();

                    break;
                }
            }
        }
    );
}


TasksPluginWidget::~TasksPluginWidget() {
}


void TasksPluginWidget::populateLayout() {
    /** Add the existing views to the widget */
    QJsonArray views = wfIpc->listViews();

    if ( views.count() == 0 ) {
        setDisabled( true );
        return;
    }

    for ( uint v = 0; v < views.count(); v++ ) {
        QJsonObject view = views[ v ].toObject();

        /** Ghost view: these are unmapped xwayland views */
        if ( view[ "pid" ].toInt() == -1 ) {
            continue;
        }

        addTask( view );
    }
}


void TasksPluginWidget::addTask( QJsonObject view ) {
    QString appId  = view[ "app-id" ].toString();
    QString title  = view[ "title" ].toString();
    int64_t viewId = view[ "id" ].toInt();

    if ( ( appId == "nil" ) || ( appId == "" ) ) {
        appId = "unknown";
    }

    /** We want only the "toplevel" views */
    QString role = view[ "role" ].toString();

    if ( role != "toplevel" ) {
        return;
    }

    /** Show only those views that belong to this output */
    if ( (bool)tasksSett->value( "ShowCurrentScreenOnly" ) == true ) {
        if ( view[ "output-id" ].toInt() != mScreenId ) {
            return;
        }
    }

    /** Show only those views that belong to this wset */
    if ( (bool)tasksSett->value( "ShowCurrentWSetOnly" ) == true ) {
        if ( view[ "wset-index" ].toInt() != mWSetId ) {
            return;
        }
    }

    /** Show only those views that belong to this workspace */
    if ( (bool)tasksSett->value( "ShowCurrentWorkspaceOnly" ) == true ) {
        if ( isSameWorkSpace( view, mWSBounds ) == false ) {
            return;
        }
    }

    /** We already have a button for this */
    if ( idTaskMap.contains( appId ) ) {
        /** Add only if this view does not exist: Xwayland hack */
        if ( idTaskMap[ appId ]->tasks().contains( viewId ) == false ) {
            idTaskMap[ appId ]->addTask( view );
        }
    }

    else {
        TaskButton *btn = new TaskButton( this );

        btn->setAppId( appId );
        btn->setIcon( iconForAppId( appId ) );
        btn->setToolTip( title );
        btn->setToolButtonStyle( Qt::ToolButtonIconOnly );
        btn->setStyleSheet( "text-align: left" );
        btn->setFixedSize( QSize( panelHeight - 2, panelHeight - 2 ) );
        btn->addTask( view );

        tasksLayout->addWidget( btn );

        /** Show/hide popup */
        connect( btn, &TaskButton::showPopup, this, &TasksPluginWidget::showPopup );
        connect( btn, &TaskButton::hidePopup, this, &TasksPluginWidget::hidePopup );

        idTaskMap[ appId ] = std::move( btn );
    }

    /** Update the focus information if the current view has focus */
    if ( view[ "activated" ] == true ) {
        for ( TaskButton *btn: idTaskMap ) {
            btn->setActive( false, "" );
        }

        idTaskMap[ appId ]->setActive( true, title );
    }
}


void TasksPluginWidget::removeTask( int64_t id, QString appId, bool noCheck ) {
    if ( idTaskMap.contains( appId ) == false ) {
        qDebug() << "No task with app-id" << appId << "found";
        qDebug() << "Attempting to retrieve from view-id" << id;

        appId = "";
        for ( TaskButton *btn: idTaskMap ) {
            if ( btn->tasks().contains( id ) ) {
                appId = btn->appId();
                break;
            }
        }

        if ( appId == "" ) {
            return;
        }
    }

    if ( noCheck == false ) {
        QJsonObject view = wfIpc->getViewInfo( id );

        /** Show only those views that belong to this output */
        if ( (bool)tasksSett->value( "ShowCurrentScreenOnly" ) == true ) {
            if ( view[ "output-id" ].toInt() != mScreenId ) {
                removeTask( view[ "id" ].toInt(), appId, true );
                return;
            }
        }

        /** Show only those views that belong to this workspace-set */
        if ( (bool)tasksSett->value( "ShowCurrentWSetOnly" ) == true ) {
            if ( view[ "wset-index" ].toInt() != mWSetId ) {
                removeTask( view[ "id" ].toInt(), appId, true );
                return;
            }
        }

        /*
         * * Correct wset, but incorrect workspace
         ** Show only those views that belong to this workspace
         */
        if ( (bool)tasksSett->value( "ShowCurrentWorkspaceOnly" ) == true ) {
            if ( isSameWorkSpace( view, mWSBounds ) == false ) {
                removeTask( view[ "id" ].toInt(), appId, true );
                return;
            }
        }
    }

    idTaskMap[ appId ]->removeTask( id );

    /** No more tasks in this button: remove it */
    if ( idTaskMap[ appId ]->tasks().length() == 0 ) {
        TaskButton *btn = idTaskMap.take( appId );

        tasksLayout->removeWidget( btn );

        delete btn;
    }
}


void TasksPluginWidget::clearLayout() {
    for ( TaskButton *btn: idTaskMap ) {
        tasksLayout->removeWidget( btn );
        delete btn;
    }

    idTaskMap.clear();
}
