/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QMouseEvent>
#include <QToolButton>
#include <QWheelEvent>
#include <QLabel>
#include <QMenu>

#include <QJsonObject>

class QBasicTimer;
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

namespace DFL {
    class Settings;
}

extern DFL::Settings *tasksSett;

class ButtonPopup : public QWidget {
    Q_OBJECT;

    public:
        ButtonPopup();

        void addEntry( int64_t, QStringList );
        void updateEntry( int64_t, QString, QString );
        void removeEntry( int64_t );

        void clear();

        void show();
        void hide();

    private:
        QHBoxLayout *baseLyt;

        /**
         * 1. title
         * 2. active
         * 3. minimized
         * 4. maximized
         */
        QMap<int64_t, QStringList> idDetailsMap;

    protected:
        /** Mouse entered this widget */
        void enterEvent( QMouseEnterEvent * ) override;

        /** The mouse left this widget */
        void leaveEvent( QEvent * ) override;

    Q_SIGNALS:
        void entered();
        void exited();
};

class TaskButton : public QToolButton {
    Q_OBJECT;

    public:
        TaskButton( QWidget *parent );
        ~TaskButton();

        QString appId();
        void setAppId( QString );

        /** List the windows this button represents */
        QList<int64_t> tasks();

        /** Add a window to this list */
        void addTask( QJsonObject );

        /** Remove a window from this list */
        void removeTask( int64_t );

        /** Set as active */
        void setActive( bool, QString );

        /** Set title */
        void setTitle( int64_t, QString );

        /** Set/unset demands attentino */
        void setDemandsAttention( bool, QString );

    private:
        ButtonPopup *mPopup;
        QList<int64_t> mToplevels;

        QString mAppId;
        volatile bool mActive = false;

        QBasicTimer *showTimer;
        QBasicTimer *hideTimer;

        void updateInfo();

    protected:
        /** Mouse entered this widget */
        void enterEvent( QMouseEnterEvent * ) override;

        /** The mouse left this widget */
        void leaveEvent( QEvent * ) override;

        /** The to hide popup on mouse exit */
        void timerEvent( QTimerEvent * ) override;

    Q_SIGNALS:
        void showPopup( QWidget * );
        void hidePopup();
};
