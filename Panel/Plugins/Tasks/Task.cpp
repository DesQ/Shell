/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"

#include <QDebug>
#include <QScreen>
#include <QPainter>
#include <QBasicTimer>
#include <QVBoxLayout>

#include <DFSettings.hpp>

#include "Task.hpp"

ButtonPopup::ButtonPopup() : QWidget() {
    setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    setFixedSize( 180, 72 );

    baseLyt = new QHBoxLayout();
    baseLyt->setContentsMargins( QMargins( 5, 5, 5, 5 ) );
    baseLyt->setSpacing( 5 );
    setLayout( baseLyt );
}


void ButtonPopup::addEntry( int64_t id, QStringList details ) {
    idDetailsMap[ id ] = details;
}


void ButtonPopup::updateEntry( int64_t id, QString field, QString value ) {
    if ( field == "title" ) {
        idDetailsMap[ id ][ 0 ] = value;
    }

    else if ( field == "active" ) {
        idDetailsMap[ id ][ 1 ] = value;
    }

    else if ( field == "minimized" ) {
        idDetailsMap[ id ][ 2 ] = value;
    }

    else if ( field == "maximized" ) {
        idDetailsMap[ id ][ 3 ] = value;
    }
}


void ButtonPopup::removeEntry( int64_t id ) {
    idDetailsMap.remove( id );
}


void ButtonPopup::clear() {
    idDetailsMap.clear();
}


void ButtonPopup::show() {
    QWidget::show();
}


void ButtonPopup::hide() {
    QWidget::hide();
}


void ButtonPopup::enterEvent( QMouseEnterEvent *event ) {
    QWidget::enterEvent( event );

    emit entered();
}


void ButtonPopup::leaveEvent( QEvent *event ) {
    QWidget::leaveEvent( event );

    emit exited();
}


TaskButton::TaskButton( QWidget *parent ) : QToolButton( parent ) {
    /** No borders when not under mouse */
    setAutoRaise( true );

    /** A fairly large icon */
    setIconSize( QSize( 24, 24 ) );

    /** Button with focus will be checked */
    setCheckable( true );
    setChecked( false );

    connect(
        this, &QToolButton::clicked, [ = ] () {
            /**
             * 1. Single View
             *    If this view has focus, minimize it.
             *    It it is minimized, unminimize it and focus it.
             *
             * 2. Multiple views
             *    Every click switches to the next view.
             *    Once all the views are cycled through, minimize them all.
             *    Once all are minimized, give focus to the last focused view on click.
             */

            /** This vies is not active; activate it */
            if ( mActive == false ) {
                qCritical() << "Attempting to focus" << mToplevels[ 0 ] << wfIpc->focusView( mToplevels[ 0 ] );
            }

            /** This view is active, minimize it */
            else {
                qCritical() << "Attempting to minimize" << mToplevels[ 0 ] << wfIpc->minimizeView( mToplevels[ 0 ], true );
            }
        }
    );

    mPopup = new ButtonPopup();

    /** We don't want the popup to close
     * when the mouse is trying to interact with it
     */
    connect(
        mPopup, &ButtonPopup::entered, [ = ] {
            hideTimer->stop();
        }
    );

    /** We want the popup to close once the mouse has left it */
    connect(
        mPopup, &ButtonPopup::exited, [ = ] {
            hideTimer->start(
                1000,
                Qt::CoarseTimer,
                this
            );
        }
    );

    showTimer = new QBasicTimer();
    hideTimer = new QBasicTimer();
}


TaskButton::~TaskButton() {
}


QString TaskButton::appId() {
    return mAppId;
}


void TaskButton::setAppId( QString newAppId ) {
    mAppId = newAppId;
}


QList<int64_t> TaskButton::tasks() {
    return mToplevels;
}


void TaskButton::addTask( QJsonObject view ) {
    int64_t viewId = view[ "id" ].toInt();

    mToplevels << viewId;

    QString title     = view[ "title" ].toString();
    QString active    = ( view[ "activated" ].toBool() ? "true" : "false" );
    QString minimized = ( view[ "minimized" ].toBool() ? "true" : "false" );
    QString maximized = ( view[ "tiled-edges" ].toInt() > 1 ? "true" : "false" );
    mPopup->addEntry( viewId, { title, active, minimized, maximized } );
}


void TaskButton::removeTask( int64_t id ) {
    mToplevels.removeAll( id );
    mPopup->removeEntry( id );
}


void TaskButton::setActive( bool yes, QString title ) {
    mActive = yes;

    setChecked( yes );
    setText( yes ? title : "" );

    if ( yes ) {
        if ( (bool)tasksSett->value( "ExpandActive" ) ) {
            setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
            setFixedSize( QSize( (int)tasksSett->value( "MaximumWidth" ), height() ) );
        }

        else {
            setToolButtonStyle( Qt::ToolButtonIconOnly );
            setFixedSize( QSize( height(), height() ) );
        }
    }

    else {
        setChecked( false );

        if ( (bool)tasksSett->value( "ExpandActive" ) ) {
            setToolButtonStyle( Qt::ToolButtonIconOnly );
            setFixedSize( QSize( height(), height() ) );
        }
    }
}


void TaskButton::setTitle( int64_t id, QString title ) {
    mPopup->updateEntry( id, "title", title );
}


void TaskButton::setDemandsAttention( bool yes, QString ) {
    if ( yes ) {
        setStyleSheet( "border-color: orange;" );
    }

    else {
        setStyleSheet( QString() );
    }
}


void TaskButton::updateInfo() {
    // mPopup->clear();
    //
    // for ( uint id: mToplevels ) {
    //     mPopup->addEntry( id, "xxx" );
    // }
}


void TaskButton::enterEvent( QMouseEnterEvent *event ) {
    QToolButton::enterEvent( event );

    // if ( hideTimer->isActive() ) {
    //     hideTimer->stop();
    // }
    //
    // if ( (showTimer->isActive() == false) && (mPopup->isVisible() == false) ) {
    //     showTimer->start(
    //         500,
    //         Qt::CoarseTimer,
    //         this
    //     );
    // }
}


void TaskButton::leaveEvent( QEvent *event ) {
    QToolButton::leaveEvent( event );

    // if ( showTimer->isActive() ) {
    //     showTimer->stop();
    // }
    //
    // hideTimer->start(
    //     500,
    //     Qt::CoarseTimer,
    //     this
    // );
}


void TaskButton::timerEvent( QTimerEvent *event ) {
    if ( event->timerId() == showTimer->timerId() ) {
        /** Stop the timer */
        showTimer->stop();

        if ( this->underMouse() && ( mPopup->isVisible() == false ) ) {
            /** Emit the show signal */
            // emit showPopup( mPopup );
        }

        return;
    }

    else if ( event->timerId() == hideTimer->timerId() ) {
        /** Stop the timer */
        hideTimer->stop();

        if ( mPopup->underMouse() || this->underMouse() ) {
            hideTimer->start(
                500,
                Qt::CoarseTimer,
                this
            );
        }

        else {
            if ( mPopup->isVisible() ) {
                /** Emit the hide signal */
                emit hidePopup();
            }
        }

        return;
    }

    QToolButton::timerEvent( event );
}
