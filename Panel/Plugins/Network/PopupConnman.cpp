/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include "PopupConnman.hpp"
#include "Common/Widgets.hpp"

int rssiToPercentage( qint16 rssi ) {
    const int minRssi = -100; // Weakest signal
    const int maxRssi = -30;  // Strongest signal
    int       percentage;

    if ( rssi <= minRssi ) {
        percentage = 0;
    }

    else if ( rssi >= maxRssi ) {
        percentage = 100;
    }

    else {
        percentage = static_cast<int>( 100.0 * ( rssi - minRssi ) / ( maxRssi - minRssi ) );
    }

    return percentage;
}


ConnmanPopup::ConnmanPopup() : PluginPopupWidget( nullptr ) {
    setMinimumSize( QSize( 360, 450 ) );

    netMgr = new ConnmanQt( this );

    QVBoxLayout *mainLayout = new QVBoxLayout();

    // Adapter selection row
    QHBoxLayout *adapterLayout = new QHBoxLayout();
    adapterComboBox = new QComboBox( this );
    adapterComboBox->setMinimumWidth( 180 );

    for ( QVariantMap iface: netMgr->listInterfaces() ) {
        adapterComboBox->addItem( QIcon(), iface[ "Name" ].toString(), iface );
    }

    /** Adapter Power Button */
    adapterCheckBox = new DesQUI::Switch( this );

    // Scan button
    scanButton = new QToolButton( this );
    scanButton->setIcon( QIcon::fromTheme( "view-refresh" ) );

    adapterLayout->addWidget( adapterComboBox );
    adapterLayout->addStretch();
    adapterLayout->addWidget( adapterCheckBox );
    adapterLayout->addWidget( scanButton );

    mainLayout->addLayout( adapterLayout );
    // mainLayout->addWidget( Separator::horizontal( this ) );

    // Device list
    QVBoxLayout *devLyt = new QVBoxLayout();
    devLyt->setSpacing( 0 );
    devLyt->setContentsMargins( QMargins() );

    progress = new QProgressBar();
    progress->setFixedHeight( 3 );
    progress->setFormat( QString() );
    progress->setRange( 0, 100 );
    progress->hide();

    anim = new QVariantAnimation( progress );
    anim->setStartValue( 0 );
    anim->setKeyValueAt( 0.5, 100 );
    anim->setEndValue( 0 );
    anim->setLoopCount( -1 );

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ] ( QVariant val ) {
            progress->setValue( val.toInt() );
        }
    );

    deviceListView = new QListView( this );
    deviceListView->setIconSize( QSize( 32, 32 ) );
    devLyt->addWidget( progress );
    devLyt->addWidget( deviceListView );

    mainLayout->addLayout( devLyt );

    /** Layout to haol the various dynamic action buttons */
    QHBoxLayout *btnLyt = new QHBoxLayout();

    // Placeholder for dynamic buttons (Connect/Pair/Disconnect)
    trustButton = new QToolButton( this );
    trustButton->setDisabled( true );

    trustButton->setIcon( QIcon::fromTheme( "security-high" ) );
    btnLyt->addWidget( trustButton );
    btnLyt->addStretch();

    // Placeholder for dynamic buttons (Connect/Pair/Disconnect)
    actionButton = new QToolButton( this );
    actionButton->setDisabled( true );

    actionButton->setIcon( QIcon::fromTheme( "media-record" ) );
    btnLyt->addWidget( actionButton );

    disconnectButton = new QToolButton( this );
    disconnectButton->setDisabled( true );

    disconnectButton->setIcon( QIcon::fromTheme( "network-disconnect" ) );
    btnLyt->addWidget( disconnectButton );

    mainLayout->addLayout( btnLyt );

    setLayout( mainLayout );

    // Signal connections
    connect(
        adapterComboBox, &QComboBox::currentIndexChanged, [ = ] ( int ) {
            updateNetworks();
        }
    );

    updateNetworks();

    connect(
        scanButton, &QToolButton::clicked, [ = ] () {
            QVariantMap iface = adapterComboBox->currentData().toMap();
            QString objPath   = iface[ "Path" ].toString();

            if ( netMgr->scanInterface( objPath ) ) {
                qDebug() << "Scanning";
            }

            else {
                qDebug() << "Failed";
            }
        }
    );

    // connect( adapterCheckBox,  &QCheckBox::toggled,   this, &ConnmanPopup::toggleAdapter );
    // connect( deviceListView,   &QListView::clicked,   this, &ConnmanPopup::deviceSelected );
    // connect( trustButton,      &QToolButton::clicked, this, &ConnmanPopup::toggleTrust );
    // connect( actionButton,     &QToolButton::clicked, this, &ConnmanPopup::performAction );
    // connect( disconnectButton, &QToolButton::clicked, this, &ConnmanPopup::disconnectDevice );
}


void ConnmanPopup::updateNetworks() {
    QVariantMap iface = adapterComboBox->currentData().toMap();

    if ( iface[ "Powered" ].toBool() ) {
        adapterCheckBox->setChecked( true );
    }

    QString okType = iface[ "Type" ].toString();

    QStandardItemModel *model = new QStandardItemModel();
    deviceListView->setModel( model );

    QList<QVariantMap> networks = netMgr->listNetworks();
    for ( QVariantMap net: networks ) {
        if ( net[ "Type" ].toString() != okType ) {
            continue;
        }

        bool  connected = ( net[ "State" ] == "online" );
        int   strength  = net[ "Strength" ].toInt();
        QIcon signalIcon;

        if ( strength >= 90 ) {
            signalIcon = QIcon::fromTheme( "network-wireless-signal-excellent" );
        }
        else if ( strength >= 70 ) {
            signalIcon = QIcon::fromTheme( "network-wireless-signal-good" );
        }
        else if ( strength >= 50 ) {
            signalIcon = QIcon::fromTheme( "network-wireless-signal-ok" );
        }
        else if ( strength >= 20 ) {
            signalIcon = QIcon::fromTheme( "network-wireless-signal-weak" );
        }
        else {
            signalIcon = QIcon::fromTheme( "network-wireless-signal-none" );
        }

        QStandardItem *item = new QStandardItem( signalIcon, net[ "Name" ].toString() );
        item->setData( net, Qt::UserRole + 1 );

        QFont font = item->font();
        font.setBold( connected );
        item->setFont( font );

        model->appendRow( item );
    }
}


int main( int argc, char *argv[] ) {
    QApplication app( argc, argv );

    ConnmanPopup *popup = new ConnmanPopup();

    popup->show();

    return app.exec();
}
