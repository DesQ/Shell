#include "Window.h"

#include "AuthUi.h"

#include <QDBusConnection>
#include <QVBoxLayout>
#include <QComboBox>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QToolButton>
#include <QLabel>
#include <QPen>
#include <QPaintEvent>
#include <QPainter>


void Window::onDeviceAdded( const QString& stationId, const QString& name ) {
    mDeviceList->addItem( name, stationId );
}


void Window::onDeviceRemoved( const QString& stationId ) {
    for (int i = 0; i < mDeviceList->count(); i++) {
        if ( mDeviceList->itemData( i ) == stationId ) {
            mDeviceList->removeItem( i );
        }
    }
}


void Window::onDeviceStateChanged( const QString& stationId, const QString& state ) {
    if ( mDeviceList->currentData().toString() != stationId ) {
        return;
    }

    QString netId = mIWD.networkName( stationId );
    emit    stateChanged( netId, state );

    mDeviceStateLabel->setText( state );
}


void Window::onStationCurrentNetworkChanged( const QString& stationId, const QString& networkId ) {
    if ( mDeviceList->currentData().toString() != stationId ) {
        return;
    }

    m_currentNetworkId = networkId;
    qDebug() << "current network" << m_currentNetworkId;
}


void Window::onDisconnectDevice() {
    mIWD.disconnectStation( mDeviceList->currentData().toString() );
}


void Window::onConnectDevice() {
    QList<QListWidgetItem *> selected = mNetworkList->selectedItems();
    QString id;

    if ( !selected.isEmpty() ) {
        qDebug() << "no generic selected";
        id = selected.first()->data( Qt::UserRole ).toString();
    }

    if ( id.isEmpty() ) {
        qWarning() << "No network selected";
        return;
    }

    mIWD.connectNetwork( id );
}


void Window::onVisibleNetworkRemoved( const QString& stationId, const QString& name ) {
    qDebug() << "Visible network removed" << stationId << name;
    // ugly, but don't know a better way
    bool found = false;
    do {
        found = false;
        for (int i = 0; i < mNetworkList->count(); i++) {
            QListWidgetItem *net = mNetworkList->item( i );

            if ( net->data( Qt::UserRole ).toString() != stationId ) {
                continue;
            }

            delete mNetworkList->takeItem( i );
            found = true;
            break;
        }
    } while ( found );
    mNetworkList->sortItems();
}


void Window::onVisibleNetworkKnownChanged( const QString& networkId, const bool isKnown ) {
    for (int i = 0; i < mNetworkList->count(); i++) {
        QListWidgetItem *net = mNetworkList->item( i );

        if ( net->data( Qt::UserRole ).toString() != networkId ) {
            continue;
        }

        net->setData( Qt::UserRole + 2, isKnown );
        QFont font = net->font();
        font.setItalic( !isKnown );
        net->setFont( font );
        return;
    }
}


void Window::onNetworkConnectedChanged( const QString& networkId, const bool connected ) {
    for (int i = 0; i < mNetworkList->count(); i++) {
        QListWidgetItem *net = mNetworkList->item( i );

        if ( net->data( Qt::UserRole ).toString() != networkId ) {
            continue;
        }

        QFont font = net->font();
        font.setBold( connected );
        net->setFont( font );
        net->setData( Qt::UserRole + 3, connected );

        QString statusStr = net->text() + ( connected ? " connected": " disconnected" );

        return;
    }
}


void Window::onVisibleNetworkAdded( const QString& stationId, const QString& name, const bool connected, const bool isKnown ) {
    qDebug() << "Visible network added" << stationId << name;
    QListWidgetItem *item = new NetworkItem( QIcon::fromTheme( "network-wireless-symbolic" ), name );
    item->setData( Qt::UserRole,     stationId );
    item->setData( Qt::UserRole + 1, -1000 );
    item->setData( Qt::UserRole + 2, isKnown );
    item->setData( Qt::UserRole + 3, connected );

    QFont font = item->font();
    font.setItalic( !isKnown );
    font.setBold( connected );
    item->setFont( font );

    item->setFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
    mNetworkList->addItem( item );
    mNetworkList->sortItems();
}


void Window::onStationSignalChanged( const QString& stationId, int newLevel ) {
    float strength = ( 10000.0 + newLevel ) / 100.0;

    QIcon signalIcon;

    // Meh, should have better thresholds
    if ( strength >= 90 ) {
        signalIcon = QIcon::fromTheme( "network-wireless-signal-excellent" );
    }
    else if ( strength >= 70 ) {
        signalIcon = QIcon::fromTheme( "network-wireless-signal-good" );
    }
    else if ( strength >= 50 ) {
        signalIcon = QIcon::fromTheme( "network-wireless-signal-ok" );
    }
    else if ( strength >= 20 ) {
        signalIcon = QIcon::fromTheme( "network-wireless-signal-weak" );
    }
    else {
        signalIcon = QIcon::fromTheme( "network-wireless-signal-none" );
    }

    if ( stationId == m_currentNetworkId ) {
        qDebug() << "Updating tray";
        emit networkStrengthChanged( mIWD.networkName( stationId ), strength );
        emit iconChanged( signalIcon );
    }

    int found = 0;
    for (int i = 0; i < mNetworkList->count(); i++) {
        QListWidgetItem *net = mNetworkList->item( i );

        if ( net->data( Qt::UserRole ).toString() != stationId ) {
            continue;
        }

        found++;
        net->setIcon( signalIcon );
        net->setData( Qt::UserRole + 1, strength );
    }

    mNetworkList->sortItems();

    if ( !found ) {
        qDebug() << "Failed to find" << stationId;
    }
    else if ( found > 1 ) {
        qDebug() << "Found too many:" << stationId;
    }
}


void Window::onSelectionChanged() {
    m_connectButton->setEnabled( !mNetworkList->selectedItems().isEmpty() );
}


void Window::onNetworkDoubleClicked( QListWidgetItem *item ) {
    const QString networkId = item->data( Qt::UserRole ).toString();

    if ( networkId.isEmpty() ) {
        qWarning() << "no network in" << item;
        return;
    }

    mIWD.connectNetwork( networkId );
}


void Window::onDeviceSelectionChanged() {
    m_scanButton->setEnabled( !mIWD.isScanning( mDeviceList->currentData().toString() ) );
}


void Window::onScanningChanged( const QString& station, bool isScanning ) {
    if ( mDeviceList->currentData().toString() != station ) {
        return;
    }

    m_scanButton->setEnabled( !isScanning );
}


Window::Window( QWidget *parent ) : DesQ::Panel::PluginPopupWidget( parent ) {
    qRegisterMetaType<ManagedObject>( "ManagedObject" );
    qDBusRegisterMetaType<ManagedObject>();

    qRegisterMetaType<ManagedObjectList>( "ManagedObjectList" );
    qDBusRegisterMetaType<ManagedObjectList>();

    qRegisterMetaType<OrderedNetworkList>( "OrderedNetworkList" );
    qDBusRegisterMetaType<OrderedNetworkList>();
    qDBusRegisterMetaType<QPair<QDBusObjectPath, int16_t> >();

    qRegisterMetaType<LevelsList>( "LevelsList" );
    qDBusRegisterMetaType<LevelsList>();

    setWindowFlag( Qt::Dialog );
    QVBoxLayout *mainLayout = new QVBoxLayout;
    setLayout( mainLayout );

    mNetworkList = new QListWidget( this );

    QHBoxLayout *devicesLayout = new QHBoxLayout;
    mDeviceList       = new QComboBox;
    mDeviceStateLabel = new QLabel;
    QPushButton *disconnectButton = new QPushButton( tr( "Disconnect" ) );
    disconnectButton->setIcon( QIcon::fromTheme( "network-disconnect" ) );
    m_connectButton = new QPushButton( tr( "Connect" ) );
    m_connectButton->setEnabled( false );
    m_connectButton->setIcon( QIcon::fromTheme( "network-connect" ) );

    m_scanButton = new QToolButton();
    m_scanButton->setIcon( QIcon::fromTheme( "view-refresh" ) );
    m_scanButton->setToolTip( tr( "Scan for networks" ) );

    devicesLayout->addWidget( mDeviceList );
    devicesLayout->addWidget( mDeviceStateLabel );
    devicesLayout->addWidget( disconnectButton );
    devicesLayout->addStretch();
    devicesLayout->addWidget( m_connectButton );

    QHBoxLayout *networkLayout = new QHBoxLayout;
    networkLayout->addWidget( new QLabel( tr( "Available networks:" ) ) );
    networkLayout->addStretch();
    networkLayout->addWidget( m_scanButton );

    mainLayout->addLayout( devicesLayout );
    mainLayout->addLayout( networkLayout );
    mainLayout->addWidget( mNetworkList );

    connect( mNetworkList,     &QListWidget::itemSelectionChanged, this,  &Window::onSelectionChanged );
    connect( mDeviceList,      &QComboBox::currentTextChanged,     this,  &Window::onDeviceSelectionChanged );

    connect( mNetworkList,     &QListWidget::itemDoubleClicked,    this,  &Window::onNetworkDoubleClicked );

    connect( disconnectButton, &QPushButton::clicked,              this,  &Window::onDisconnectDevice );
    connect( m_scanButton,     &QPushButton::clicked,              &mIWD, &Iwd::scan );
    connect( m_connectButton,  &QPushButton::clicked,              this,  &Window::onConnectDevice );

    connect( &mIWD,            &Iwd::visibleNetworkAdded,          this,  &Window::onVisibleNetworkAdded );
    connect( &mIWD,            &Iwd::visibleNetworkRemoved,        this,  &Window::onVisibleNetworkRemoved );
    connect( &mIWD,            &Iwd::deviceAdded,                  this,  &Window::onDeviceAdded );
    connect( &mIWD,            &Iwd::deviceRemoved,                this,  &Window::onDeviceRemoved );
    connect( &mIWD,            &Iwd::signalLevelChanged,           this,  &Window::onStationSignalChanged );
    connect( &mIWD,            &Iwd::stationScanningChanged,       this,  &Window::onScanningChanged );
    connect( &mIWD,            &Iwd::stationStateChanged,          this,  &Window::onDeviceStateChanged );
    connect( &mIWD,            &Iwd::stationCurrentNetworkChanged, this,  &Window::onStationCurrentNetworkChanged );
    connect( &mIWD,            &Iwd::networkConnectedChanged,      this,  &Window::onNetworkConnectedChanged );
    connect( &mIWD,            &Iwd::visibleNetworkKnownChanged,   this,  &Window::onVisibleNetworkKnownChanged );

    m_signalAgent = new SignalLevelAgent( &mIWD );

    if ( QDBusConnection::systemBus().registerObject( m_signalAgent->objectPath().path(), this ) ) {
        mIWD.setSignalAgent( m_signalAgent->objectPath(), { -20, -40, -49, -50, -51, -60, -80 } );
        qDebug() << "set signal agent";
    }
    else {
        qWarning() << "Failed to register signal agent";
    }

    connect( m_signalAgent, &SignalLevelAgent::signalLevelChanged, this, &Window::onStationSignalChanged );

    m_authUi = new AuthUi( &mIWD );

    if ( QDBusConnection::systemBus().registerObject( m_authUi->objectPath().path(), this ) ) {
        mIWD.setAuthAgent( m_authUi->objectPath() );
    }
    else {
        qWarning() << "Failed to register auth agent";
    }

    setMinimumWidth( 400 );

    mIWD.init();
}
