/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QtDBus/QtDBus>
#include <QString>
#include <QVariant>
#include <QList>
#include <QMap>

typedef struct OVmType_t {
    QDBusObjectPath objPath;
    QVariantMap     properties;
} OVmType;


class ConnmanQt : public QObject {
    Q_OBJECT;

    public:
        explicit ConnmanQt( QObject *parent = nullptr );
        ~ConnmanQt();

        // List all known interfaces (e.g., eth0, wlan0, etc.)
        // Technically, we get only WiFi, Bluetooth, etc..
        QList<QVariantMap> listInterfaces();

        // List all known networks and available networks with their properties
        QList<QVariantMap> listNetworks();

        // Create a new network (wired, wireless, VPN, etc.)
        bool createNetwork( const QString& type, const QVariantMap& properties );

        // Delete a known network
        bool deleteNetwork( const QString& networkPath );

        // Connect to a given network (with optional authentication)
        bool connectToNetwork( const QString& networkPath, const QVariantMap& credentials );

        // Enable or disable a given network
        bool setNetworkEnabled( const QString& networkPath, bool enabled );

        // Set a given network to auto-connect
        bool setAutoConnect( const QString& networkPath, bool autoConnect );

        // Scan an interface for networks
        bool scanInterface( const QString& interfacePath );

        // Create a wireless hotspot
        bool createHotspot( const QVariantMap& hotspotProperties );

        // Delete a wireless hotspot
        bool deleteHotspot( const QString& hotspotPath );

    signals:
        // Signals for network events
        void networkChanged( const QString& networkPath, int strength );
        void networkAvailabilityChanged( const QString& networkPath, bool available );

        /** Network removed */
        void networkRemoved( const QString& networkPath );

        /** Newly added interface (WiFi, Bluetooth, etc) */
        void interfaceAdded( const QString& interfaceName );

        /** Removed existing interface (WiFi, Bluetooth, etc) */
        void interfaceRemoved( const QString& interfaceName );

    private:
        // Helper to emit interfaces added/removed signals
        Q_SLOT void onTechnologiesAdded( const QDBusObjectPath& objPath, const QVariantMap& properties );
        Q_SLOT void onTechnologiesRemoved( const QDBusObjectPath& objPath );

        // Helper to handle network property changes
        Q_SLOT void onPropertyChanged( const QString&, const QDBusVariant& );

        // Helper to handle service property changes
        Q_SLOT void onServicesChanged( const QList<OVmType>&, const QList<QDBusObjectPath>& );

        // Helper to query a service's properties
        QVariantMap getServiceProperties( const QString& servicePath );

        // Setup signal listeners for ConnMan
        void setupSignalListeners();

        // DBus interface
        QDBusInterface *connmanManager; // ConnMan Manager Interface
};

Q_DECLARE_METATYPE( OVmType );
Q_DECLARE_METATYPE( QList<OVmType> );
