class NetworkManagerQt : public QObject {
    Q_OBJECT;

    public:
        // Existing code...

        // New Authentication Enum
        enum AuthenticationType {
            None,
            WPA_PSK,
            WPA_EAP,
            IEEE8021X,
            PEAP,
            TLS,
            TTLS
        };

        // Enhanced Network Connection Structure
        struct NetworkConnectionDetails {
            QString            ssid;
            QString            password;
            AuthenticationType authType;
            QString            username;        // For enterprise networks
            QString            certificatePath; // For TLS/PEAP networks
        };

        // Wireless Hotspot Management
        bool createWirelessHotspot( const QString& ssid, const QString& password, const QString& interfaceName, bool hidden = false
        );

        bool deleteWirelessHotspot( const QString& ssid );

        // Network Deletion
        bool deleteKnownNetwork( const QString& networkUuid );
        bool deleteAllKnownNetworks();

        // Advanced Network Connection
        bool connectToNetwork( const NetworkConnectionDetails& connectionDetails, const QString& interfaceName = QString()
        );

        // Disconnect from current network
        bool disconnectNetwork( const QString& networkUuid );

    private:
        // Helper methods for advanced connection
        QVariantMap prepareNetworkConnectionConfig( const NetworkConnectionDetails& details
        );

        bool authenticateNetwork( const NetworkConnectionDetails& details
        );
};
