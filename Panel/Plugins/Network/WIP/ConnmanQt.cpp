/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "WIP/ConnmanQt.hpp"
#include <QDebug>
#include <private/qdbusutil_p.h>

// ConnMan D-Bus paths and interfaces
static const QString CONNMAN_SERVICE      = "net.connman";
static const QString CONNMAN_MANAGER_PATH = "/";

// Marshall the MyStructure data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const OVmType& myStruct ) {
    argument.beginStructure();
    argument << myStruct.objPath << myStruct.properties;
    argument.endStructure();
    return argument;
}


// Retrieve the MyStructure data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, OVmType& myStruct ) {
    argument.beginStructure();
    argument >> myStruct.objPath >> myStruct.properties;
    argument.endStructure();
    return argument;
}


ConnmanQt::ConnmanQt( QObject *parent ) : QObject( parent ) {
    connmanManager = new QDBusInterface(
        CONNMAN_SERVICE,
        CONNMAN_MANAGER_PATH,
        "net.connman.Manager",
        QDBusConnection::systemBus(),
        this
    );

    qDBusRegisterMetaType<OVmType>();
    qDBusRegisterMetaType<QList<OVmType> >();

    if ( !connmanManager->isValid() ) {
        qWarning() << "Failed to connect to ConnMan Manager on D-Bus";
    }

    setupSignalListeners();
}


ConnmanQt::~ConnmanQt() {
    delete connmanManager;
}


QList<QVariantMap> ConnmanQt::listInterfaces() {
    QList<QVariantMap> interfaces;

    if ( !connmanManager->isValid() ) {
        return interfaces;
    }

    QDBusMessage  reply = connmanManager->call( "GetTechnologies" );
    QDBusArgument dArg  = qvariant_cast<QDBusArgument>( reply.arguments().first() );
    dArg.beginArray();
    while ( !dArg.atEnd() ) {
        OVmType value;
        dArg >> value;

        /** Add the object path to the properties */
        value.properties[ "Path" ] = value.objPath.path();
        interfaces << value.properties;
    }
    dArg.endArray();

    return interfaces;
}


QList<QVariantMap> ConnmanQt::listNetworks() {
    QList<QVariantMap> networks;

    if ( !connmanManager->isValid() ) {
        return networks;
    }

    QDBusMessage  reply = connmanManager->call( "GetServices" );
    QDBusArgument dArg  = qvariant_cast<QDBusArgument>( reply.arguments().first() );
    dArg.beginArray();
    while ( !dArg.atEnd() ) {
        OVmType value;
        dArg >> value;

        /** Add the object path to the properties */
        value.properties[ "Path" ] = value.objPath.path();
        networks << value.properties;
    }
    dArg.endArray();

    return networks;
}


QVariantMap ConnmanQt::getServiceProperties( const QString& servicePath ) {
    QDBusInterface serviceIface(
        CONNMAN_SERVICE,
        servicePath,
        "net.connman.Service",
        QDBusConnection::systemBus()
    );

    QDBusReply<QVariantMap> reply = serviceIface.call( "GetProperties" );

    if ( reply.isValid() ) {
        return reply.value();
    }
    else {
        qWarning() << "Error fetching service properties:" << reply.error().message();
        return QVariantMap();
    }
}


bool ConnmanQt::createNetwork( const QString& type, const QVariantMap& properties ) {
    if ( !connmanManager->isValid() ) {
        return false;
    }

    QDBusReply<void> reply = connmanManager->call( "CreateService", type, properties );

    return reply.isValid();
}


bool ConnmanQt::deleteNetwork( const QString& networkPath ) {
    QDBusInterface serviceIface(
        CONNMAN_SERVICE,
        networkPath,
        "net.connman.Service",
        QDBusConnection::systemBus()
    );

    QDBusReply<void> reply = serviceIface.call( "Remove" );

    return reply.isValid();
}


bool ConnmanQt::connectToNetwork( const QString& networkPath, const QVariantMap& credentials ) {
    QDBusInterface serviceIface(
        CONNMAN_SERVICE,
        networkPath,
        "net.connman.Service",
        QDBusConnection::systemBus()
    );

    QDBusReply<void> reply = serviceIface.call( "Connect" );

    if ( !reply.isValid() ) {
        qWarning() << "Failed to connect:" << reply.error().message();

        return false;
    }

    // Set credentials if required
    for (auto it = credentials.begin(); it != credentials.end(); ++it) {
        serviceIface.call( "SetProperty", it.key(), it.value() );
    }

    return true;
}


bool ConnmanQt::setNetworkEnabled( const QString& networkPath, bool enabled ) {
    QDBusInterface serviceIface(
        CONNMAN_SERVICE,
        networkPath,
        "net.connman.Service",
        QDBusConnection::systemBus()
    );

    QDBusReply<void> reply = serviceIface.call( enabled ? "Connect" : "Disconnect" );

    return reply.isValid();
}


bool ConnmanQt::setAutoConnect( const QString& networkPath, bool autoConnect ) {
    QDBusInterface serviceIface(
        CONNMAN_SERVICE,
        networkPath,
        "net.connman.Service",
        QDBusConnection::systemBus()
    );

    QDBusReply<void> reply = serviceIface.call( "SetProperty", "AutoConnect", QVariant::fromValue( autoConnect ) );

    return reply.isValid();
}


bool ConnmanQt::scanInterface( const QString& interfacePath ) {
    QDBusInterface techIface(
        CONNMAN_SERVICE,
        interfacePath,
        "net.connman.Technology",
        QDBusConnection::systemBus()
    );

    QDBusReply<void> reply = techIface.asyncCall( "Scan" );

    return reply.isValid();
}


bool ConnmanQt::createHotspot( const QVariantMap& hotspotProperties ) {
    return createNetwork( "wifi", hotspotProperties );
}


bool ConnmanQt::deleteHotspot( const QString& hotspotPath ) {
    return deleteNetwork( hotspotPath );
}


void ConnmanQt::setupSignalListeners() {
    QDBusConnection::systemBus().connect(
        CONNMAN_SERVICE,
        "/",
        "net.connman.Manager",
        "TechnologyAdded",
        this,
        SLOT( onTechnologiesAdded( const QDBusObjectPath&, const QVariantMap& ) )
    );

    QDBusConnection::systemBus().connect(
        CONNMAN_SERVICE,
        "/",
        "net.connman.Manager",
        "TechnologyRemoved",
        this,
        SLOT( onTechnologiesRemoved( const QDBusObjectPath& ) )
    );

    QDBusConnection::systemBus().connect(
        CONNMAN_SERVICE,
        "/",
        "net.connman.Manager",
        "PropertyChanged",
        this,
        SLOT( onPropertyChanged( const QString&, const QDBusVariant& ) )
    );

    QDBusConnection::systemBus().connect(
        CONNMAN_SERVICE,
        "/",
        "net.connman.Manager",
        "ServicesChanged",
        this,
        SLOT( onServicesChanged( const QList<OVmType>&, const QList<QDBusObjectPath>& ) )
    );
}


void ConnmanQt::onTechnologiesAdded( const QDBusObjectPath& objPath, const QVariantMap& properties ) {
    qDebug() << "Props" << properties.keys();
    // for (const QVariant& interface : added) {
    //     QString interfaceName = interface.value<QString>();
    //     emit    interfaceAdded( interfaceName );
    // }

    // for (const QVariant& interface : removed) {
    //     QString interfaceName = interface.value<QString>();
    //     emit    interfaceRemoved( interfaceName );
    // }
}


void ConnmanQt::onTechnologiesRemoved( const QDBusObjectPath& objPath ) {
    qDebug() << "ObjPath:" << objPath;
    // for (const QVariant& interface : added) {
    //     QString interfaceName = interface.value<QString>();
    //     emit    interfaceAdded( interfaceName );
    // }

    // for (const QVariant& interface : removed) {
    //     QString interfaceName = interface.value<QString>();
    //     emit    interfaceRemoved( interfaceName );
    // }
}


void ConnmanQt::onPropertyChanged( const QString& property, const QDBusVariant& value ) {
    qDebug() << "Property changed:" << property << value.variant();
}


void ConnmanQt::onServicesChanged( const QList<OVmType>& changed, const QList<QDBusObjectPath>& removed ) {
    // for (const OVmType& service : added) {
    //     emit networkAdded( service.value<QString>() );
    // }

    for (const QDBusObjectPath& path : removed) {
        emit networkRemoved( path.path() );
    }
}
