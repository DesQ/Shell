/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <DFPowerManager.hpp>

#include "Network.hpp"
#include "Window.h"

NetworkPluginWidget::NetworkPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    network = new QLabel();
    network->setAlignment( Qt::AlignCenter );
    network->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );
    network->setPixmap( QIcon::fromTheme( "network-offline" ).pixmap( panelHeight - 8 ) );

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( network );
    setLayout( lyt );

    mTooltip = new QLabel( "Disconnected" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );

    mPopup = new Window();
    mPopup->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    mPopup->setAttribute( Qt::WA_TranslucentBackground );

    connect( mPopup, &Window::hidePopup, this, &NetworkPluginWidget::hidePopup );
    // connect( mPopup, &ConnmanPopup::hidePopup, this, &NetworkPluginWidget::hidePopup );

    connect(
        mPopup, &Window::iconChanged, [ = ]( QIcon icon ) {
            network->setPixmap( icon.pixmap( panelHeight - 8 ) );
        }
    );

    connect(
        mPopup, &Window::networkStrengthChanged, [ = ]( QString netId, qreal strength ) {
            mTooltip->setText( QString( "%1 (%2 %)" ).arg( netId ).arg( strength ) );
        }
    );

    connect(
        mPopup, &Window::stateChanged, [ = ]( QString netId, QString state ) {
            if ( state == "connected" ) {
                /** Once */
                return;
            }

            else if ( state == "connecting" ) {
                mTooltip->setText( QString( "%1 (connecting)" ).arg( netId ) );
            }

            else if ( state == "disconnected" ) {
                network->setPixmap( QIcon::fromTheme( "network-offline" ).pixmap( panelHeight - 8 ) );
                mTooltip->setText( "Disconncted" );
            }
        }
    );

    connect(
        mPopup, &Window::networkConnectedChanged, [ = ]( QString msg ) {
            if ( msg.endsWith( " connected" ) ) {
                mTooltip->setText( msg.split( " " ).at( 0 ) );
            }

            else {
                mTooltip->setText( "Disconnected" );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            if ( mPopup && mPopup->isVisible() ) {
                emit hidePopup();
            }

            else {
                if ( mTooltip && mTooltip->isVisible() ) {
                    emit hideTooltip();
                }

                emit showPopup( mPopup );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip && mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );
}


NetworkPluginWidget::~NetworkPluginWidget() {
    if ( mTooltip ) {
        mTooltip->close();
    }

    if ( mPopup ) {
        mPopup->close();
    }

    delete mPopup;
    delete mTooltip;
}


void NetworkPluginWidget::notifyUser( QString title, QString message, short weight ) {
    QString urgency;

    switch ( weight ) {
        case 0: {
            urgency = "low";
            break;
        }

        case 1: {
            urgency = "normal";
            break;
        }

        default: {
            urgency = "critical";
            break;
        }
    }

    QProcess::startDetached(
        "desq-notifier", {
            "-i", "desq", "-a", "desq-power-manager",
            "-t", "5000", "-u", urgency, "-r", mID,
            title, message
        }
    );
}
