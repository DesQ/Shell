#pragma once

#include <QWidget>
#include <QPointer>
#include <QListWidgetItem>

#include <desqui/PanelPlugin.hpp>

#include "Iwd.h"

class QListWidget;
class QComboBox;
class QPushButton;
class QToolButton;
class QLabel;
class AuthUi;
class SignalLevelAgent;
class QSystemTrayIcon;

struct NetworkItem : public QListWidgetItem {
    using QListWidgetItem::QListWidgetItem;
    bool operator<( const QListWidgetItem& other ) const override {
        const bool connected      = data( Qt::UserRole + 3 ).toBool();
        const bool otherConnected = other.data( Qt::UserRole + 3 ).toBool();

        if ( connected != otherConnected ) {
            return connected > otherConnected;
        }

        const bool isKnown    = data( Qt::UserRole + 2 ).toBool();
        const bool otherKnown = other.data( Qt::UserRole + 2 ).toBool();

        if ( isKnown != otherKnown ) {
            return isKnown > otherKnown;
        }

        return data( Qt::UserRole + 1 ).toInt() < other.data( Qt::UserRole + 1 ).toInt();
    }
};

class Window : public DesQ::Panel::PluginPopupWidget {
    Q_OBJECT;

    public:
        explicit Window( QWidget *parent = nullptr );

    private slots:
        void onNetworkConnectedChanged( const QString& networkId, const bool connected );

        void onDeviceAdded( const QString& stationId, const QString& name );
        void onDeviceRemoved( const QString& stationId );
        void onDisconnectDevice();
        void onConnectDevice();
        void onDeviceStateChanged( const QString& deviceId, const QString& state );

        void onVisibleNetworkRemoved( const QString& stationId, const QString& name );
        void onVisibleNetworkAdded( const QString& stationId, const QString& name, const bool connected, const bool isKnown );
        void onVisibleNetworkKnownChanged( const QString& id, const bool isKnown );

        void onStationSignalChanged( const QString& stationId, int newLevel );

        void onSelectionChanged();

        void onDeviceSelectionChanged();
        void onNetworkDoubleClicked( QListWidgetItem *item );
        void onScanningChanged( const QString& station, bool isScanning );

        void onStationCurrentNetworkChanged( const QString& stationId, const QString& networkId );

    private:
        QListWidget *mNetworkList = nullptr;
        QComboBox *mDeviceList    = nullptr;
        QLabel *mDeviceStateLabel = nullptr;

        Iwd mIWD;
        QPointer<AuthUi> m_authUi;
        QPointer<SignalLevelAgent> m_signalAgent;
        QPushButton *m_connectButton = nullptr;
        QToolButton *m_scanButton    = nullptr;

        QString m_currentNetworkId;

    Q_SIGNALS:
        void iconChanged( QIcon );
        void networkStrengthChanged( QString, qreal );
        void networkConnectedChanged( QString );
        void stateChanged( QString, QString );
};
