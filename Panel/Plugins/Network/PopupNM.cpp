/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <BluezQt/PendingCall>

#include "Popup.hpp"
#include "Common/Widgets.hpp"

int rssiToPercentage( qint16 rssi ) {
    const int minRssi = -100; // Weakest signal
    const int maxRssi = -30;  // Strongest signal
    int       percentage;

    if ( rssi <= minRssi ) {
        percentage = 0;
    }

    else if ( rssi >= maxRssi ) {
        percentage = 100;
    }

    else {
        percentage = static_cast<int>( 100.0 * ( rssi - minRssi ) / ( maxRssi - minRssi ) );
    }

    return percentage;
}


ConnmanPopup::ConnmanPopup() : PluginPopupWidget( nullptr ) {
    setMinimumSize( QSize( 360, 450 ) );

    manager = new BluezQt::Manager( this );

    QVBoxLayout *mainLayout = new QVBoxLayout();

    // Adapter selection row
    QHBoxLayout *adapterLayout = new QHBoxLayout();
    adapterComboBox = new QComboBox( this );
    adapterComboBox->setMinimumWidth( 180 );

    /** Adapter Power Button */
    adapterCheckBox = new DesQUI::Switch( this );

    // Scan button
    scanButton = new QToolButton( this );
    scanButton->setIcon( QIcon::fromTheme( "view-refresh" ) );

    adapterLayout->addWidget( adapterComboBox );
    adapterLayout->addStretch();
    adapterLayout->addWidget( adapterCheckBox );
    adapterLayout->addWidget( scanButton );

    mainLayout->addLayout( adapterLayout );
    mainLayout->addWidget( Separator::horizontal( this ) );

    // Device list
    QVBoxLayout *devLyt = new QVBoxLayout();
    devLyt->setSpacing( 0 );
    devLyt->setContentsMargins( QMargins() );

    progress = new QProgressBar();
    progress->setFixedHeight( 3 );
    progress->setFormat( QString() );
    progress->setRange( 0, 100 );
    progress->hide();

    anim = new QVariantAnimation( progress );
    anim->setStartValue( 0 );
    anim->setKeyValueAt( 0.5, 100 );
    anim->setEndValue( 0 );
    anim->setLoopCount( -1 );

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ] ( QVariant val ) {
            progress->setValue( val.toInt() );
        }
    );

    deviceListView = new QListView( this );
    deviceListView->setIconSize( QSize( 32, 32 ) );
    devLyt->addWidget( progress );
    devLyt->addWidget( deviceListView );

    mainLayout->addLayout( devLyt );

    /** Layout to haol the various dynamic action buttons */
    QHBoxLayout *btnLyt = new QHBoxLayout();

    // Placeholder for dynamic buttons (Connect/Pair/Disconnect)
    trustButton = new QToolButton( this );
    trustButton->setDisabled( true );

    trustButton->setIcon( QIcon::fromTheme( "security-high" ) );
    btnLyt->addWidget( trustButton );
    btnLyt->addStretch();

    // Placeholder for dynamic buttons (Connect/Pair/Disconnect)
    actionButton = new QToolButton( this );
    actionButton->setDisabled( true );

    actionButton->setIcon( QIcon::fromTheme( "media-record" ) );
    btnLyt->addWidget( actionButton );

    disconnectButton = new QToolButton( this );
    disconnectButton->setDisabled( true );

    disconnectButton->setIcon( QIcon::fromTheme( "network-disconnect" ) );
    btnLyt->addWidget( disconnectButton );

    mainLayout->addLayout( btnLyt );

    setLayout( mainLayout );

    // Initialize BluezQt
    auto initJob = manager->init();
    connect( initJob, &BluezQt::InitManagerJob::result, this, &ConnmanPopup::onManagerInitialized );
    initJob->start();

    // Signal connections
    connect( scanButton,       &QPushButton::clicked, this, &ConnmanPopup::scanForDevices );
    connect( adapterCheckBox,  &QCheckBox::toggled,   this, &ConnmanPopup::toggleAdapter );
    connect( deviceListView,   &QListView::clicked,   this, &ConnmanPopup::deviceSelected );
    connect( trustButton,      &QToolButton::clicked, this, &ConnmanPopup::toggleTrust );
    connect( actionButton,     &QToolButton::clicked, this, &ConnmanPopup::performAction );
    connect( disconnectButton, &QToolButton::clicked, this, &ConnmanPopup::disconnectDevice );
}


void ConnmanPopup::onManagerInitialized( BluezQt::InitManagerJob *job ) {
    if ( job->error() ) {
        QMessageBox::critical( this, "Error", "Failed to initialize BluezQt Manager" );
        return;
    }

    const auto adapters = manager->adapters();
    for (const auto& adapter : adapters) {
        adapterComboBox->addItem( adapter->name(), QVariant::fromValue( adapter ) );
    }

    if ( !adapters.isEmpty() ) {
        currentAdapter = adapters.first();
        adapterCheckBox->setChecked( currentAdapter->isPowered() );
        connect( adapterComboBox, &QComboBox::currentIndexChanged, this, &ConnmanPopup::adapterChanged );
        updateKnownDevices();

        /** Start discovery */
        scanForDevices();
    }
}


void ConnmanPopup::scanForDevices() {
    if ( !currentAdapter ) {
        qDebug() << "No currentAdapter available.";
        return;
    }

    scanButton->setEnabled( false );
    progress->show();
    anim->start();

    // Start the discovery process
    BluezQt::PendingCall *pending = currentAdapter->startDiscovery();
    connect(
        pending, &BluezQt::PendingCall::finished, this, [ = ] ( BluezQt::PendingCall *call ) {
            if ( call->error() ) {
                anim->stop();
                progress->hide();
                scanButton->setEnabled( true );

                return;
            }

            if ( currentAdapter->discoverableTimeout() ) {
                QTimer::singleShot(
                    currentAdapter->discoverableTimeout() * 1000, [ = ] () {
                        anim->stop();
                        progress->hide();
                        scanButton->setEnabled( true );
                        qDebug() << "Scan complete.";
                    }
                );
            }

            else {
                anim->stop();
                progress->hide();
                scanButton->setEnabled( false );
            }
        }
    );

    connect( currentAdapter.get(), &BluezQt::Adapter::deviceAdded, this, &ConnmanPopup::deviceFound );
}


void ConnmanPopup::deviceFound( BluezQt::DevicePtr device ) {
    qDebug() << device->friendlyName() << ( device->isConnected() ? "Connected" : "Disconnected" ) << ( device->isTrusted() ? "Trusted" : "Not trusted" );
    qDebug() << "    " << device->address();

    if ( availableDevices.contains( device ) == false ) {
        prepareDeviceConnections( device );

        availableDevices.append( device );
        updateDeviceList();
    }
}


void ConnmanPopup::updateDeviceList() {
    auto model = new QStandardItemModel( this );

    for (const auto& device : knownDevices) {
        QString deviceName = device->name();

        // Determine the appropriate icon based on device type
        QIcon deviceIcon = QIcon::fromTheme( device->icon() );

        // Create a QStandardItem and set its text and icon
        auto item = new QStandardItem( deviceIcon, deviceName );
        item->setData( QVariant::fromValue( device ), Qt::UserRole + 1 );

        /** Is device available */
        if ( rssiToPercentage( device->rssi() ) ) {
            item->setEnabled( true );
        }

        else {
            item->setEnabled( false );
        }

        model->appendRow( item );
    }

    for ( const auto& device : availableDevices ) {
        /** We've already added it above */
        if ( knownDevices.contains( device ) ) {
            continue;
        }

        QString deviceName = device->name();

        // Determine the appropriate icon based on device type
        QIcon deviceIcon = QIcon::fromTheme( device->icon() );

        // Create a QStandardItem and set its text and icon
        auto item = new QStandardItem( deviceIcon, deviceName );
        item->setData( QVariant::fromValue( device ), Qt::UserRole + 1 );

        model->appendRow( item );
    }

    deviceListView->setModel( model );
}


void ConnmanPopup::adapterChanged( int index ) {
    currentAdapter = adapterComboBox->itemData( index ).value<BluezQt::AdapterPtr>();
    adapterCheckBox->setChecked( currentAdapter->isPowered() );

    if ( currentAdapter->isPowered() ) {
        scanButton->setEnabled( true );
        deviceListView->setEnabled( true );
    }

    else {
        scanButton->setDisabled( true );
        deviceListView->setDisabled( true );
    }

    updateKnownDevices();
}


void ConnmanPopup::toggleAdapter( bool enabled ) {
    if ( !currentAdapter ) {
        return;
    }

    currentAdapter->setPowered( enabled );

    if ( enabled ) {
        scanButton->setEnabled( true );
        deviceListView->setEnabled( true );
    }

    else {
        scanButton->setDisabled( true );
        deviceListView->setDisabled( true );
    }
}


void ConnmanPopup::deviceSelected( const QModelIndex& index ) {
    if ( !index.isValid() ) {
        return;
    }

    auto device = index.data( Qt::UserRole + 1 ).value<BluezQt::DevicePtr>();
    selectedDevice = device;

    /** Check if the device is "Available" */
    if ( rssiToPercentage( device->rssi() ) ) {
        trustButton->setEnabled( true );
        actionButton->setEnabled( true );
        disconnectButton->setEnabled( true );
    }

    else {
        trustButton->setDisabled( true );
        actionButton->setDisabled( true );
        disconnectButton->setDisabled( true );

        return;
    }

    if ( device->isTrusted() ) {
        trustButton->setIcon( QIcon::fromTheme( "security-low" ) );
        trustButton->setToolTip( "Untrust device" );
    }

    else {
        trustButton->setIcon( QIcon::fromTheme( "security-high" ) );
        trustButton->setToolTip( "Trust device" );
    }

    if ( device->isConnected() ) {
        disconnectButton->setEnabled( true );
        actionButton->setDisabled( true );
    }

    else {
        disconnectButton->setDisabled( true );

        if ( device->isPaired() ) {
            actionButton->setIcon( QIcon::fromTheme( "network-connect" ) );
        }

        else {
            actionButton->setIcon( QIcon::fromTheme( "" ) );
        }

        actionButton->setEnabled( true );
    }
}


void ConnmanPopup::toggleTrust() {
    if ( !selectedDevice ) {
        return;
    }

    if ( selectedDevice->isTrusted() ) {
        selectedDevice->setTrusted( false );
        trustButton->setIcon( QIcon::fromTheme( "security-high" ) );
    }

    else {
        selectedDevice->setTrusted( true );
        trustButton->setIcon( QIcon::fromTheme( "security-low" ) );
    }
}


void ConnmanPopup::performAction() {
    if ( !selectedDevice ) {
        return;
    }

    if ( selectedDevice->isPaired() ) {
        selectedDevice->connectToDevice();
    }

    else {
        selectedDevice->pair();
    }
}


void ConnmanPopup::disconnectDevice() {
    if ( !selectedDevice || !selectedDevice->isConnected() ) {
        return;
    }

    selectedDevice->disconnectFromDevice();
    disconnectButton->setDisabled( true );
}


void ConnmanPopup::updateKnownDevices() {
    if ( !currentAdapter ) {
        return;
    }

    knownDevices.clear();
    for (const auto& device : currentAdapter->devices() ) {
        prepareDeviceConnections( device );

        if ( device->isTrusted() ) {
            knownDevices.append( device );
        }
    }

    updateDeviceList();
}


void ConnmanPopup::prepareDeviceConnections( BluezQt::DevicePtr dev ) {
    connect(
        dev.get(), &BluezQt::Device::deviceChanged, [ = ] ( BluezQt::DevicePtr dev ) {
            updateDeviceList();
        }
    );

    connect(
        dev.get(), &BluezQt::Device::deviceRemoved, [ = ] ( BluezQt::DevicePtr dev ) {
            if ( availableDevices.contains( dev ) ) {
                availableDevices.removeAll( dev );
            }

            updateDeviceList();
        }
    );

    connect(
        dev.get(), &BluezQt::Device::connectedChanged, [ = ] ( bool yes ) {
            QString info = QString( "Device %1connected." ).arg( yes ? "" : "dis" );

            if ( yes ) {
                info += QString( "<br>Signal Strength: %1%" ).arg( rssiToPercentage( dev->rssi() ) );
                info += QString( "<br>Battery percentage: N/A" );
            }

            QProcess::startDetached(
                "desq-notifier", {
                    "-i", "desq-bluetooth", "-a", "desq-bluez",
                    "-t", "5000", "-u normal", "-r", mID,
                    dev->friendlyName(), info
                }
            );
        }
    );
}
