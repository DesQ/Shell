/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

const QMap<int, QStringList> WeatherInfoMap = {
    {
        0,{
            "Clear sky",
            "\uf00d",
            "\uf02e",
        }
    },
    {
        1,{
            "Mainly Clear Sky",
            "\uf00c",
            "\uf083",
        }
    },
    {
        2,{
            "Partly Cloudy",
            "\uf00c",
            "\uf083",
        }
    },
    {
        3,{
            "Overcast Sky",
            "\uf002",
            "\uf086",
        }
    },
    {
        45,{
            "Fog",
            "\uf003",
            "\uf04a",
        }
    },
    {
        48,{
            "Rime",
            "\uf003",
            "\uf04a",
        }
    },
    {
        51,{
            "Light Drizzle",
            "\uf00b",
            "\uf02b",
        }
    },
    {
        53,{
            "Medium Drizzle",
            "\uf00b",
            "\uf02b",
        }
    },
    {
        55,{
            "Heavy Drizzle",
            "\uf008",
            "\uf028",
        }
    },
    {
        56,{
            "Freezing Drizzle",
            "\uf006",
            "\uf026",
        }
    },
    {
        57,{
            "Freezing Drizzle",
            "\uf006",
            "\uf026",
        }
    },
    {
        61,{
            "Light rain",
            "\uf008",
            "\uf028",
        }
    },
    {
        63,{
            "Rain",
            "\uf008",
            "\uf028",
        }
    },
    {
        65,{
            "Heavy rain",
            "\uf008",
            "\uf028",
        }
    },
    {
        66,{
            "Freezing rain",
            "\uf006",
            "\uf026",
        }
    },
    {
        67,{
            "Heavy Freezing Rain",
            "\uf006",
            "\uf026",
        }
    },
    {
        71,{
            "Light Snow Fall",
            "\uf00a",
            "\uf02a",
        }
    },
    {
        73,{
            "Snow Fall",
            "\uf00a",
            "\uf02a",
        }
    },
    {
        75,{
            "Heavy Snow Fall",
            "\uf0b2",
            "\uf0b3",
        }
    },
    {
        77,{
            "Grainy Snow Fall",
            "\uf0b2",
            "\uf0b3",
        }
    },
    {
        80,{
            "Light Rain Showers",
            "\uf009",
            "\uf029",
        }
    },
    {
        81,{
            "Rain Showers",
            "\uf009",
            "\uf029",
        }
    },
    {
        82,{
            "Heavy Rain Showers",
            "\uf008",
            "\uf028",
        }
    },
    {
        85,{
            "Light Snow Showers",
            "\uf065",
            "\uf067",
        }
    },
    {
        86,{
            "Heavy Snow Showers",
            "\uf065",
            "\uf067",
        }
    },
    {
        95,{
            "Thunderstorm",
            "\uf00e",
            "\uf02c",
        }
    },
    {
        96,{
            "Thunderstorm",
            "\uf068",
            "\uf06a",
        }
    },
    {
        99,{
            "Heavy Thunderstorm",
            "\uf010",
            "\uf02d",
        }
    },
};
