/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/


/**
 * MoonRise Calculator: https://github.com/signetica/MoonRise
 **/

#include <curl/curl.h>

#include <QtWidgets>
#include <desqui/PanelPlugin.hpp>
#include <DFHjsonParser.hpp>

#include "WeatherWidget.hpp"
#include "WeatherData.hpp"

const QString GeoUrl        = "https://geocoding-api.open-meteo.com/v1/search?name=%1&count=100&language=en&format=json";
const QString WeatherNowUrl = "https://api.open-meteo.com/v1/forecast?latitude=%1&longitude=%2&timezone=%3&current_weather=true";

class WeatherTask : public QObject, public QRunnable {
    Q_OBJECT;

    public:
        WeatherTask() {
            mUrl = QString();
            setAutoDelete( false );
        }

        void setUrl( QString url ) {
            mUrl = url;
        }

        void run() {
            if ( mUrl.length() == 0 ) {
                emit weather( QString() );
                return;
            }

            CURL *handle = curl_easy_init();

            QString response;

            QSettings network( "DesQ", "Network" );

            if ( network.value( "ProxyType" ).toInt() == 4 ) {
                curl_easy_setopt( handle, CURLOPT_PROXY, network.value( "HTTPProxy" ).toString().toUtf8().data() );
            }

            /** Forcibly set Proxy to "", so that it does not read from env */
            else if ( network.value( "ProxyType" ).toInt() == 0 ) {
                curl_easy_setopt( handle, CURLOPT_PROXY, "" );
            }

            /** Curl does not support PAC Url */
            else if ( network.value( "ProxyType" ).toInt() == 3 ) {
                emit weather( QString() );
                return;
            }

            curl_easy_setopt( handle, CURLOPT_URL, mUrl.toUtf8().data() );
            curl_easy_setopt(
                handle, CURLOPT_WRITEFUNCTION, +[] ( char *buffer, size_t size, size_t nmemb, QString * str )->size_t {
                    size_t newLength = size * nmemb;
                    try {
                        str->append( QString::fromUtf8( buffer, newLength ) );
                    } catch ( std::bad_alloc& e ) {
                        return 0;
                    }

                    return newLength;
                }
            );

            curl_easy_setopt( handle, CURLOPT_WRITEDATA, &response );

            /** Should not take more than 10s to get the info */
            curl_easy_setopt( handle, CURLOPT_TIMEOUT,   10 );

            /** Perform the request */
            CURLcode ret = curl_easy_perform( handle );

            Q_UNUSED( ret );

            /** Cleanup the handle */
            curl_easy_cleanup( handle );

            emit weather( response );
        }

    private:
        QString mUrl;

    Q_SIGNALS:
        void weather( QString data );
};


WeatherWidget::WeatherWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    weatherSett = new DFL::Settings( QDir::home().filePath( ".config/DesQ/WeatherInfo.conf" ) );

    QFontDatabase().addApplicationFont( ":/weather-icons.ttf" );

    weather = new QLabel();
    weather->setFixedSize( 2 * panelHeight, panelHeight );
    weather->setAlignment( Qt::AlignCenter );
    weather->setToolTip( "Click to refresh" );
    weather->setFont( QFont( "Weather Icons", 11 ) );
    weather->setText( "\uf075 -- °C" );

    /** We will use a QRunnable started in a thread to obtain the weather info */
    weatherTask = new WeatherTask();
    connect( weatherTask, &WeatherTask::weather, this, &WeatherWidget::updateWeather );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );

    lyt->addWidget( weather );

    setLayout( lyt );

    /** Check if we have the place set */
    QStringList keys = weatherSett->allKeys();

    if ( keys.contains( "Latitude" ) and keys.contains( "Longitude" ) ) {
        placeInit = true;
    }

    hourlyTimer = new QBasicTimer();

    mTooltip = new QLabel();

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    setFixedSize( 2 * panelHeight, panelHeight );

    setMouseTracking( true );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip and mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );

    getCurrentWeatherInfo();
}


void WeatherWidget::getCurrentWeatherInfo() {
    hourlyTimer->stop();

    weather->setText( "\uf075 -- °C" );

    mTooltip->setText( "Beware!!<br>Here there be aliens..." );

    QCoreApplication::processEvents();

    /** Place is not ready, do nothing! */
    if ( placeInit == false ) {
        return;
    }

    double  latitude  = weatherSett->value( "Latitude" );
    double  longitude = weatherSett->value( "Longitude" );
    QString timeZone  = weatherSett->value( "TimeZone" );

    weatherTask->setUrl( WeatherNowUrl.arg( latitude ).arg( longitude ).arg( timeZone ) );
    QThreadPool::globalInstance()->start( weatherTask );
}


void WeatherWidget::updateWeather( QString response ) {
    if ( response.isEmpty() ) {
        return;
    }

    QString placeStr = weatherSett->value( "Place" );

    QVariantMap weatherInfoMap = DFL::Config::Hjson::readConfigFromString( response );
    QVariantMap currentInfo    = weatherInfoMap[ "current_weather" ].toMap();

    qreal       curTemp  = currentInfo[ "temperature" ].toDouble();
    QStringList iconText = WeatherInfoMap[ currentInfo[ "weathercode" ].toInt() ];
    bool        isDay    = ( currentInfo[ "is_day" ].toInt() ? true : false );

    weather->setText( QString( "%1 <small>%2 °C</small>" ).arg( isDay ? iconText[ 1 ] : iconText[ 2 ] ).arg( curTemp, 0, 'f', 1 ) );

    mTooltip->setText( QString( "%1<br>%2" ).arg( placeStr ).arg( iconText[ 0 ] ) );

    /** Get the current time stamp */
    QDateTime dt = QDateTime::fromString( currentInfo[ "time" ].toString(), "yyyy-MM-ddTHH:mm" );

    /** Schedule the next update at 1h 1m from the current time stamp */
    int ms = QDateTime::currentDateTime().msecsTo( dt.addSecs( 3660 ) );

    /** Start the timer */
    hourlyTimer->start( ms, Qt::VeryCoarseTimer, this );
}


void WeatherWidget::mousePressEvent( QMouseEvent *mEvent ) {
    getCurrentWeatherInfo();

    QWidget::mousePressEvent( mEvent );
}


void WeatherWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == hourlyTimer->timerId() ) {
        getCurrentWeatherInfo();
    }
}


#include "WeatherWidget.moc"
