/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SNHost.hpp"
#include <QDebug>
#include <QtDBus>
#include <QLabel>

#define WATCHER_SERVICE_KDE    "org.kde.StatusNotifierWatcher"
#define WATCHER_OBJECT         "/StatusNotifierWatcher"
#define WATCHER_PATH_KDE       "org.kde.StatusNotifierWatcher"

#define WATCHER_SERVICE_FDO    "org.freedesktop.StatusNotifierWatcher"
#define WATCHER_OBJECT         "/StatusNotifierWatcher"
#define WATCHER_PATH_FDO       "org.freedesktop.StatusNotifierWatcher"

static inline void RegisterTrayHost( QDBusInterface *watcher, QString host ) {
    if ( watcher->isValid() ) {
        QDBusReply<void> reply = watcher->call( "RegisterStatusNotifierHost", host );

        if ( reply.isValid() ) {
            qInfo() << "Registered this widget as StatusNotifierHost.";
        }

        else {
            qDebug() << "Failed to register this widget as StatusNotifierHost. Clients may not realize that the tray is running.";
        }
    }

    else {
        qWarning() << "Unable to connect to" << watcher->service();
    }
}


TrayPluginWidget::TrayPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    QString dbusName = QString( "org.kde.StatusNotifierHost-%1-%2" ).arg( qApp->applicationPid() ).arg( 1 );

    QDBusInterface watcherKDE( WATCHER_SERVICE_KDE, WATCHER_OBJECT, WATCHER_PATH_KDE, QDBusConnection::sessionBus() );
    QDBusInterface watcherFDO( WATCHER_SERVICE_FDO, WATCHER_OBJECT, WATCHER_PATH_FDO, QDBusConnection::sessionBus() );

    RegisterTrayHost( &watcherKDE, dbusName );
    RegisterTrayHost( &watcherFDO, dbusName );

    sniLyt = new QHBoxLayout();
    sniLyt->setAlignment( Qt::AlignLeft );
    sniLyt->setSpacing( mItemSpacing );
    sniLyt->setContentsMargins( mMargins );
    setLayout( sniLyt );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_KDE,
        WATCHER_OBJECT,
        WATCHER_PATH_KDE,
        "StatusNotifierItemRegistered",
        "s",
        this,
        SLOT( itemAdded( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_FDO,
        WATCHER_OBJECT,
        WATCHER_PATH_FDO,
        "StatusNotifierItemRegistered",
        "s",
        this,
        SLOT( itemAdded( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_KDE,
        WATCHER_OBJECT,
        WATCHER_PATH_KDE,
        "StatusNotifierItemUnregistered",
        "s",
        this,
        SLOT( itemRemoved( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_FDO,
        WATCHER_OBJECT,
        WATCHER_PATH_FDO,
        "StatusNotifierItemUnregistered",
        "s",
        this,
        SLOT( itemRemoved( QString ) )
    );

    QVariant registered = watcherKDE.property( "RegisteredStatusNotifierItems" );

    for ( QString sni: registered.toStringList() ) {
        itemAdded( sni );
    }

    registered = watcherFDO.property( "RegisteredStatusNotifierItems" );

    for ( QString sni: registered.toStringList() ) {
        itemAdded( sni );
    }
}


TrayPluginWidget::~TrayPluginWidget() {
    delete sniLyt;
}


void TrayPluginWidget::itemAdded( QString serviceAndPath ) {
    if ( mServices.contains( serviceAndPath ) ) {
        return;
    }

    int                  slash   = serviceAndPath.indexOf( '/' );
    QString              serv    = serviceAndPath.left( slash );
    QString              path    = serviceAndPath.mid( slash );
    StatusNotifierButton *button = new StatusNotifierButton( serv, path, this );

    resizeButton( button );

    mServices.insert( serviceAndPath, button );
    sniLyt->addWidget( button );

    connect( button, &StatusNotifierButton::showPopup,   this, &DesQ::Panel::PluginWidget::showPopup );
    connect( button, &StatusNotifierButton::hidePopup,   this, &DesQ::Panel::PluginWidget::hidePopup );

    connect( button, &StatusNotifierButton::showTooltip, this, &DesQ::Panel::PluginWidget::showTooltip );
}


void TrayPluginWidget::itemRemoved( QString serviceAndPath ) {
    StatusNotifierButton *button = mServices.value( serviceAndPath, nullptr );

    if ( button ) {
        button->deleteLater();
        sniLyt->removeWidget( button );

        mServices.remove( serviceAndPath );
    }
}


void TrayPluginWidget::resizeButton( StatusNotifierButton *btn ) {
    btn->setFixedSize( panelHeight, panelHeight );
    btn->setIconSize( QSize( panelHeight - 4, panelHeight - 4 ) );
}
