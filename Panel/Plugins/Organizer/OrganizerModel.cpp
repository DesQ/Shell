/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OrganizerModel.hpp"

OrganizerModel::OrganizerModel() : QAbstractItemModel() {
    /** Our calendar */
    mCalendar = new QCalendar();

    /** Current user locale */
    mLocale = new QLocale();

    /** Current date */
    QDate dt = QDate::currentDate();

    /** Let's start with current month/year */
    mMonth = dt.month();
    mYear  = dt.year();

    /** Create the base for the calendar table data */
    for ( int r = 0; r < 6; r++ ) {
        QIntList row;
        for ( int c = 0; c < 7; c++ ) {
            row << 0;
        }
        mCal << row;
    }

    mCalFont     = qApp->font();
    mCalFontBold = qApp->font();

    mCalFont.setPointSize( mCalFont.pointSize() * 1.25 );
    mCalFontBold.setPointSize( mCalFont.pointSize() * 1.25 );
    mCalFontBold.setWeight( QFont::Bold );

    dataDirPath = QStandardPaths::writableLocation( QStandardPaths::ConfigLocation ) + "/DesQ/Organizer/";
    QDir dataDir( dataDirPath );

    if ( not dataDir.exists() ) {
        dataDir.mkpath( "." );
    }

    settReminders = new QSettings( dataDirPath + "Reminders.conf", QSettings::IniFormat );    // Brown:
                                                                                              // #A52A2A
    settEvents = new QSettings( dataDirPath + "Events.conf", QSettings::IniFormat );          // GoldenRod:
                                                                                              // #DAA520
    settNotes = new QSettings( dataDirPath + "Notes.conf", QSettings::IniFormat );            // Purple:
                                                                                              // #800080
    settTodos = new QSettings( dataDirPath + "TODOs.conf", QSettings::IniFormat );            // Teal:
                                                                                              // #008080

    QFileSystemWatcher *watcher = new QFileSystemWatcher( this );
    watcher->addPath( settReminders->fileName() );
    watcher->addPath( settEvents->fileName() );
    watcher->addPath( settNotes->fileName() );
    watcher->addPath( settTodos->fileName() );

    connect( watcher, &QFileSystemWatcher::fileChanged, this, &OrganizerModel::reloadModel );

    reloadModel();
}


void OrganizerModel::setMonth( int m, int y ) {
    bool needsUpdate = false;

    /** Update only if different */
    if ( ( m != 0 ) and ( mMonth != m ) ) {
        mMonth      = m;
        needsUpdate = true;
    }

    if ( ( y != 0 ) and ( mYear != y ) ) {
        mYear       = y;
        needsUpdate = true;
    }

    if ( needsUpdate ) {
        reloadModel();
    }
}


int OrganizerModel::month() {
    return mMonth;
}


int OrganizerModel::year() {
    return mYear;
}


int OrganizerModel::rowCount( const QModelIndex& ) const {
    return 6;
}


int OrganizerModel::columnCount( const QModelIndex& ) const {
    return 7;
}


Qt::ItemFlags OrganizerModel::flags( const QModelIndex& ) const {
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}


QModelIndex OrganizerModel::index( int row, int column, const QModelIndex& ) const {
    if ( ( row < 6 ) and ( column < 7 ) ) {
        return createIndex( row, column );
    }

    return QModelIndex();
}


QModelIndex OrganizerModel::index( QDate date ) const {
    /** Current month and year */
    if ( date.month() == mMonth and date.year() == mYear ) {
        int col = mCurMonthRange.first;
        for ( int r = 0; r < 6; r++ ) {
            for ( int c = col; c < 7; c++ ) {
                if ( date.day() == mCal[ r ][ c ] ) {
                    return createIndex( r, c );
                }
            }

            col = 0;
        }
    }

    /** Null index */
    return QModelIndex();
}


QModelIndex OrganizerModel::parent( const QModelIndex& ) const {
    return QModelIndex();
}


QVariant OrganizerModel::data( const QModelIndex& index, int role ) const {
    int elmIdx = index.row() * 7 + index.column();
    int date   = mCal[ index.row() ][ index.column() ];

    int month = mMonth;
    int year  = mYear;

    if ( elmIdx < mCurMonthRange.first ) {
        /** Last month of the previous year */
        if ( mMonth == 1 ) {
            month = 12;
            year--;
        }

        /** Previous month of the current year */
        else {
            month--;
        }
    }

    else if ( elmIdx > mCurMonthRange.second ) {
        /** First month of the next year */
        if ( mMonth == 12 ) {
            month = 1;
            year++;
        }

        /** Next month of the current year */
        else {
            month++;
        }
    }

    QDate dt( year, month, date );
    QDate curDt( QDate::currentDate() );

    switch ( role ) {
        case Qt::DisplayRole: {
            return date;
        }

        case Qt::ToolTipRole: {
            /** TODO: Return event/reminder/notes etc */
            return QString();
        }

        case Qt::SizeHintRole: {
            return QSize( 45, 30 );
        }

        case Qt::FontRole: {
            /** Current date */
            if ( dt == curDt ) {
                return mCalFontBold;
            }

            return mCalFont;
        }

        case Qt::TextAlignmentRole: {
            return Qt::AlignCenter;
        }

        case Qt::BackgroundRole: {
            return QColor( Qt::transparent );
        }

        case Qt::ForegroundRole: {
            /** If we're in the current month */
            if ( dt == curDt ) {
                return qApp->palette().color( QPalette::Highlight );
            }

            /** Saturday/Sunday */
            if ( mCalendar->dayOfWeek( dt ) >= 6 ) {
                QColor red( Qt::darkRed );

                if ( dt.month() != mMonth ) {
                    red.setAlphaF( 0.36 );
                }

                return red;
            }

            /** Holiday */
            // if ( isHoliday( dt ) ) {
            //     return QColor( Qt::darkRed );
            // }

            /** Other dates of the current month */
            if ( dt.month() == mMonth ) {
                return qApp->palette().color( QPalette::WindowText );
            }

            /** Dull the previous/next month dates */
            QColor clr( qApp->palette().color( QPalette::WindowText ) );
            clr.setAlphaF( 0.36 );

            return clr;
        }

        /** Also remember the date */
        case Qt::UserRole + 1: {
            return dt;
        }

        default: {
            return QVariant();
        }
    }
}


QVariant OrganizerModel::headerData( int section, Qt::Orientation orientation, int role ) const {
    if ( role == Qt::DisplayRole ) {
        if ( orientation == Qt::Horizontal ) {
            int day = ( section + mLocale->firstDayOfWeek() ) % 7;
            return mCalendar->weekDayName( QLocale(), ( day == 0 ? 7 : day ), QLocale::ShortFormat );
        }
    }

    return QVariant();
}


bool OrganizerModel::hasReminder( QDate date ) {
    Q_FOREACH ( QDateTime dt, rMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            return true;
        }
    }

    return false;
}


bool OrganizerModel::hasEvent( QDate date ) {
    Q_FOREACH ( QDateTime dt, eMap.uniqueKeys() ) {
        // If the date of the event corresponds to selected date
        if ( dt.date() == date ) {
            return true;
        }
    }

    return false;
}


bool OrganizerModel::hasNote( QDate date ) {
    Q_FOREACH ( QDateTime dt, nMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            return true;
        }
    }

    return false;
}


bool OrganizerModel::hasTodo( QDate date ) {
    Q_FOREACH ( QDateTime dt, tMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            return true;
        }
    }

    return false;
}


void OrganizerModel::setDone( QDateTime dt, int idx, bool yes ) {
    QString     dtStr = dt.toString( "yyyyMMddTHHmmss" );
    QStringList data  = settTodos->value( QString( "%1/%2" ).arg( dtStr ).arg( idx ) ).toStringList();

    data[ 1 ] = ( yes ? "true" : "false" );
    settTodos->setValue( QString( "%1/%2" ).arg( dtStr ).arg( idx ), data );

    settTodos->sync();
}


Organizer::Reminders OrganizerModel::reminders( QDate date ) {
    Organizer::Reminders rems;

    Q_FOREACH ( QDateTime dt, rMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            rems << rMap.values( dt );
        }
    }

    return rems;
}


Organizer::Events OrganizerModel::events( QDate date ) {
    Organizer::Events events;

    Q_FOREACH ( QDateTime dt, eMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            events << eMap.values( dt );
        }
    }

    return events;
}


Organizer::Notes OrganizerModel::notes( QDate date ) {
    Organizer::Notes notes;

    Q_FOREACH ( QDateTime dt, nMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            notes << nMap.values( dt );
        }
    }

    return notes;
}


Organizer::Todos OrganizerModel::todos( QDate date ) {
    Organizer::Todos todos;

    Q_FOREACH ( QDateTime dt, tMap.uniqueKeys() ) {
        if ( dt.date() == date ) {
            todos << tMap.values( dt );
        }
    }

    return todos;
}


void OrganizerModel::reloadModel() {
    /** Create the date */
    QDate dt( mYear, mMonth, 1 );

    int col     = ( dt.dayOfWeek() + mLocale->firstDayOfWeek() ) % 7;
    int maxDays = mCalendar->daysInMonth( mMonth, mYear );

    mCurMonthRange = qMakePair( col, col + maxDays - 1 );

    int date = 1;

    beginResetModel();

    for ( int r = 0; r < 6; r++ ) {
        for ( int c = col; c < 7; c++ ) {
            mCal[ r ][ c ] = date;
            date++;

            if ( date > maxDays ) {
                date = 1;
            }
        }

        col = 0;
    }

    col = ( dt.dayOfWeek() + mLocale->firstDayOfWeek() ) % 7;
    int revDate = mCalendar->daysInMonth( mMonth == 1 ? 12 : mMonth - 1, mYear );
    for ( int c = col - 1; c >= 0; c-- ) {
        mCal[ 0 ][ c ] = revDate;
        revDate--;
    }

    endResetModel();

    settReminders->sync();
    settEvents->sync();
    settNotes->sync();
    settTodos->sync();

    rMap.clear();
    eMap.clear();
    nMap.clear();
    tMap.clear();

    /* Reminders */
    Q_FOREACH ( QString date, settReminders->value( "Dates" ).toStringList() ) {
        /* Each date-time is a group; open it */
        settReminders->beginGroup( date );

        /* Get the QDateTime object for the date-time */
        QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

        /* Iterate through multiple entries */
        Q_FOREACH ( QString key, settReminders->childKeys() ) {
            /* Get the data */
            QStringList data = settReminders->value( key ).toStringList();

            Organizer::Reminder *rem = new Organizer::Reminder();
            rem->dateTime = dt;
            rem->title    = data[ 0 ];
            rem->text     = data[ 1 ];
            rem->alarm    = ( data[ 2 ] == "false" ? false : true );
            rem->notify   = ( data[ 3 ] == "false" ? false : true );

            /* Add it to the map */
            rMap.insert( dt, rem );
        }

        /* Close the group */
        settReminders->endGroup();
    }

    /* Events */
    Q_FOREACH ( QString date, settEvents->value( "Dates" ).toStringList() ) {
        /* Each date-time is a group; open it */
        settEvents->beginGroup( date );

        /* Get the QDateTime object for the date-time */
        QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

        /* Iterate through multiple entries */
        Q_FOREACH ( QString key, settEvents->childKeys() ) {
            /* Get the data */
            QStringList data = settEvents->value( key ).toStringList();

            Organizer::Event *event = new Organizer::Event();
            event->dateTime = dt;
            event->title    = data[ 0 ];
            event->text     = data[ 1 ];
            event->location = data[ 2 ];
            event->alarm    = ( data[ 3 ] == "false" ? false : true );
            event->notify   = ( data[ 4 ] == "false" ? false : true );
            event->repeat   = data[ 5 ].toInt();

            /* Add it to the map */
            eMap.insert( dt, event );
        }

        /* Close the group */
        settEvents->endGroup();
    }

    /* Notes */
    Q_FOREACH ( QString date, settNotes->value( "Dates" ).toStringList() ) {
        /* Each date-time is a group; open it */
        settNotes->beginGroup( date );

        /* Get the QDateTime object for the date-time */
        QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

        Q_FOREACH ( QString key, settNotes->childKeys() ) {
            QString data = settNotes->value( key ).toString();

            Organizer::Note *note = new Organizer::Note();

            /* Get the data */
            note->dateTime = dt;
            note->note     = data;

            /* Add it to the map */
            nMap.insert( dt, note );
        }

        /* Close the group */
        settNotes->endGroup();
    }

    /* TODOs */
    Q_FOREACH ( QString date, settTodos->value( "Dates" ).toStringList() ) {
        /* Each date-time is a group; open it */
        settTodos->beginGroup( date );

        /* Get the QDateTime object for the date-time */
        QDateTime dt = QDateTime::fromString( date, "yyyyMMddTHHmmss" );

        /* Iterate through multiple entries */
        int i = 1;
        Q_FOREACH ( QString key, settTodos->childKeys() ) {
            /* Get the data */
            QStringList data = settTodos->value( key ).toStringList();

            Organizer::Todo *event = new Organizer::Todo();
            event->dateTime = dt;
            event->text     = data[ 0 ];
            event->idx      = i;
            event->done     = ( data[ 1 ] == "false" ? false : true );

            /* Add it to the map */
            tMap.insert( dt, event );
            i++;
        }

        /* Close the group */
        settTodos->endGroup();
    }

    emit modelReloaded();
}
