/**
 * Orgranizer Plugin Viewer
 **/

#include <csignal>

#include "Global.hpp"
#include "OrganizerWidget.hpp"

int main( int argc, char *argv[] ) {
    QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );

    QApplication app( argc, argv );

    // Set application info
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Organizer" );

    OrganizerWidget *org = new OrganizerWidget();
    org->show();

    return app.exec();
}
