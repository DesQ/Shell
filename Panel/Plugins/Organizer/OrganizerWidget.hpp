/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "OrganizerModel.hpp"

#include <desqui/PanelPlugin.hpp>

class OrganizerWidget : public DesQ::Panel::PluginPopupWidget {
    Q_OBJECT

    public:
        OrganizerWidget( QWidget *parent = nullptr );

        void goToToday();

    private:
        QTableView *calendar;
        OrganizerModel *calModel;

        QLabel *infoLabel;
        QLabel *monthLbl;

        QToolButton *nextMonthBtn, *prevMonthBtn;
        QToolButton *nextYearBtn, *prevYearBtn;

        QWidget *infoWidget;
        QVBoxLayout *infoLyt;

        void showInfo( QDate );
};
