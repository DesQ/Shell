/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "OrganizerWidget.hpp"
#include "OrganizerModel.hpp"

OrganizerWidget::OrganizerWidget( QWidget *parent ) : DesQ::Panel::PluginPopupWidget( parent ) {
    calendar = new QTableView( this );
    calendar->setFocusPolicy( Qt::NoFocus );

    calModel = new OrganizerModel();
    calendar->setModel( calModel );

    setAttribute( Qt::WA_TranslucentBackground );
    setAttribute( Qt::WA_NoSystemBackground );

    calendar->setAttribute( Qt::WA_TranslucentBackground );
    calendar->setAttribute( Qt::WA_NoSystemBackground );

    calendar->viewport()->setAttribute( Qt::WA_TranslucentBackground );
    calendar->viewport()->setAttribute( Qt::WA_NoSystemBackground );

    calendar->setGridStyle( Qt::NoPen );
    calendar->horizontalHeader()->show();
    calendar->verticalHeader()->hide();

    calendar->horizontalHeader()->setStyleSheet( "background: rgba(0, 0, 0, 90);" );

    calendar->setFrameStyle( QFrame::NoFrame );
    calendar->setFixedSize( QSize( 315, 208 ) );

    calendar->horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch );
    calendar->verticalHeader()->setSectionResizeMode( QHeaderView::Stretch );

    nextMonthBtn = new QToolButton();
    nextMonthBtn->setIcon( QIcon::fromTheme( "go-next" ) );
    nextMonthBtn->setAutoRaise( true );
    nextMonthBtn->setFocusPolicy( Qt::NoFocus );
    connect(
        nextMonthBtn, &QToolButton::clicked, [ = ] () {
            if ( calModel->month() == 12 ) {
                calModel->setMonth( 1, calModel->year() + 1 );
            }

            else {
                calModel->setMonth( calModel->month() + 1 );
            }

            monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
        }
    );

    prevMonthBtn = new QToolButton();
    prevMonthBtn->setIcon( QIcon::fromTheme( "go-previous" ) );
    prevMonthBtn->setAutoRaise( true );
    prevMonthBtn->setFocusPolicy( Qt::NoFocus );
    connect(
        prevMonthBtn, &QToolButton::clicked, [ = ] () {
            if ( calModel->month() == 1 ) {
                calModel->setMonth( 12, calModel->year() - 1 );
            }

            else {
                calModel->setMonth( calModel->month() - 1 );
            }

            monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
        }
    );

    nextYearBtn = new QToolButton();
    nextYearBtn->setIcon( QIcon::fromTheme( "go-last" ) );
    nextYearBtn->setAutoRaise( true );
    nextYearBtn->setFocusPolicy( Qt::NoFocus );
    connect(
        nextYearBtn, &QToolButton::clicked, [ = ] () {
            if ( calModel->year() + 1 == 0 ) {
                calModel->setMonth( 0, 1 );
            }

            else {
                calModel->setMonth( 0, calModel->year() + 1 );
            }

            monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
        }
    );

    prevYearBtn = new QToolButton();
    prevYearBtn->setIcon( QIcon::fromTheme( "go-first" ) );
    prevYearBtn->setAutoRaise( true );
    prevYearBtn->setFocusPolicy( Qt::NoFocus );
    connect(
        prevYearBtn, &QToolButton::clicked, [ = ] () {
            if ( calModel->year() - 1 == 0 ) {
                calModel->setMonth( 0, -1 );
            }

            else {
                calModel->setMonth( 0, calModel->year() - 1 );
            }

            monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
        }
    );

    infoLabel = new QLabel();

    monthLbl = new QLabel();
    monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );

    QHBoxLayout *btnLyt = new QHBoxLayout();
    btnLyt->addWidget( prevYearBtn );
    btnLyt->addWidget( prevMonthBtn );
    btnLyt->addStretch();
    btnLyt->addWidget( monthLbl );
    btnLyt->addStretch();
    btnLyt->addWidget( nextMonthBtn );
    btnLyt->addWidget( nextYearBtn );

    QVBoxLayout *baseLyt = new QVBoxLayout();
    baseLyt->addLayout( btnLyt );
    baseLyt->addWidget( calendar );
    baseLyt->addWidget( infoLabel );

    setLayout( baseLyt );

    connect(
        calendar, &QTableView::clicked, [ = ] ( const QModelIndex& idx ) {
            QDate dt = idx.data( Qt::UserRole + 1 ).toDate();

            if ( dt.month() != calModel->month() ) {
                calModel->setMonth( dt.month(), dt.year() );
                infoLabel->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
                calendar->setCurrentIndex( calModel->index( dt ) );
            }

            showInfo( dt );
        }
    );

    showInfo( QDate::currentDate() );
}


void OrganizerWidget::goToToday() {
    QDate today = QDate::currentDate();

    calModel->setMonth( today.month(), today.year() );

    monthLbl->setText( QString( "%1, %2" ).arg( QLocale::system().monthName( calModel->month() ) ).arg( calModel->year() ) );
}


void OrganizerWidget::showInfo( QDate date ) {
    /* Delete all the todos */
    // for ( QCheckBox *check: infoWidget->findChildren<QCheckBox *>() ) {
    //     delete check;
    // }

    QString info;

    /* Reminders */
    if ( calModel->hasReminder( date ) ) {
        Organizer::Reminders reminders = calModel->reminders( date );
        for ( int i = 0; i < reminders.count(); i++ ) {
            Organizer::Reminder *rem = reminders.at( i );

            info += QString( "<b>Remember: %1</b><p>%2</p>" ).arg( rem->title ).arg( rem->text );
        }
    }

    /* Events */
    if ( calModel->hasEvent( date ) ) {
        Organizer::Events events = calModel->events( date );
        for ( int i = 0; i < events.count(); i++ ) {
            Organizer::Event *event = events.at( 0 );

            info += QString( "<b>Event: %1 at %3</b><p>%2</p>" ).arg( event->title ).arg( event->text ).arg( event->location );
        }
    }

    /* Notes */
    if ( calModel->hasNote( date ) ) {
        info += QString( "<b>Notes:</b><br>" );
        Organizer::Notes notes = calModel->notes( date );
        for ( int i = 0; i < notes.count(); i++ ) {
            Organizer::Note *note = notes.at( i );

            info += QString( "%1<br>" ).arg( note->note );
        }
        info += "<br>";
    }

    /* TODOs */
    if ( calModel->hasTodo( date ) ) {
        info += QString( "<b>To Do:</b>" );
        for ( int i = 0; i < calModel->todos( date ).count(); i++ ) {
            Organizer::Todo *todo = calModel->todos( date ).at( i );

            QCheckBox *check = new QCheckBox( todo->text );
            check->setChecked( todo->done );
            // infoLyt->addWidget( check );

            connect(
                check, &QCheckBox::clicked, [ = ]() {
                    calModel->setDone( todo->dateTime, todo->idx, check->isChecked() );
                }
            );
        }
    }

    infoLabel->setText( info );

    if ( info.length() ) {
        infoLabel->show();
    }

    /* Hide the info label if there is no info */
    else {
        infoLabel->hide();
    }
}
