/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "OrganizerModel.hpp"

typedef QList<int> QIntList;

namespace Organizer {
    typedef struct _rem {
        //Title,Text,Alarm,Notification
        QDateTime dateTime;
        QString   title;
        QString   text;
        bool      alarm;
        bool      notify;
    } Reminder;
    typedef QList<Reminder *> Reminders;

    typedef struct _event {
        //Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
        QDateTime dateTime;
        QString   title;
        QString   text;
        QString   location;
        bool      alarm;
        bool      notify;
        int       repeat;   // 0 = Never; 1 = Daily; 2 = Weekly; 3 = Monthly; 4 = Yearly
    } Event;
    typedef QList<Event *> Events;

    typedef struct _note {
        //Title,Text,Location,Alarm,Notification,Repeat(Never/Daily/Weekly/Monthly/Yearly)
        QDateTime dateTime;
        QString   note;
    } Note;
    typedef QList<Note *> Notes;

    typedef struct _todo {
        //Text,Alarm,Notification
        QDateTime dateTime;
        QString   text;
        int       idx;
        bool      done;
    } Todo;
    typedef QList<Todo *> Todos;
}


class OrganizerModel : public QAbstractItemModel {
    Q_OBJECT;

    public:
        OrganizerModel();

        /** Set month and/or year */
        void setMonth( int month = 0, int year = 0 );
        int month();
        int year();

        /* Children Info */
        int rowCount( const QModelIndex& parent    = QModelIndex() ) const;
        int columnCount( const QModelIndex& parent = QModelIndex() ) const;

        Qt::ItemFlags flags( const QModelIndex& ) const;

        /* Display Info */
        QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const;
        QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;

        /* Node Info */
        QModelIndex index( int row = 0, int column = 0, const QModelIndex& parent = QModelIndex() ) const;
        QModelIndex index( QDate date ) const;
        QModelIndex parent( const QModelIndex& index = QModelIndex() ) const;

        bool hasReminder( QDate );
        bool hasEvent( QDate );
        bool hasNote( QDate );
        bool hasTodo( QDate );

        Organizer::Reminders reminders( QDate );
        Organizer::Events events( QDate );
        Organizer::Notes notes( QDate );
        Organizer::Todos todos( QDate );

        void setDone( QDateTime, int, bool );

    private:
        QSettings *settReminders;
        QSettings *settEvents;
        QSettings *settNotes;
        QSettings *settTodos;

        QMultiMap<QDateTime, Organizer::Reminder *> rMap;
        QMultiMap<QDateTime, Organizer::Event *> eMap;
        QMultiMap<QDateTime, Organizer::Note *> nMap;
        QMultiMap<QDateTime, Organizer::Todo *> tMap;

        QString dataDirPath;

        void reloadModel();

        int mMonth;
        int mYear;

        /** Cell ranges of the current month */
        QPair<int, int> mCurMonthRange;

        QCalendar *mCalendar;
        QLocale *mLocale;

        QFont mCalFont;
        QFont mCalFontBold;

        QList<QIntList> mCal;

    Q_SIGNALS:
        void modelReloaded();
};
