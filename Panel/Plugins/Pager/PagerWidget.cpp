/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <iostream>

#include "Global.hpp"
#include "PagerWidget.hpp"
#include "dbustypes.hpp"

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

WorkspaceWidget::WorkspaceWidget( WorkSpace ws, int number, QWidget *parent ) : QWidget( parent ) {
    mCurWS  = ws;
    mNumber = number;
    setFixedSize( QSize( 32, 24 ) );

    setMouseTracking( true );
}


WorkSpace WorkspaceWidget::workSpace() {
    return mCurWS;
}


void WorkspaceWidget::highlight( bool yes ) {
    mHighlight = yes;
    repaint();
}


void WorkspaceWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void WorkspaceWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( ( mEvent->button() == Qt::LeftButton ) and mPressed ) {
        /**
         * If we're highlighted, then this is the current workspace.
         * We toggle "ShowDesktop"
         */
        if ( mHighlight ) {
            emit showDesktop( mCurWS );
        }

        else {
            emit switchWorkSpace( mCurWS );
        }
    }

    repaint();
    mEvent->accept();
}


// void WorkspaceWidget::enterEvent( QEvent *event ) {
//     // emit showWorkSpaceInfo( mCurWS );
//
//     QWidget::enterEvent( event );
// }


void WorkspaceWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();

    if ( mHighlight ) {
        QColor clr( palette().color( QPalette::Highlight ) );
        clr = clr.darker();
        painter.setPen( QPen( clr, 1.0 ) );
        clr = clr.lighter();
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    else {
        QColor clr( palette().color( QPalette::Window ) );
        painter.setPen( QPen( clr, 1.0 ) );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    painter.restore();

    painter.save();

    if ( mHighlight ) {
        QFont f( font() );
        f.setWeight( QFont::Bold );
        painter.setFont( f );
    }

    painter.drawText( rect(), Qt::AlignCenter, QString::number( mNumber ) );
    painter.restore();

    painter.end();

    pEvent->accept();
}


PagerPluginWidget::PagerPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    /** Defaults, so that we don't have a crash */
    currentWset         = 1;
    currentGrid.rows    = 1;
    currentGrid.columns = 1;
    currentWS.row       = 0;
    currentWS.column    = 0;

    wsLyt = new QGridLayout();
    wsLyt->setContentsMargins( QMargins( 5, 0, 5, 0 ) );
    setLayout( wsLyt );

    connect(
        this, &DesQ::Panel::PluginWidget::panelScreenChanged, [ = ] ( QScreen *scrn ) {
            /** First get the output information */
            QJsonArray opInfoList = wfIpc->listOutputs();

            /** 'result' will be there only when something goes wrong */
            if ( opInfoList.count() == 0 ) {
                populateLayout( { 1, 1 }, true );
                return;
            }

            /** And retrieve the output-id of the current screen */
            for ( uint i = 0; i < opInfoList.size(); i++ ) {
                QJsonObject opInfo = opInfoList[ i ].toObject();

                /** Get the name */
                QString opName = opInfo[ "name" ].toString();

                /** If this is the screen we are rendering */
                if ( scrn->name() == opName ) {
                    /** Save the output id */
                    mScreenId = (uint32_t)opInfo[ "id" ].toInt();

                    /** Now we can set the workspace-set */
                    currentWset = (uint32_t)opInfo[ "wset-index" ].toInt();

                    /** Next, we set the workspace grid */
                    QJsonObject curWS   = opInfo[ "workspace" ].toObject();
                    currentGrid.rows    = (uint32_t)curWS[ "grid_height" ].toInt();
                    currentGrid.columns = (uint32_t)curWS[ "grid_width" ].toInt();

                    /** Finally, we set the current workspace */
                    currentWS.row    = (uint32_t)curWS[ "y" ].toInt();
                    currentWS.column = (uint32_t)curWS[ "x" ].toInt();

                    populateLayout( currentGrid );
                    highlightWorkspace( currentWS );

                    break;
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceChanged, [ = ] ( QJsonDocument wsInfoJson ) {
            // std::cout << wsInfo.dump( 4 ) << std::endl;

            if ( wsInfoJson.isObject() ) {
                QJsonObject wsInfo = wsInfoJson.object();
                QJsonObject opData = wsInfo[ "output-data" ].toObject();

                if ( (uint32_t)opData[ "id" ].toInt() == mScreenId ) {
                    QJsonObject newWSInfo = wsInfo[ "new-workspace" ].toObject();

                    currentWS.row    = (uint32_t)newWSInfo[ "y" ].toInt();
                    currentWS.column = (uint32_t)newWSInfo[ "x" ].toInt();

                    highlightWorkspace( currentWS );
                }
            }
        }
    );

    connect(
        wfIpc, &DFL::IPC::Wayfire::workspaceSetChanged, [ = ] ( QJsonDocument wssInfoJson ) {
            // std::cout << wssInfo.dump( 4 ) << std::endl;

            if ( wssInfoJson.isObject() ) {
                QJsonObject wssInfo = wssInfoJson.object();
                QJsonObject opData  = wssInfo[ "output-data" ].toObject();

                if ( (uint32_t)opData[ "id" ].toInt() == mScreenId ) {
                    QJsonObject newWsetInfo = wssInfo[ "new-wset-data" ].toObject();
                    /** Now we can set the workspace-set */
                    currentWset = (uint32_t)wssInfo[ "index" ].toInt();

                    QJsonObject wsGrid = newWsetInfo[ "workspace" ].toObject();

                    /** Next, we set the workspace grid */
                    currentGrid.rows    = wsGrid[ "grid_height" ].toInt();
                    currentGrid.columns = wsGrid[ "grid_width" ].toInt();

                    /** Finally, we set the current workspace */
                    currentWS.row    = wsGrid[ "y" ].toInt();
                    currentWS.column = wsGrid[ "x" ].toInt();

                    populateLayout( currentGrid );
                    highlightWorkspace( currentWS );
                }
            }
        }
    );

    /** So that we know exactly where the mouse is */
    setMouseTracking( true );

    popup = new QLabel();
    popup->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 180);" );
    popup->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
}


PagerPluginWidget::~PagerPluginWidget() {
    qDeleteAll( workspaceList );

    delete wsLyt;
    delete popup;
}


void PagerPluginWidget::populateLayout( WorkSpaceGrid grid, bool dummy ) {
    /* Delete the previously added WorkSpace widgets */
    if ( wsLyt->count() ) {
        qDeleteAll( wsLyt->findChildren<WorkspaceWidget *>() );
    }

    if ( workspaceList.count() ) {
        qDeleteAll( workspaceList );
    }

    workspaceList.clear();

    for ( int r = 0; r < grid.rows; r++ ) {
        for ( int c = 0; c < grid.columns; c++ ) {
            int             wsCount = r * grid.columns + c + 1;
            WorkspaceWidget *wsw    = new WorkspaceWidget( { r, c }, wsCount, this );
            wsLyt->addWidget( wsw, r, c );
            workspaceList << wsw;

            if ( dummy == false ) {
                connect( wsw, &WorkspaceWidget::showWorkSpaceInfo, this, &PagerPluginWidget::showWorkSpaceInfo );

                connect(
                    wsw, &WorkspaceWidget::switchWorkSpace, [ = ] ( WorkSpace ws ) {
                        wfIpc->switchToWorkspace( mScreenId, ws.row, ws.column );
                    }
                );

                connect(
                    wsw, &WorkspaceWidget::showDesktop, [ = ] ( WorkSpace ) {
                        QJsonObject request;
                        request[ "method" ] = "wm-actions/toggle_showdesktop";
                        request[ "data" ]   = QJsonObject( {
                                                               { "output_id", QJsonValue::fromVariant( (quint64)mScreenId ) },
                                                           } );
                        wfIpc->genericRequest( QJsonDocument( request ) );
                    }
                );

                connect(
                    wsw, &WorkspaceWidget::hideTooltip, [ = ]() {
                        if ( popup->isVisible() ) {
                            popup->close();
                        }
                    }
                );
            }

            else {
                wsw->setDisabled( true );
            }
        }
    }
}


void PagerPluginWidget::highlightWorkspace( WorkSpace ws ) {
    for ( WorkspaceWidget *wsw: workspaceList ) {
        if ( wsw->workSpace() == ws ) {
            wsw->highlight( true );
        }

        else {
            wsw->highlight( false );
        }
    }
}


void PagerPluginWidget::updatePopup( uint ) {
    if ( popup and popup->isVisible() ) {
        QPoint          cursor = mapFromGlobal( QCursor::pos() );
        WorkspaceWidget *ww    = qobject_cast<WorkspaceWidget *>( childAt( cursor ) );

        if ( ww ) {
            WorkSpace ws = ww->workSpace();
            showWorkSpaceInfo( ws );
        }
    }
}


void PagerPluginWidget::showWorkSpaceInfo( WorkSpace ws ) {
    // QString info = QString( "Workspace (%1, %2)" ).arg( ws.row + 1 ).arg( ws.column + 1 );
    // for ( uint vId: reply.value() ) {
    //     QDBusReply<QString> title = views->call( "QueryViewTitle", vId );

    //     if ( title.isValid() ) {
    //         info += "<br>&nbsp;&nbsp;&nbsp;&nbsp;- " + title.value();
    //     }
    // }

    // popup->setText( info );
    // if ( not popup->isVisible() ) {
    //     emit showTooltip( popup );
    // }

    // if ( popup->isVisible() ) {
    //     close();
    // }
}
