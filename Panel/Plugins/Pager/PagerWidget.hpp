/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QIcon>
#include <QLabel>
#include <QWidget>
#include <QGridLayout>
#include <desqui/PanelPlugin.hpp>

#include <QtPlugin>

#include "dbustypes.hpp"

class WorkspaceWidget : public QWidget {
    Q_OBJECT;

    public:
        WorkspaceWidget( WorkSpace, int number, QWidget * );

        WorkSpace workSpace();

        void highlight( bool );

    private:
        WorkSpace mCurWS;
        int mNumber;
        bool mPressed   = false;
        bool mHighlight = false;

    protected:
        void mousePressEvent( QMouseEvent *mEvent );
        void mouseReleaseEvent( QMouseEvent *mEvent );

        void paintEvent( QPaintEvent *pEvent );

    Q_SIGNALS:
        void showDesktop( WorkSpace );
        void switchWorkSpace( WorkSpace );
        void showWorkSpaceInfo( WorkSpace );
        void hideTooltip();
};


class PagerPluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT

    public:
        PagerPluginWidget( QWidget *parent = 0 );
        ~PagerPluginWidget();

    private:
        QGridLayout *wsLyt;
        QList<WorkspaceWidget *> workspaceList;

        uint32_t currentWset = 0;
        WorkSpaceGrid currentGrid;
        WorkSpace currentWS;

        QLabel *popup = nullptr;

        /** This is what we will be using to track our events */
        uint32_t mScreenId = 0;

        /** Populate the layout */
        Q_SLOT void populateLayout( WorkSpaceGrid, bool dummy = false );

        /** Highlight the active workspace */
        Q_SLOT void highlightWorkspace( WorkSpace );

        /** Update the popup to show new views */
        Q_SLOT void updatePopup( uint view );

        void showWorkSpaceInfo( WorkSpace );
};


class PagerPlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "Pager";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "virtual-desktops" );
        }

        /* The plugin version */
        QString version() override {
            return PROJECT_VERSION;
        }

        /* The clock Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new PagerPluginWidget( parent );
        }
};
