/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <csignal>
#include <unistd.h>

#include <iostream>
#include <sstream>

// Local Headers
#include "Global.hpp"
#include "ModernUI.hpp"
#include "Manager.hpp"

#include <signal.h>
#include <desq/Utils.hpp>

#include <QtDBus>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFApplication.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

DFL::Settings *panelSett  = nullptr;
WQt::Registry *wlRegistry = nullptr;

DFL::IPC::Wayfire *wfIpc = nullptr;

QDBusInterface *compositor = nullptr;
QDBusInterface *workspaces = nullptr;
QDBusInterface *output     = nullptr;
QDBusInterface *views      = nullptr;

QHash<QString, DesQ::Panel::ModernUI *> panelInstances;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Panel.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Panel" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-panel" );

    /** Remain open */
    app->setQuitOnLastWindowClosed( false );

    QObject::connect(
        app, &DFL::Application::unixSignalReceived, [ app ] ( int signum ) {
            if ( ( signum == SIGTERM ) || ( signum == SIGQUIT ) || ( signum == SIGINT ) ) {
                app->quit();
            }
        }
    );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "r", "raise" }, "Raise the panel to the top" } );
    parser.addOption( { { "l", "lower" }, "Lower the panel to the bottom" } );
    parser.addOption( { { "t", "toggle" }, "Show/hide the panel instance" } );
    parser.addOption( { { "o", "output" }, "Output on which raise/lower/toggle is done.", "output" } );

    parser.process( *app );

    /** Ensure all env vars and locations are available */
    DesQ::Utils::prepareSessionVariables();

    /** If the panel is already running, bug out */
    if ( app->isRunning() ) {
        if ( parser.isSet( "raise" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Panel instance is to be raised.";
                return 1;
            }

            app->messageServer( "raise\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "lower" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Panel instance is to be lowered.";
                return 1;
            }

            app->messageServer( "lower\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "toggle" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which Panel instance is to be lowered.";
                return 1;
            }

            app->messageServer( "toggle\n" + parser.value( "output" ) );
        }

        else {
            qWarning() << "Panel is already running";
            parser.showHelp();
        }

        return 0;
    }

    if ( app->lockApplication() ) {
        /** Prepare the DesQ Settings */
        panelSett = DesQ::Utils::initializeDesQSettings( "Panel", "Panel" );

        wfIpc = new DFL::IPC::Wayfire( qgetenv( "WAYFIRE_SOCKET" ) );
        qInfo() << "Connecting to" << qgetenv( "WAYFIRE_SOCKET" ) << wfIpc->connectToServer();

        /** Wayland Registry */
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );
        wlRegistry->setup();

        DesQ::Panel::Manager *panelMgr = new DesQ::Panel::Manager();

        QObject::connect( app, &DFL::Application::messageFromClient, panelMgr, &DesQ::Panel::Manager::handleMessages );

        return app->exec();
    }

    else {
        qCritical() << "Unable to lock this instance. Aborting...";
        return 1;
    }

    return 0;
}
