#!/bin/bash

# Install the basic dependencies
sudo apt update
sudo apt -y --no-install-recommends install pkgconf gcc g++ git qtbase5-dev qtbase5-private-dev meson ninja-build qtbase5-dev-tools cmake ca-certificates libqt5svg5-dev libwayland-dev qtwayland5-private-dev libpng-dev libsensors-dev libdbusmenu-qt5-dev

if [ ! -d depends ]; then
    mkdir depends
fi

cd depends

declare -a dfl=("utils" "xdg" "ipc" "applications" "settings" "layouts" "wayqt" "status-notifier" "login1" "inotify", "color-utils")
declare -a desq=("libdesq" "libdesqui")

# Build and install DFL
for i in "${dfl[@]}"
do
    git clone https://gitlab.com/desktop-frameworks/$i
    cd $i;
    meson .build --prefix=/usr --buildtype=release
    ninja -C .build -j 2 -k 0 && sudo ninja -C .build install
    cd ../
done

for i in "${desq[@]}"
do
    git clone https://gitlab.com/DesQ/$i
    cd $i
    meson .build --prefix=/usr --buildtype=release
    ninja -C .build -j 2 -k 0 && sudo ninja -C .build install
    cd ../
done

# Come out of 'depends'
cd ../

meson .build --prefix=/usr --buildtype=release
ninja -C .build -j 4 -k 0 && sudo ninja -C .build install
