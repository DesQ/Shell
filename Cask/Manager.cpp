/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Cask.hpp"
#include "Manager.hpp"
#include "Global.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFApplication.hpp>

DesQ::Cask::Manager::Manager( QStringList widgets ) {
    mWidgets << widgets;

    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn );

        QVariantAnimation *showAnim = createAnimator( mSurfaces[ scrn->name() ], mInstances[ scrn->name() ], true );
        showAnim->start();
        mInstanceStates[ scrn->name() ] = true;
    }

    connect(
        qApp, &QApplication::screenAdded, [ = ] ( QScreen *screen ) {
            createInstance( screen );

            QVariantAnimation *showAnim = createAnimator( mSurfaces[ screen->name() ], mInstances[ screen->name() ], true );
            showAnim->start();
            mInstanceStates[ screen->name() ] = true;
        }
    );
}


DesQ::Cask::Manager::~Manager() {
}


void DesQ::Cask::Manager::handleMessages( QString mesg, int fd ) {
    if ( mesg == "list-widgets" ) {
        qApp->messageClient( mWidgets.join( "\n" ), fd );
    }

    else if ( mesg.startsWith( "add" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) && ( mWidgets.contains( parts[ 1 ] ) == false ) ) {
            mWidgets << parts[ 1 ];
        }
    }

    else if ( mesg.startsWith( "insert" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) and not mWidgets.contains( parts[ 1 ] ) ) {
            mWidgets << parts[ 1 ];
        }
    }

    else if ( mesg.startsWith( "reload" ) ) {
    }

    else if ( mesg.startsWith( "remove" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) and mWidgets.contains( parts[ 1 ] ) ) {
            mWidgets.removeAll( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "raise" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) ) {
            raiseInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "lower" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( ( parts.count() >= 2 ) ) {
            lowerInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "toggle" ) ) {
        QStringList parts = mesg.split( "\n" );

        /** Already on top, so lower it. */
        if ( mSurfaceStates[ parts[ 1 ] ] == WQt::LayerShell::Top ) {
            lowerInstance( parts[ 1 ] );
        }

        /** Already on bottom, so riase it. */
        else {
            raiseInstance( parts[ 1 ] );
        }
    }

    else {
        qDebug() << "Invalid command\n";
        return;
    }

    for ( DesQ::Cask::UI *cask: mInstances.values() ) {
        if ( mesg.startsWith( "add\n" ) ) {
            QStringList parts = mesg.split( "\n" );

            if ( parts.count() >= 2 ) {
                cask->addWidget( parts[ 1 ] );
            }
        }

        else if ( mesg.startsWith( "insert\n" ) ) {
            QStringList parts = mesg.split( "\n" );

            if ( parts.count() >= 3 ) {
                int  pos = 0;
                bool ok  = false;
                parts[ 2 ].toInt( &ok );

                if ( ok ) {
                    cask->insertWidget( parts[ 1 ], pos );
                }

                else {
                    cask->insertWidget( parts[ 1 ], -1 );
                }
            }
        }

        else if ( mesg.startsWith( "remove\n" ) ) {
            QStringList parts = mesg.split( "\n" );

            if ( parts.count() >= 2 ) {
                cask->removeWidget( parts[ 1 ] );
            }
        }

        else if ( mesg.startsWith( "reload\n" ) ) {
            QStringList parts = mesg.split( "\n" );

            if ( parts.count() >= 2 ) {
                cask->reloadWidget( parts[ 1 ] );
            }
        }
    }
}


void DesQ::Cask::Manager::createInstance( QScreen *scrn ) {
    DesQ::Cask::UI *ui = new DesQ::Cask::UI( mWidgets, scrn );

    ui->show();

    if ( WQt::Utils::isWayland() ) {
        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen( scrn );

        WQt::LayerSurface *cls = wlRegistry->layerShell()->getLayerSurface(
            ui->windowHandle(),               // Window Handle
            output,                           // wl_output object - for multi-monitor support
            WQt::LayerShell::Bottom,          // Background layer
            "desq-cask"                       // Dummy namespace
        );

        /** Anchor everywhere */
        cls->setAnchors(
            WQt::LayerSurface::Top |
            WQt::LayerSurface::Bottom |
            WQt::LayerSurface::Right
        );

        /** Size - when */
        cls->setSurfaceSize( ui->size() );

        /** Nothing should move this */
        cls->setExclusiveZone( 0 );

        /** No keyboard interaction */
        cls->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

        /** Default margins: hide the surface */
        cls->setMargins( QMargins( 0, 0, -ui->width(), 0 ) );

        /** Apply the changes */
        cls->apply();

        mInstances[ scrn->name() ]      = ui;
        mSurfaces[ scrn->name() ]       = cls;
        mSurfaceStates[ scrn->name() ]  = WQt::LayerShell::Bottom;
        mInstanceStates[ scrn->name() ] = false;
    }

    else {
        ui->close();
        delete ui;
    }
}


void DesQ::Cask::Manager::destroyInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) ) {
        WQt::LayerSurface *surf = mSurfaces.take( opName );
        delete surf;
    }

    if ( mSurfaceStates.contains( opName ) ) {
        mSurfaceStates.remove( opName );
    }

    if ( mInstances.contains( opName ) ) {
        DesQ::Cask::UI *w = mInstances.take( opName );
        w->close();
        delete w;
    }

    if ( mInstanceStates.contains( opName ) ) {
        mInstanceStates.remove( opName );
    }
}


void DesQ::Cask::Manager::raiseInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Top ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Top;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
            mSurfaces[ opName ]->setKeyboardInteractivity( WQt::LayerSurface::OnDemand );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    QTimer *hideTimer = new QTimer();
    hideTimer->setSingleShot( true );
    hideTimer->setInterval( 2500 );
    connect(
        hideTimer, &QTimer::timeout, [ = ] () {
            /** Cask in still under the mouse */
            if ( mInstances[ opName ]->underMouse() ) {
                qDebug() << "Under mouse";
                hideTimer->start();
            }

            /** Mouse is away, hide/lower the cask */
            else {
                /** Cask is to be lowered */
                qDebug() << "Cask lowering";
                lowerInstance( opName );

                hideTimer->stop();
            }
        }
    );

    connect(
        grp, &QSequentialAnimationGroup::finished, [ = ] () {
            qDebug() << "Starting hide timer";
            hideTimer->start();
        }
    );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Cask::Manager::lowerInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Bottom ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Bottom;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Bottom );
            mSurfaces[ opName ]->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::Cask::Manager::showLayerSurface( QString opName ) {
    if ( not mInstances.contains( opName ) ) {
        return;
    }

    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    showAnim->start();

    mInstanceStates[ opName ] = true;
}


void DesQ::Cask::Manager::hideLayerSurface( QString opName ) {
    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );

    hideAnim->start();

    mInstanceStates[ opName ] = false;
}


QVariantAnimation * DesQ::Cask::Manager::createAnimator( WQt::LayerSurface *surf, DesQ::Cask::UI *ui, bool show ) {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    if ( show ) {
        anim->setStartValue( -ui->width() );
        anim->setEndValue( 10 );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::OutCubic ) );
    }

    else {
        anim->setStartValue( 10 );
        anim->setEndValue( -ui->width() );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuart ) );
    }

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
            surf->setMargins( QMargins( 0, 0, val.toInt(), 0 ) );
            surf->apply();

            qApp->processEvents();
        }
    );

    return anim;
}
