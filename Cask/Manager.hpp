/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include "Cask.hpp"

namespace DesQ {
    namespace Cask {
        class UI;
        class Manager;
    }
}

class QVariantAnimation;

/**
 * @class DesQ::Cask::Manager
 * This class will handle the creation, showing, hiding and destruction
 * of DesQ::Cask::UI UI on one or all screens.
 * Additionally, it will also handle the CLI requests
 */
class DesQ::Cask::Manager : public QObject {
    Q_OBJECT;

    public:
        Manager( QStringList widgets );
        ~Manager();

        /**
         * Load the user settings.
         */
        void loadSettings();

        /**
         * Handle the requests from other instance. To reply to queries,
         * write suitable data to the file descriptor @fd.
         */
        Q_SLOT void handleMessages( QString msg, int fd );

        /**
         * Start an instance of DesQ::Cask::UI on screen @scrn
         */
        void createInstance( QScreen *scrn );

        /**
         * Stop an instance of DesQ::Cask::UI on screen named @opName
         */
        void destroyInstance( QString opName );

        /**
         * Raise an existing instance of DesQ::Cask::UI
         * which is open on screen named @opName.
         * NOTE: To be used when DesQ::Cask::UI is shown on desktop.
         */
        void raiseInstance( QString opName );

        /**
         * Lower an existing instance of DesQ::Cask::UI
         * which is open on screen named @opName.
         * NOTE: To be used when DesQ::Cask::UI is shown on desktop.
         */
        void lowerInstance( QString opName );

    private:
        void showLayerSurface( QString );            // Impl details: Perform the actual showing
        void hideLayerSurface( QString );            // Impl details: Perform the actual hiding

        QHash<QString, DesQ::Cask::UI *> mInstances; // List of instances according to the screen
        QHash<QString, bool> mInstanceStates;        // Note the screens on which an instances exists

        QHash<QString, WQt::LayerSurface *> mSurfaces;
        QHash<QString, WQt::LayerShell::LayerType> mSurfaceStates;

        bool mBackgroundMode = false;

        QStringList mWidgets;

        /**
         * Return an animator which will animate a layer surface when started.
         * @surf - Layer surface to be moved
         * @ui   - Instance of DesQ::Cask::UI (used to calculate the margin positions)
         * @show - If true, the widget is being shown, otherwise hidden
         */
        QVariantAnimation *createAnimator( WQt::LayerSurface *surf, DesQ::Cask::UI *ui, bool show );
};
