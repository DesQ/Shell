/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Casklet.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>
#include <desqui/ShellPlugin.hpp>

/** Eventually to be replaced by caskSett->value( "CaskWidth" ) */
const int WIDGET_WIDTH = 200;

// DesQ Headers
#include <desqui/ShellPlugin.hpp>

static inline DesQ::Shell::PluginWidget * failedPlugin( QString pluginName, QWidget *parent ) {
    QLabel *lbl = new QLabel();

    lbl->setAlignment( Qt::AlignCenter );
    lbl->setPixmap( QIcon::fromTheme( "computer-fail-symbolic" ).pixmap( 64, 64 ) );

    DesQ::Shell::PluginWidget *base    = new DesQ::Shell::PluginWidget( parent );
    QHBoxLayout               *baseLyt = new QHBoxLayout();

    baseLyt->setContentsMargins( QMargins() );
    baseLyt->addStretch();
    baseLyt->addWidget( lbl );
    baseLyt->addStretch();
    base->setLayout( baseLyt );

    QLabel *popup = new QLabel( "Failed to load the plugin " + pluginName );

    popup->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    popup->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );

    QObject::connect(
        base, &DesQ::Shell::PluginWidget::entered, [ = ] () {
            // emit base->showTooltip( popup );
        }
    );

    QObject::connect(
        base, &DesQ::Shell::PluginWidget::exited, [ = ] () {
            // if ( popup->isVisible() ) {
            //     popup->close();
            // }
        }
    );

    return base;
}


DesQ::Cask::Casklet::Casklet( QString widget, QScreen *scrn ) {
    mScreen = scrn;
    QSettings wSett( MetadataPath + widget + ".ini", QSettings::IniFormat );

    mPluginName = widget;
    mWidgetName = wSett.value( "Name" ).toString();
    mWidgetDesc = wSett.value( "Descrpition" ).toString();

    setToolTip( mWidgetDesc );
    setAttribute( Qt::WA_TranslucentBackground );

    setFixedWidth( WIDGET_WIDTH );

    /* To show the widget name and the error message */
    setMinimumHeight( 50 );

    /** Layout */
    lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins( 2, 20, 2, 2 ) );
    setLayout( lyt );

    pluginWidget = nullptr;
    QTimer::singleShot( 1000, this, &DesQ::Cask::Casklet::prepareWidget );
}


bool DesQ::Cask::Casklet::canReload() {
    return pluginWidget->canReload();
}


void DesQ::Cask::Casklet::reload() {
    if ( pluginWidget->canReload() ) {
        delete pluginWidget;
        pluginWidget = nullptr;

        prepareWidget();
    }
}


void DesQ::Cask::Casklet::prepareWidget() {
    repaint();
    qApp->processEvents();

    QPluginLoader loader( PluginPath "shell/libdesq-plugin-" + mPluginName + ".so" );
    QObject       *pObject = loader.instance();

    pluginWidget = nullptr;

    if ( pObject ) {
        DesQ::Plugin::ShellInterface *plugin = qobject_cast<DesQ::Plugin::ShellInterface *>( pObject );

        if ( plugin ) {
            pluginWidget = plugin->widget( this );
            /** Margin of 2px on the left and 2x on the right = 4px. */
            pluginWidget->setWidgetWidth( WIDGET_WIDTH - 4 );
            pluginWidget->setWidgetScreen( mScreen );
            lyt->addWidget( pluginWidget );

            connect(
                pluginWidget, &DesQ::Shell::PluginWidget::showPopup, [ = ] ( QWidget * ) {
                    // showPopup( popup, pluginWidget );
                }
            );

            connect(
                pluginWidget, &DesQ::Shell::PluginWidget::showTooltip, [ = ] ( QWidget * ) {
                    // showTooltip( tooltip, pluginWidget );
                }
            );

            return;
        }
    }

    qWarning() << "Plugin Error:" << mPluginName << loader.errorString();
    pluginWidget = failedPlugin( mPluginName, this );

    /**
     * Connect the showTooltip signal to the corresponding slot.
     * We know for sure that when the mouse exits the widget, the toolip is closed.
     */
    connect(
        pluginWidget, &DesQ::Shell::PluginWidget::showTooltip, [ = ] ( QWidget * ) {
            // showTooltip( tooltip, failed );
        }
    );
    lyt->addWidget( pluginWidget );

    repaint();
    qApp->processEvents();
}


void DesQ::Cask::Casklet::resizeContainer() {
    qDebug() << "Not implemented";
}


void DesQ::Cask::Casklet::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QBrush brush( QColor( 0, 0, 0, 180 ) );

    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( brush );
    painter.drawRoundedRect( rect().adjusted( 0, 0, -1, -1 ), 3, 3 );
    painter.restore();

    if ( pluginWidget ) {
        /* Nothing from our side */
        painter.drawText( QRect( 0, 3, width(), height() ), Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap, mWidgetName );
        QWidget::paintEvent( pEvent );
    }

    else {
        painter.drawText( rect(), Qt::AlignCenter | Qt::TextWordWrap, "Loading the widget..." );
        pEvent->accept();
    }

    painter.end();
}
