/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"

#include "Cask.hpp"
#include "Casklet.hpp"

#include <DFApplication.hpp>

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

// DesQ Headers
#include <desqui/ShellPlugin.hpp>

DesQ::Cask::UI::UI( QStringList widgets, QScreen *scrn ) {
    mScreen = scrn;
    mWidgets << widgets;

    /** If not widgets are specified, load the default set */
    if ( mWidgets.empty() ) {
        mWidgets = shellSett->value( "Cask::Args" );
    }

    /* Title */
    setWindowTitle( "DesQShell | Widgets" );
    setToolTip( "Desktop Widgets of DesQ Shell" );

    /* No frame, and stay at bottom */
    Qt::WindowFlags wFlags = Qt::FramelessWindowHint | Qt::WindowStaysOnBottomHint | Qt::BypassWindowManagerHint;

    /* Set the windowFlags */
    setWindowFlags( wFlags );

    setAttribute( Qt::WA_TranslucentBackground );

    setFixedWidth( 200 );
    setFixedHeight( qApp->primaryScreen()->size().height() );

    widgetLyt = new QVBoxLayout();

    widgetLyt->setContentsMargins( 0, 10, 0, 10 );
    widgetLyt->setSpacing( 10 );
    widgetLyt->addStretch();
    setLayout( widgetLyt );

    QTimer::singleShot( 100, this, &DesQ::Cask::UI::startLayout );
}


void DesQ::Cask::UI::startLayout() {
    for ( QString widget: mWidgets ) {
        insertWidget( widget, -1 );
    }
}


void DesQ::Cask::UI::addWidget( QString name ) {
    /** Check if it exists */
    if ( widgetMap.contains( name ) ) {
        widgetMap[ name ]->reload();
        return;
    }

    insertWidget( name, -1 );
}


void DesQ::Cask::UI::insertWidget( QString name, int pos ) {
    /** Check if it exists */
    if ( widgetMap.contains( name ) ) {
        widgetMap[ name ]->reload();
        return;
    }

    DesQ::Cask::Casklet *w = new DesQ::Cask::Casklet( name, mScreen );

    if ( pos < 0 ) {
        widgetLyt->insertWidget( widgetLyt->count() + pos, w );
    }

    else {
        widgetLyt->insertWidget( pos, w );
    }

    widgetMap[ name ] = w;
}


void DesQ::Cask::UI::removeWidget( QString name ) {
    if ( not widgetMap.contains( name ) ) {
        qDebug() << "Loaded widgets:" << widgetMap.keys();
        return;
    }

    qDebug() << name << widgetMap[ name ];
    DesQ::Cask::Casklet *w = widgetMap.take( name );

    widgetLyt->removeWidget( w );
    delete w;
}


void DesQ::Cask::UI::reloadWidget( QString name ) {
    if ( name == "*" or name == "all" ) {
        for ( DesQ::Cask::Casklet *casklet: widgetMap.values() ) {
            if ( casklet->canReload() ) {
                casklet->reload();
            }
        }

        return;
    }

    if ( widgetMap.contains( name ) ) {
        if ( widgetMap[ name ]->canReload() ) {
            widgetMap[ name ]->reload();
        }
    }
}


void DesQ::Cask::UI::handleMessage( QString mesg, int fd ) {
    if ( mesg.startsWith( "add\n" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( parts.count() >= 2 ) {
            addWidget( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "insert\n" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( parts.count() >= 3 ) {
            int  pos = 0;
            bool ok  = false;
            parts[ 2 ].toInt( &ok );

            if ( ok ) {
                insertWidget( parts[ 1 ], pos );
            }

            else {
                insertWidget( parts[ 1 ], -1 );
            }
        }
    }

    else if ( mesg.startsWith( "list-widgets" ) ) {
        qApp->messageClient( mWidgets.join( "\n" ), fd );
    }

    else if ( mesg.startsWith( "remove\n" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( parts.count() >= 2 ) {
            removeWidget( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "reload\n" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( parts.count() >= 2 ) {
            reloadWidget( parts[ 1 ] );
        }
    }

    else {
        qDebug() << "Invalid command\n";
    }
}
